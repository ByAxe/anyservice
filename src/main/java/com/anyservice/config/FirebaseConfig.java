package com.anyservice.config;

import com.anyservice.web.security.dto.SecurityProperties;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Configuration
public class FirebaseConfig {

    private final SecurityProperties secProps;

    @Value("${spring.cloud.gcp.project-id}")
    private String googleProjectId;

    public FirebaseConfig(SecurityProperties secProps) {
        this.secProps = secProps;
    }

    @PostConstruct
    public void firebaseInit() throws IOException {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.getApplicationDefault())
                .setProjectId(googleProjectId)
                .setDatabaseUrl(secProps.getFirebaseProps().getDatabaseUrl()).build();
        if (FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options);
        }
    }

    @Bean
    public Firestore getDatabase() throws IOException {
        FirestoreOptions firestoreOptions = FirestoreOptions.newBuilder()
                .setCredentials(GoogleCredentials.getApplicationDefault()).build();
        return firestoreOptions.getService();
    }
}
