package com.anyservice.config;

import com.anyservice.core.enums.UserRole;
import com.anyservice.core.enums.UserState;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.web.security.SecurityFilter;
import com.anyservice.web.security.dto.SecurityProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final SecurityFilter tokenAuthenticationFilter;
    private final ObjectMapper objectMapper;
    private final SecurityProperties restSecProps;

    public SecurityConfig(ObjectMapper objectMapper, SecurityProperties restSecProps,
                          SecurityFilter tokenAuthenticationFilter) {
        this.objectMapper = objectMapper;
        this.restSecProps = restSecProps;
        this.tokenAuthenticationFilter = tokenAuthenticationFilter;
    }

    @Bean
    public AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return (httpServletRequest, httpServletResponse, e) -> {
            Map<String, Object> errorObject = new HashMap<>();
            int errorCode = 401;
            errorObject.put("message", "Unauthorized access of protected resource, invalid credentials");
            errorObject.put("error", HttpStatus.UNAUTHORIZED);
            errorObject.put("code", errorCode);
            errorObject.put("timestamp", new Timestamp(new Date().getTime()));
            httpServletResponse.setContentType("application/json;charset=UTF-8");
            httpServletResponse.setStatus(errorCode);
            httpServletResponse.getWriter().write(objectMapper.writeValueAsString(errorObject));
        };
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(restSecProps.getAllowedOrigins());
        configuration.setAllowedMethods(restSecProps.getAllowedMethods());
        configuration.setAllowedHeaders(restSecProps.getAllowedHeaders());
        configuration.setAllowCredentials(restSecProps.isAllowCredentials());
        configuration.setExposedHeaders(restSecProps.getExposedHeaders());
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .configurationSource(corsConfigurationSource())
                .and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint())
                .and().authorizeRequests()

                // APPLICATION ADMINISTRATION
                .antMatchers(restSecProps.getAllowedPublicApis().toArray(new String[0])).permitAll()
                .antMatchers("/actuator/**").access("hasRole('ROLE_SUPER_ADMIN')")

                // SECURITY
                .antMatchers("/**/login").access("isAnonymous() || hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers("/**/logout").access("isAuthenticated()")

                // USER
                .antMatchers(POST, "/**/user").access("isAnonymous() || hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers(GET, "/**/user/**").access("isAuthenticated()")
                .antMatchers(GET, "/**/user").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
                .antMatchers(PUT, "/**/user/**").access("isAuthenticated()")
                .antMatchers(DELETE, "/**/user/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")

                // CATEGORY
                .antMatchers(GET, "/**/category/**").access("isAuthenticated()")
                .antMatchers(POST, "/**/category/**").access("hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers(PUT, "/**/category/**").access("hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers(DELETE, "/**/category/**").access("hasRole('ROLE_SUPER_ADMIN')")

                // FILE
                .antMatchers("/**/file/**").access("isAuthenticated()")

                // ORDER
                .antMatchers("/**/order/**").access("isAuthenticated()")
                .antMatchers("/**/review/**").access("isAuthenticated()")
                .antMatchers("/**/candidate/**").access("isAuthenticated()")
                .antMatchers("/**/chat/**").access("isAuthenticated()")

                .antMatchers("/**").access("hasRole('ROLE_SUPER_ADMIN')")

                .antMatchers(OPTIONS).permitAll()
                .and()
                .addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * Create special inner user instance with super role
     * That can access anything
     * (For development purposes)
     *
     * @return inner user instance
     */
    @Bean
    public UserDetailed innerUser() {
        return UserDetailed.builder()
                .id(UUID.randomUUID().toString())
                .email("inneruseremail@anyservice.com")
                .state(UserState.ACTIVE)
                .role(UserRole.ROLE_SUPER_ADMIN)
                .isVerified(true)
                .password("inneruserpassword")
                .passwordUpdateDate(OffsetDateTime.now())
                .build();
    }
}
