package com.anyservice.core;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ByteArrayBearer {
    private byte[] byteArray;
}
