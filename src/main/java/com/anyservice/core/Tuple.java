package com.anyservice.core;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Tuple<X, Y> {

    private final X x;
    private final Y y;

    private Tuple(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public static <X, Y> Tuple<X, Y> of(X x, Y y) {
        return new Tuple<>(x, y);
    }

    public X x() {
        return x;
    }

    public Y y() {
        return y;
    }
}

