package com.anyservice.core.enums;

public enum Compliment {
    NICE_PERSON,
    GREAT_DIALOG
}
