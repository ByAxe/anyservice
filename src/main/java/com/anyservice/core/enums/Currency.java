package com.anyservice.core.enums;

public enum Currency {
    USD,
    BYN,
    PLN,
    EUR,
}
