package com.anyservice.core.enums;

import java.util.ArrayList;
import java.util.List;

import static com.anyservice.core.enums.FileExtension.*;
import static java.util.Arrays.asList;

/**
 * Describes, to what Domain (business area/operation)
 * the file belongs to
 */
public enum FileType {
    PROFILE_PHOTO("User profile photo", "user/photo"),
    PORTFOLIO("Portfolio of an individual or a company", "user/portfolio"),
    DOCUMENT("Document for approval of user profile", "user/document"),
    MAIN_ATTACHMENT("Main attachment of a document", "order/attachment/main"),
    ATTACHMENT("Attachment for an order", "order/attachment");

    public final String description;
    public final String directory;

    FileType(String description, String directory) {
        this.description = description;
        this.directory = directory;
    }

    /**
     * Returns a {@link List} of appropriate {@link FileExtension}
     * for given {@link FileType}
     *
     * @param type of a file {@link FileType}
     * @return list of file extensions those are allowed for given file type
     */
    public static List<FileExtension> getAllowedFileExtensions(FileType type) {
        List<FileExtension> extensions = new ArrayList<>();

        List<FileExtension> photoFormats = asList(jpeg, jpg, png, jpe);
        List<FileExtension> allExtensions = asList(FileExtension.values());

        switch (type) {
            case MAIN_ATTACHMENT:
                return photoFormats;
            case PROFILE_PHOTO:
            case DOCUMENT:
                extensions.addAll(photoFormats);
                extensions.add(pdf);
                return extensions;
            case PORTFOLIO:
            case ATTACHMENT:
                return allExtensions;
            default:
                return extensions;
        }
    }

    /**
     * Returns boolean result if the given file extension is allowed for given file type
     *
     * @param fileType      type of file
     * @param fileExtension extension of a file
     * @return is allowed?
     */
    public static boolean isAllowedFileExtensionForFileType(FileType fileType, FileExtension fileExtension) {
        return getAllowedFileExtensions(fileType).contains(fileExtension);
    }
}
