package com.anyservice.core.enums;

public enum LocationType {
    AT_CUSTOMER("At customer's location"),
    AT_WORKER("At worker's location"),
    REMOTELY("Work can be done remotely"),
    ANYWHERE("Does not matter where");

    public final String description;

    LocationType(String description) {
        this.description = description;
    }
}
