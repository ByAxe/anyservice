package com.anyservice.core.enums;

public enum MessageStatus {
    SENT("Message sent, but not yet delivered"),
    DELIVERED("Message delivered, but not yet read"),
    READ("Receiver already 'opened' message");

    private final String description;

    MessageStatus(String description) {
        this.description = description;
    }
}
