package com.anyservice.core.enums;

public enum OrderState {
    DRAFT("Order in state of draft - not yet published"),
    PUBLISHED("Order published and visible for candidates"),
    EDITED("Order edited after publication"),
    CANDIDATE_SELECTED("Candidate selected, order removed from search list"),
    COMPLETED("Order completed"),
    NOT_COMPLETED("Order completed with a problem"),
    CANCELLED("Order cancelled by customer"),
    BLOCKED("Order blocked by administrator"),
    REMOVED("Order removed by user before it was published");

    private final String description;

    OrderState(String description) {
        this.description = description;
    }
}
