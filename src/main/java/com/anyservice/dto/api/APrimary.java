package com.anyservice.dto.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.OffsetDateTime;

/**
 * Root class for all DTO in application
 * Contains necessary fields for all DTOs
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public abstract class APrimary {
    private Object id;
    private OffsetDateTime dtCreate;
    private OffsetDateTime dtUpdate;

    /**
     * Copying constructor
     *
     * @param source dto to copy
     */
    public APrimary(APrimary source) {
        setId(source.getId());
        setDtCreate(source.getDtCreate());
        setDtUpdate(source.getDtUpdate());
    }

}
