package com.anyservice.dto.order;

import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Detailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.order.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Detailed
public class Candidate extends APrimary {
    private boolean isSelected;
    private Response response;
    private UserBrief user;
    private Chat chat;
    private UUID orderId;
}
