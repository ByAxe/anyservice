package com.anyservice.dto.order;

import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Detailed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Detailed
public class Category extends APrimary {
    private String title;
    private String description;
    private Category parentCategory;
}
