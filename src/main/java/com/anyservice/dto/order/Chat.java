package com.anyservice.dto.order;

import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Detailed;
import com.anyservice.entity.order.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Detailed
public class Chat extends APrimary {
    private List<Message> messages;
}
