package com.anyservice.dto.order;

import com.anyservice.core.enums.LocationType;
import com.anyservice.entity.CountryEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Location {
    private CountryEntity country;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String zipCode;
    private LocationType locationType;
}
