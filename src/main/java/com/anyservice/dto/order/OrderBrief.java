package com.anyservice.dto.order;

import com.anyservice.core.enums.Currency;
import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Brief;
import com.anyservice.dto.file.FileDetailed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Brief
public class OrderBrief extends APrimary {
    private String headline;
    private String description;
    private BigDecimal price;
    private boolean isDiscussablePrice;
    private Currency currency;
    private OffsetDateTime deadline;
    private Location location;
    private FileDetailed mainAttachment;
    private OrderState state;
    private int responses;
    private List<Category> categories;

    public OrderBrief(OrderBrief source) {
        super(source);

        setHeadline(source.getHeadline());
        setDescription(source.getDescription());
        setPrice(source.getPrice());
        setDiscussablePrice(source.isDiscussablePrice());
        setCurrency(source.getCurrency());
        setDeadline(source.getDeadline());
        setLocation(source.getLocation());
        setMainAttachment(source.getMainAttachment());
        setState(source.getState());
        setResponses(source.getResponses());
        setCategories(source.getCategories());
    }
}
