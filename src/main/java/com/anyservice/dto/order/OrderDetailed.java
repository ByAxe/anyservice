package com.anyservice.dto.order;

import com.anyservice.dto.api.Detailed;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.user.UserBrief;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Detailed
public class OrderDetailed extends OrderBrief {
    private String phone;
    private UserBrief customer;
    private List<Candidate> candidates;
    private List<FileDetailed> attachments;

    public OrderDetailed(OrderBrief source) {
        super(source);
    }
}
