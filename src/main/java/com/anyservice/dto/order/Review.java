package com.anyservice.dto.order;

import com.anyservice.core.enums.Compliment;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Detailed;
import com.anyservice.dto.user.UserBrief;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Detailed
public class Review extends APrimary {
    private String header;
    private String body;
    private BigDecimal rating;
    private String contractorResponse;
    private Set<Compliment> compliments;
    private UserBrief author;
    private UserBrief contractor;
    private OrderBrief order;
}
