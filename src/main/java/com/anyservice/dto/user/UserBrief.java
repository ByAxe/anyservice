package com.anyservice.dto.user;

import com.anyservice.core.enums.UserRole;
import com.anyservice.core.enums.UserState;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.api.Brief;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Brief
public class UserBrief extends APrimary implements Serializable {
    private static final long serialVersionUID = 4408418647685225829L;

    private String email;
    private UserState state;
    private UserRole role;
    private boolean isVerified;
    private boolean isLegalStatusVerified;

    private String name;
    private String issuer;
    private String picture;

    public UserBrief(UserBrief source) {
        setId(source.getId());
        setDtCreate(source.getDtCreate());
        setDtUpdate(source.getDtUpdate());

        setEmail(source.getEmail());
        setState(source.getState());
        setRole(source.getRole());
        setVerified(source.isVerified());
        setLegalStatusVerified(source.isLegalStatusVerified());

        setName(source.getName());
        setIssuer(source.getIssuer());
        setPicture(source.getPicture());
    }
}
