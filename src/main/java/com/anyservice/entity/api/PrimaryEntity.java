package com.anyservice.entity.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;

@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
@Data
@AllArgsConstructor
@SuperBuilder
public abstract class PrimaryEntity extends EntityWithUUID {

    @Column(name = "dt_create", nullable = false)
    private OffsetDateTime dtCreate;
    @Column(name = "dt_update", nullable = false)
    private OffsetDateTime dtUpdate;

    public PrimaryEntity() {
        super();
    }

}
