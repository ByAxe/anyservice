package com.anyservice.entity.file;

import com.anyservice.core.enums.FileExtension;
import com.anyservice.core.enums.FileState;
import com.anyservice.core.enums.FileType;
import com.anyservice.entity.api.EntityWithUUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SuperBuilder
@Table(name = "file_description")
@DynamicUpdate
@DynamicInsert
public class FileEntity extends EntityWithUUID {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String link;

    private Long size;

    @Enumerated(value = EnumType.STRING)
    private FileExtension extension;

    @Column(name = "dt_create", nullable = false)
    private OffsetDateTime dtCreate;

    @Enumerated(value = EnumType.STRING)
    private FileState state;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private FileType type;
}
