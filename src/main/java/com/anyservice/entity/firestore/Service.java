package com.anyservice.entity.firestore;


import com.google.cloud.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Service implements Serializable {
    private String id;
    private Timestamp dtCreate;
    private Timestamp dtUpdate;
    private String categoryId;
    private String title;
    private BigDecimal lowestPrise;
    private BigDecimal highestPrise;
    private String description;
    private List<String> portfolioIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getDtCreate() {
        return dtCreate;
    }

    public void setDtCreate(Timestamp dtCreate) {
        this.dtCreate = dtCreate;
    }

    public Timestamp getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(Timestamp dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getLowestPrise() {
        return lowestPrise;
    }

    public void setLowestPrise(BigDecimal lowestPrise) {
        this.lowestPrise = lowestPrise;
    }

    public BigDecimal getHighestPrise() {
        return highestPrise;
    }

    public void setHighestPrise(BigDecimal highestPrise) {
        this.highestPrise = highestPrise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPortfolioIds() {
        return portfolioIds;
    }

    public void setPortfolioIds(List<String> portfolioIds) {
        this.portfolioIds = portfolioIds;
    }
}
