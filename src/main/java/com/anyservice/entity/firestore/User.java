package com.anyservice.entity.firestore;

import com.anyservice.core.enums.LegalStatus;
import com.anyservice.core.enums.UserRole;
import com.anyservice.core.enums.UserState;
import com.google.cloud.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private String id;
    private Timestamp dtCreate;
    private Timestamp dtUpdate;

    private String email;
    private UserState state;
    private UserRole role;
    private boolean isVerified;
    private boolean isLegalStatusVerified;

    private String name;
    private String issuer;
    private String picture;

    private Timestamp passwordUpdateDate;
    private String description;
    private Map<String, Object> contacts;
    private LegalStatus legalStatus;

    private String passwordHash;
    private Map<String, String> addresses;
    private String defaultCountryId;
    private String profilePhotoId;
    private List<String> documentIds;
    private List<String> portfolioIds;
    private List<String> countryIdsWhereServicesProvided;
}
