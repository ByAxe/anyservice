package com.anyservice.entity.order;

import com.anyservice.entity.api.PrimaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static javax.persistence.CascadeType.*;

@Entity
@Table(name = "candidates")
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class CandidateEntity extends PrimaryEntity {

    @Column(name = "selected")
    private boolean isSelected;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Response response;

    @ManyToOne(cascade = {PERSIST, REFRESH})
    @JoinColumn(name = "order_id")
    private OrderEntity order;

    @ManyToOne(cascade = {REMOVE})
    @JoinColumn(name = "chat_id")
    private ChatEntity chat;

    @Column(name = "user_id", nullable = false)
    private String user;
}
