package com.anyservice.entity.order;

import com.anyservice.entity.api.PrimaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@SuperBuilder
@Table(name = "categories")
@DynamicUpdate
@DynamicInsert
public class CategoryEntity extends PrimaryEntity {

    @Column(nullable = false)
    private String title;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private CategoryEntity parentCategory;

    public CategoryEntity() {
        super();
    }

}
