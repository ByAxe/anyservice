package com.anyservice.entity.order;

import com.anyservice.entity.api.PrimaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@SuperBuilder
@Table(name = "chats")
public class ChatEntity extends PrimaryEntity {

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Message> messages;

    public ChatEntity() {
        super();
    }
}
