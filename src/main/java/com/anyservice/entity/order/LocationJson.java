package com.anyservice.entity.order;

import com.anyservice.core.enums.LocationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LocationJson {
    private UUID countryId;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String zipCode;
    private LocationType locationType;
}
