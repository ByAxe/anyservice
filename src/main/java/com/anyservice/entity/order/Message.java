package com.anyservice.entity.order;

import com.anyservice.core.enums.MessageStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {
    private String senderId;
    private String text;
    private MessageStatus status;
    private OffsetDateTime dateTime;
}
