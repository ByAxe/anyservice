package com.anyservice.entity.order;

import com.anyservice.core.enums.Currency;
import com.anyservice.core.enums.OrderState;
import com.anyservice.entity.api.PrimaryEntity;
import com.anyservice.entity.file.FileEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.LAZY;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@SuperBuilder
@Table(name = "orders")
@DynamicUpdate
@DynamicInsert
public class OrderEntity extends PrimaryEntity {

    @Column(nullable = false)
    private String headline;

    @Column(nullable = false)
    private String description;

    private String phone;
    private BigDecimal price;

    @Column(name = "is_discussable_price")
    private boolean isDiscussablePrice;

    private OffsetDateTime deadline;

    @Enumerated(value = EnumType.STRING)
    private Currency currency;

    @Enumerated(value = EnumType.STRING)
    private OrderState state;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private LocationJson location;

    @Column(nullable = false)
    private String customer;

    @ManyToOne(fetch = LAZY, cascade = REMOVE)
    @JoinColumn(name = "main_attachment")
    private FileEntity mainAttachment;

    @OneToMany(mappedBy = "order", cascade = ALL)
    private List<CandidateEntity> candidates;

    @ManyToMany(fetch = LAZY, cascade = REMOVE)
    @JoinTable(name = "orders_files",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "file_id")
    )
    private List<FileEntity> attachments;

    @ManyToMany(fetch = LAZY, cascade = REMOVE)
    @JoinTable(name = "orders_categories",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<CategoryEntity> categories;

    public OrderEntity() {
        super();
    }
}
