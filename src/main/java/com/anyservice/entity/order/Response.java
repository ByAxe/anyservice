package com.anyservice.entity.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Response {
    private String text;
    private BigDecimal offeredPrice;
    private boolean isPriceUpdated;
}
