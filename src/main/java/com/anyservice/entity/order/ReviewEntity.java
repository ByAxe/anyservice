package com.anyservice.entity.order;

import com.anyservice.core.enums.Compliment;
import com.anyservice.entity.api.PrimaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

import static javax.persistence.FetchType.LAZY;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@SuperBuilder
@Table(name = "reviews")
@DynamicUpdate
@DynamicInsert
public class ReviewEntity extends PrimaryEntity {

    private String header;
    private String body;
    @Column(nullable = false)
    private BigDecimal rating;
    @Column(name = "contractor_response")
    private String contractorResponse;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<Compliment> compliments;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private String contractor;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "order_id", nullable = false)
    private OrderEntity order;

    public ReviewEntity() {
        super();
    }

}


