package com.anyservice.repository;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class FirestoreRepository implements IFirestoreRepository {
    private final Firestore firestore;

    @Value("${spring.cloud.gcp.firestore.search-in-parallel}")
    private boolean searchInParallel;

    @Value("${spring.cloud.gcp.firestore.id-column}")
    private String idColumn;

    public FirestoreRepository(Firestore firestore) {
        this.firestore = firestore;
    }

    @Override
    public Firestore getFirestore() {
        return firestore;
    }

    @Override
    public DocumentReference getDocumentReference(String collection, String documentKey) {
        return firestore.collection(collection).document(documentKey);
    }

    @Override
    public <V> Future<WriteResult> saveAsync(String collection, String key, V value) {
        return firestore.collection(collection)
                .document(key)
                .set(value);
    }

    public <V> WriteResult saveSync(String collection, String key, V value)
            throws ExecutionException, InterruptedException {
        return saveAsync(collection, key, value)
                .get();
    }

    @Override
    public <V> Future<WriteResult> updateAsync(String collection, String key, V value) {
        return saveAsync(collection, key, value);
    }

    @Override
    public <V> Future<WriteResult> createAsync(String collection, String key, V value) {
        return saveAsync(collection, key, value);
    }

    @Override
    public <V> V findByKey(String collection, String key, Class<V> valueClass)
            throws ExecutionException, InterruptedException {
        return firestore.collection(collection)
                .document(key)
                .get()
                .get()
                .toObject(valueClass);
    }

    @Override
    public <V> List<V> findAll(String collection, Class<V> valueClass) {
        Iterable<DocumentReference> listDocuments = firestore
                .collection(collection)
                .listDocuments();

        return StreamSupport.stream(listDocuments.spliterator(), searchInParallel)
                .map(DocumentReference::get)
                .map(d -> {
                    try {
                        return d.get();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .map(d -> d.toObject(valueClass))
                .collect(Collectors.toList());
    }

    @Override
    public <V> List<V> findAllById(String collection, List<String> idList, Class<V> valueClass)
            throws ExecutionException, InterruptedException {
        return firestore.collection(collection)
                .whereIn(idColumn, idList)
                .get()
                .get()
                .getDocuments()
                .stream()
                .map(d -> d.toObject(valueClass))
                .collect(Collectors.toList());
    }

    @Override
    public boolean exists(String collection, String key) throws ExecutionException, InterruptedException {
        return firestore
                .collection(collection)
                .document(key)
                .get()
                .get()
                .exists();
    }

    @Override
    public long count(String collection) {
        return StreamSupport.stream(firestore
                .collection(collection)
                .listDocuments()
                .spliterator(), false)
                .count();
    }

    @Override
    @SneakyThrows
    public void delete(String collection, String key) {
        firestore.collection(collection)
                .document(key)
                .delete()
                .get();
    }

    @Override
    public void deleteAll(String collection) {
        firestore.collection(collection)
                .listDocuments()
                .forEach(DocumentReference::delete);
    }
}
