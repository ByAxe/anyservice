package com.anyservice.repository;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface IFirestoreRepository {
    /**
     * Save given key-value pair
     *
     * @param collection collection, this object should be in
     * @param key        key of saving object
     * @param <V>        type of class stored in cache
     * @param value      value of saving object
     * @return future of saving results;
     * <p>
     * Call .get() on result of this method, to block on finishing operation
     */
    <V> Future<WriteResult> saveAsync(String collection, String key, V value) throws Exception;

    /**
     * Save given key-value pair synchronously
     *
     * @param collection collection, this object should be in
     * @param key        key of saving object
     * @param <V>        type of class stored in cache
     * @param value      value of saving object
     * @return saving result
     */
    <V> WriteResult saveSync(String collection, String key, V value) throws ExecutionException, InterruptedException;

    /**
     * Wraps {@link this#saveAsync(String, String, Object)} method and redirects all the data to it
     */
    <V> Future<WriteResult> updateAsync(String collection, String key, V value) throws Exception;

    /**
     * Wraps {@link this#saveAsync(String, String, Object)} method and redirects all the data to it
     */
    <V> Future<WriteResult> createAsync(String collection, String key, V value) throws Exception;

    /**
     * Find entity in cache by passed key
     *
     * @param key        object key in cache
     * @param valueClass class of stored entity
     * @param collection collection, this object expected to be in
     * @param <V>        type of stored object
     * @return Object of given type retrieved from cache
     */
    <V> V findByKey(String collection, String key, Class<V> valueClass)
            throws ExecutionException, InterruptedException;

    /**
     * Remove object with the given key from cache
     *
     * @param collection collection, this object expected to be in
     * @param key        object key
     */
    void delete(String collection, String key);

    /**
     * Check if document exists
     *
     * @param collection collection, this object expected to be in
     * @param key        object key
     * @return whether it exists or not
     */
    boolean exists(String collection, String key) throws ExecutionException, InterruptedException;

    /**
     * Get firestore for custom operations
     *
     * @return firestore library interface
     */
    Firestore getFirestore();

    /**
     * Find all the documents for given collection and convert all found entities to given class
     *
     * @param collection where search will be performed
     * @param valueClass class of stored entity
     * @param <V>        type of stored objects
     * @return list of all found objects
     */
    <V> List<V> findAll(String collection, Class<V> valueClass);

    /**
     * Find all documents for given collection via given list of id's
     * and convert all found entities to given class
     *
     * @param collection where search will be performed
     * @param idList     list of identifiers
     * @param valueClass class of stored entity
     * @param <V>        type of stored objects
     * @return list of all found objects
     */
    <V> List<V> findAllById(String collection, List<String> idList, Class<V> valueClass)
            throws ExecutionException, InterruptedException;


    /**
     * Get {@link DocumentReference}
     * <p>
     * Needed for transaction operations.
     * For example
     *
     * <code>
     * final DocumentReference userDocumentReference = firestoreRepository.getDocumentReference(usersCollection, userId);
     * final DocumentReference cacheDocumentReference = firestoreRepository.getDocumentReference(cacheCollection, userId);
     * <p>
     * // Run following operations as single transaction
     * firestoreRepository.getFirestore().runTransaction(transaction -> {
     * try {
     * // update user verification status
     * transaction.set(userDocumentReference, user);
     * <p>
     * // remove verification code from cache
     * transaction.delete(cacheDocumentReference);
     * <p>
     * // In case everything is ok nothing should occur
     * return null;
     * } catch (Exception e) {
     * throw new RuntimeException("Updating user verification status was unsuccessful", e);
     * }
     * });
     * </code>
     *
     * @param collection  collection, this object expected to be in
     * @param documentKey key of object
     * @return special reference in firestore
     */
    DocumentReference getDocumentReference(String collection, String documentKey);

    /**
     * Counts the amount of elements in given collection
     *
     * @param collection given collection
     * @return amount of elements
     */
    long count(String collection);

    /**
     * Delete all documents from given collection
     *
     * @param collection collection with documents\objects
     */
    void deleteAll(String collection);

}
