package com.anyservice.service;

import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.user.api.IUserService;
import com.anyservice.web.security.dto.EnumGrantedAuthority;
import com.anyservice.web.security.exceptions.api.LoginException;
import com.google.firebase.auth.FirebaseToken;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
public class SecurityService {

    private final IUserService firebaseUserService;
    private final MessageSource messageSource;

    @Value("${security.inner.key}")
    private String innerKey;

    @Value("${security.inner.header}")
    private String innerHeader;

    @Value("${security.jwt.header}")
    private String jwtHeader;

    @Value("${security.jwt.key-prefix}")
    private String jwtKeyPrefix;

    public SecurityService(IUserService userService, MessageSource messageSource) {
        this.firebaseUserService = userService;
        this.messageSource = messageSource;
    }

    /**
     * Get special auth token from request header
     *
     * @param request with headers and token
     * @return firebase token as string
     */
    public String getBearerToken(HttpServletRequest request) {
        String bearerToken = null;
        String authorization = request.getHeader(jwtHeader);
        if (StringUtils.hasText(authorization) && authorization.startsWith(jwtKeyPrefix)) {
            bearerToken = authorization.substring(7);
        }
        return bearerToken;
    }

    /**
     * Get user data from token
     * <p>
     * Reads already existing user from db, or creates it
     *
     * @param decodedToken firebase token with user
     * @return inner user representation
     */
    public UserDetailed getUserFromFirebaseToken(@NonNull FirebaseToken decodedToken) {
        // get user uid from token
        String uid = decodedToken.getUid();

        // find user in db
        Optional<UserDetailed> user = firebaseUserService.findById(uid);

        // if user wasn't found - create it and return created
        return user.orElseGet(() -> firebaseUserService.create(decodedToken));
    }

    /**
     * Get granted authorities of a user
     *
     * @param user user
     * @return user authorities
     */
    public Collection<GrantedAuthority> getUserAuthorities(UserDetailed user) {
        return Collections.singletonList(
                Optional.ofNullable(user)
                        .map(UserDetailed::getRole)
                        .map(EnumGrantedAuthority::new)
                        .orElseThrow(
                                () -> new LoginException(
                                        messageSource.getMessage(
                                                "jwt.authentication.provider.retrieve.user",
                                                null, LocaleContextHolder.getLocale()
                                        )
                                )
                        )
        );
    }

    /**
     * Validate if given request authorized with inner user
     *
     * @param request http request
     * @return from inner user ?
     */
    public boolean isInner(HttpServletRequest request) {
        String header = request.getHeader(innerHeader);

        if (header != null) {
            return innerKey.equals(header);
        }

        return false;
    }
}
