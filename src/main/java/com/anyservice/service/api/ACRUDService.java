package com.anyservice.service.api;

import com.anyservice.service.converters.api.CustomConversionService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
public abstract class ACRUDService<BRIEF, DETAILED, ID, VERSION>
        implements ICRUDService<BRIEF, DETAILED, ID, VERSION> {

    protected abstract JpaRepository<?, ID> getRepository();

    protected abstract CustomConversionService getConversionService();

    protected abstract Class<DETAILED> getDetailedClass();

    protected abstract Class<BRIEF> getBriefClass();

    @Override
    public Optional<DETAILED> findById(ID id) {
        return getRepository().findById(id)
                .map(e -> getConversionService().convert(e, getDetailedClass()));
    }

    @Override
    public List<BRIEF> findAll() {
        return getRepository().findAll()
                .stream()
                .map(e -> getConversionService().convert(e, getBriefClass()))
                .collect(Collectors.toList());
    }

    @Override
    public List<BRIEF> findAllById(Iterable<ID> idList) {
        return getRepository().findAllById(idList)
                .stream()
                .map(e -> getConversionService().convert(e, getBriefClass()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(ID id) {
        return getRepository().existsById(id);
    }

    @Override
    public long count() {
        return getRepository().count();
    }
}
