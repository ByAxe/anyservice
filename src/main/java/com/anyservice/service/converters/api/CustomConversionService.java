package com.anyservice.service.converters.api;

import com.anyservice.core.Tuple;
import com.anyservice.service.converters.api.exceptions.ConversionException;
import lombok.NonNull;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CustomConversionService {
    private final Map<Tuple<Class<?>, Class<?>>, IConverter<?, ?>> convertersMap;
    private final MessageSource messageSource;

    public CustomConversionService(List<IConverter<?, ?>> converters, MessageSource messageSource) {
        // Convert list to map with keys "source_class-target_class"
        convertersMap = converters.stream()
                .collect(Collectors.toMap(
                        k -> Tuple.of(k.getSourceClass(), k.getTargetClass()),
                        v -> v));

        this.messageSource = messageSource;
    }

    /**
     * Converts source object to object with target type
     * if such converter registered as bean
     *
     * @param source     source object
     * @param targetType type of object at the end of conversion
     * @param <T>        type of object at the end of conversion
     * @return source object with target type
     * @throws ConversionException if converter for given pair was not found
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @NonNull
    public <T> T convert(@NonNull Object source, Class<T> targetType) {
        final Class<?> sourceType = source.getClass();

        // Get tuple of classes - key for our map
        Tuple<Class<?>, Class<T>> classTuple = Tuple.of(sourceType, targetType);

        // Extract target converter from map via key
        IConverter converter = convertersMap.get(classTuple);

        // Check if it's present
        if (converter == null) {
            throw new ConversionException(messageSource.getMessage("conversion.exception",
                    new Object[]{sourceType, targetType}, LocaleContextHolder.getLocale()));
        }

        // Convert source object to target type
        return (T) converter.convert(source);
    }
}
