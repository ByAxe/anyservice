package com.anyservice.service.converters.api;


import lombok.NonNull;

public interface IConverter<S, T> {

    /**
     * Convert {@link S} source object to object with {@link T} type
     *
     * @param source source object
     * @return object with {@link T} type
     */
    @NonNull
    T convert(@NonNull S source);

    /**
     * Get source class of converted object
     *
     * @return {@link Class} of {@link S}
     */
    Class<S> getSourceClass();

    /**
     * Get target class of converted object
     *
     * @return {@link Class} of {@link T}
     */
    Class<T> getTargetClass();
}
