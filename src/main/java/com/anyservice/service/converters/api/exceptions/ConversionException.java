package com.anyservice.service.converters.api.exceptions;

/**
 * If some error occurs during conversion
 */
public class ConversionException extends IllegalArgumentException {
    public ConversionException() {
        super();
    }

    public ConversionException(String s) {
        super(s);
    }

    public ConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConversionException(Throwable cause) {
        super(cause);
    }
}
