package com.anyservice.service.converters.country;

import com.anyservice.entity.CountryEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class CountryEntityToCountryIdConverter implements IConverter<CountryEntity, String> {
    @Override
    public String convert(@NonNull CountryEntity source) {
        return source.getId().toString();
    }

    @Override
    public Class<CountryEntity> getSourceClass() {
        return CountryEntity.class;
    }

    @Override
    public Class<String> getTargetClass() {
        return String.class;
    }
}
