package com.anyservice.service.converters.country;

import com.anyservice.entity.CountryEntity;
import com.anyservice.repository.CountryRepository;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CountryIdToCountryEntityConverter implements IConverter<String, CountryEntity> {

    private final CountryRepository countryRepository;

    public CountryIdToCountryEntityConverter(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public CountryEntity convert(@NonNull String source) {
        UUID countryId = UUID.fromString(source);

        CountryEntity country = countryRepository.findById(countryId)
                .orElseThrow(() -> new IllegalStateException("Country id present, but no country was found via given id"));

        return country;
    }

    @Override
    public Class<String> getSourceClass() {
        return String.class;
    }

    @Override
    public Class<CountryEntity> getTargetClass() {
        return CountryEntity.class;
    }
}
