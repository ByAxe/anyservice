package com.anyservice.service.converters.file;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class FileDetailedToFileIdConverter implements IConverter<FileDetailed, String> {
    @Override
    public String convert(@NonNull FileDetailed source) {
        return source.getId().toString();
    }

    @Override
    public Class<FileDetailed> getSourceClass() {
        return FileDetailed.class;
    }

    @Override
    public Class<String> getTargetClass() {
        return String.class;
    }
}
