package com.anyservice.service.converters.file;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.repository.FileRepository;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FileIdToFileDetailedConverter implements IConverter<String, FileDetailed> {

    private final FileRepository fileRepository;
    private final IConverter<FileEntity, FileDetailed> fileConverter;

    public FileIdToFileDetailedConverter(FileRepository fileRepository,
                                         IConverter<FileEntity, FileDetailed> fileConverter) {
        this.fileRepository = fileRepository;
        this.fileConverter = fileConverter;
    }

    @Override
    public FileDetailed convert(@NonNull String source) {
        UUID fileId = UUID.fromString(source);

        FileEntity fileEntity = fileRepository.findById(fileId)
                .orElseThrow(() -> new IllegalArgumentException("File id present, but file was not found"));

        @NonNull FileDetailed target = fileConverter.convert(fileEntity);

        return target;
    }

    @Override
    public Class<String> getSourceClass() {
        return String.class;
    }

    @Override
    public Class<FileDetailed> getTargetClass() {
        return FileDetailed.class;
    }
}
