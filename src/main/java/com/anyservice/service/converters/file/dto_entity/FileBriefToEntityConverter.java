package com.anyservice.service.converters.file.dto_entity;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FileBriefToEntityConverter implements IConverter<FileBrief, FileEntity> {
    @Override
    public FileEntity convert(@NonNull FileBrief source) {
        return FileEntity.builder()
                .id(UUID.fromString(String.valueOf(source.getId())))
                .dtCreate(source.getDtCreate())
                .name(source.getName())
                .link(source.getLink())
                .extension(source.getExtension())
                .size(source.getSize())
                .state(source.getState())
                .build();
    }

    @Override
    public Class<FileBrief> getSourceClass() {
        return FileBrief.class;
    }

    @Override
    public Class<FileEntity> getTargetClass() {
        return FileEntity.class;
    }
}
