package com.anyservice.service.converters.file.dto_entity;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class FileDetailedToEntityConverter implements IConverter<FileDetailed, FileEntity> {

    private final IConverter<FileBrief, FileEntity> fileBriefToEntityConverter;

    public FileDetailedToEntityConverter(IConverter<FileBrief, FileEntity> fileBriefToEntityConverter) {
        this.fileBriefToEntityConverter = fileBriefToEntityConverter;
    }

    @Override
    public FileEntity convert(@NonNull FileDetailed source) {
        FileEntity target = fileBriefToEntityConverter.convert(source);

        target.setType(source.getFileType());

        return target;
    }

    @Override
    public Class<FileDetailed> getSourceClass() {
        return FileDetailed.class;
    }

    @Override
    public Class<FileEntity> getTargetClass() {
        return FileEntity.class;
    }
}
