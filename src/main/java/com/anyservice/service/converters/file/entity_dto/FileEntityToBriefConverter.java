package com.anyservice.service.converters.file.entity_dto;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class FileEntityToBriefConverter implements IConverter<FileEntity, FileBrief> {

    @Override
    public FileBrief convert(@NonNull FileEntity source) {
        return FileBrief.builder()
                .id(source.getId())
                .dtCreate(source.getDtCreate())
                .name(source.getName())
                .link(source.getLink())
                .extension(source.getExtension())
                .size(source.getSize())
                .state(source.getState())
                .build();
    }

    @Override
    public Class<FileEntity> getSourceClass() {
        return FileEntity.class;
    }

    @Override
    public Class<FileBrief> getTargetClass() {
        return FileBrief.class;
    }
}
