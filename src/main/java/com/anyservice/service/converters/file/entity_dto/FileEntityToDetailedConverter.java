package com.anyservice.service.converters.file.entity_dto;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class FileEntityToDetailedConverter implements IConverter<FileEntity, FileDetailed> {

    private final IConverter<FileEntity, FileBrief> fileEntityToBriefConverter;

    public FileEntityToDetailedConverter(IConverter<FileEntity, FileBrief> fileEntityToBriefConverter) {
        this.fileEntityToBriefConverter = fileEntityToBriefConverter;
    }

    @Override
    public FileDetailed convert(@NonNull FileEntity source) {
        FileDetailed target = new FileDetailed(fileEntityToBriefConverter.convert(source));

        target.setFileType(source.getType());

        return target;
    }

    @Override
    public Class<FileEntity> getSourceClass() {
        return FileEntity.class;
    }

    @Override
    public Class<FileDetailed> getTargetClass() {
        return FileDetailed.class;
    }
}
