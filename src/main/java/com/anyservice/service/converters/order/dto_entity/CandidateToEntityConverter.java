package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Chat;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.entity.order.ChatEntity;
import com.anyservice.repository.OrderRepository;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CandidateToEntityConverter implements IConverter<Candidate, CandidateEntity> {

    private final IConverter<Chat, ChatEntity> chatConverter;
    private final OrderRepository orderRepository;

    public CandidateToEntityConverter(OrderRepository orderRepository,
                                      IConverter<Chat, ChatEntity> chatConverter) {
        this.orderRepository = orderRepository;
        this.chatConverter = chatConverter;
    }

    @Override
    public CandidateEntity convert(@NonNull Candidate source) {
        // Convert candidate
        CandidateEntity candidate = CandidateEntity.builder()
                .id(UUID.fromString(String.valueOf(source.getId())))
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .isSelected(source.isSelected())
                .response(source.getResponse())
                .user(source.getUser() != null ? String.valueOf(source.getUser().getId()) : null)
                .build();

        // Safely convert&set chat
        Chat chat = source.getChat();
        if (chat != null) {
            @NonNull ChatEntity chatEntity = chatConverter.convert(chat);
            candidate.setChat(chatEntity);
        }

        // Safely convert&set parent order
        if (source.getOrderId() != null) {
            orderRepository.findById(source.getOrderId()).ifPresent(candidate::setOrder);
        }

        return candidate;
    }

    @Override
    public Class<Candidate> getSourceClass() {
        return Candidate.class;
    }

    @Override
    public Class<CandidateEntity> getTargetClass() {
        return CandidateEntity.class;
    }
}
