package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.order.Category;
import com.anyservice.entity.order.CategoryEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CategoryToEntityConverter implements IConverter<Category, CategoryEntity> {

    @Override
    public CategoryEntity convert(@NonNull Category source) {

        CategoryEntity target = CategoryEntity.builder()
                .title(source.getTitle())
                .description(source.getDescription())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .id(UUID.fromString(String.valueOf(source.getId())))
                .build();

        // Safely convert and set parent category
        if (source.getParentCategory() != null) {
            CategoryEntity parentCategory = convert(source.getParentCategory());
            target.setParentCategory(parentCategory);
        }

        return target;
    }

    @Override
    public Class<Category> getSourceClass() {
        return Category.class;
    }

    @Override
    public Class<CategoryEntity> getTargetClass() {
        return CategoryEntity.class;
    }
}
