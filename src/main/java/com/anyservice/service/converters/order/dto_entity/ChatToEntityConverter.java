package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.order.Chat;
import com.anyservice.entity.order.ChatEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ChatToEntityConverter implements IConverter<Chat, ChatEntity> {

    @Override
    public ChatEntity convert(@NonNull Chat source) {
        return ChatEntity.builder()
                .id(UUID.fromString(String.valueOf(source.getId())))
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .messages(source.getMessages())
                .build();
    }

    @Override
    public Class<Chat> getSourceClass() {
        return Chat.class;
    }

    @Override
    public Class<ChatEntity> getTargetClass() {
        return ChatEntity.class;
    }
}
