package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.order.Location;
import com.anyservice.entity.CountryEntity;
import com.anyservice.entity.order.LocationJson;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class LocationToEntityConverter implements IConverter<Location, LocationJson> {
    @Override
    public LocationJson convert(@NonNull Location source) {
        LocationJson location = LocationJson.builder()
                .city(source.getCity())
                .addressLine1(source.getAddressLine1())
                .addressLine2(source.getAddressLine2())
                .zipCode(source.getZipCode())
                .locationType(source.getLocationType())
                .build();

        CountryEntity country = source.getCountry();

        if (country != null) location.setCountryId(country.getId());

        return location;
    }

    @Override
    public Class<Location> getSourceClass() {
        return Location.class;
    }

    @Override
    public Class<LocationJson> getTargetClass() {
        return LocationJson.class;
    }
}
