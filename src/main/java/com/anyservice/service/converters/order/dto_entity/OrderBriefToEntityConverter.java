package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Category;
import com.anyservice.dto.order.Location;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.entity.order.CategoryEntity;
import com.anyservice.entity.order.LocationJson;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderBriefToEntityConverter implements IConverter<OrderBrief, OrderEntity> {

    private final IConverter<FileDetailed, FileEntity> fileConverter;
    private final IConverter<Location, LocationJson> locationConverter;
    private final IConverter<Category, CategoryEntity> categoryConverter;

    public OrderBriefToEntityConverter(IConverter<FileDetailed, FileEntity> fileConverter,
                                       IConverter<Location, LocationJson> locationConverter,
                                       IConverter<Category, CategoryEntity> categoryConverter) {
        this.fileConverter = fileConverter;
        this.locationConverter = locationConverter;
        this.categoryConverter = categoryConverter;
    }

    @Override
    public OrderEntity convert(@NonNull OrderBrief source) {
        OrderEntity target = OrderEntity.builder()
                .id(UUID.fromString(String.valueOf(source.getId())))
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .headline(source.getHeadline())
                .description(source.getDescription())
                .price(source.getPrice())
                .isDiscussablePrice(source.isDiscussablePrice())
                .currency(source.getCurrency())
                .deadline(source.getDeadline())
                .state(source.getState())
                .build();

        // Convert attachment
        FileDetailed mainAttachmentDto = source.getMainAttachment();

        if (mainAttachmentDto != null) {
            FileEntity fileEntity = fileConverter.convert(mainAttachmentDto);
            target.setMainAttachment(fileEntity);
        }

        // Convert location
        Location locationDto = source.getLocation();

        if (locationDto != null) {
            LocationJson location = locationConverter.convert(locationDto);
            target.setLocation(location);
        }

        // Convert categories
        List<Category> categoriesDto = source.getCategories();

        if (!CollectionUtils.isEmpty(categoriesDto)) {
            Set<@NonNull CategoryEntity> categories = categoriesDto.stream()
                    .map(categoryConverter::convert)
                    .collect(Collectors.toSet());

            target.setCategories(categories);
        }

        return target;
    }

    @Override
    public Class<OrderBrief> getSourceClass() {
        return OrderBrief.class;
    }

    @Override
    public Class<OrderEntity> getTargetClass() {
        return OrderEntity.class;
    }
}
