package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderDetailedToEntityConverter implements IConverter<OrderDetailed, OrderEntity> {

    private final IConverter<OrderBrief, OrderEntity> orderBriefToEntityConverter;
    private final IConverter<FileDetailed, FileEntity> fileConverter;
    private final IConverter<Candidate, CandidateEntity> candidateDtoToEntityConverter;

    public OrderDetailedToEntityConverter(IConverter<OrderBrief, OrderEntity> orderBriefToEntityConverter,
                                          IConverter<FileDetailed, FileEntity> fileConverter,
                                          IConverter<Candidate, CandidateEntity> candidateDtoToEntityConverter) {
        this.orderBriefToEntityConverter = orderBriefToEntityConverter;
        this.fileConverter = fileConverter;
        this.candidateDtoToEntityConverter = candidateDtoToEntityConverter;
    }

    @Override
    public OrderEntity convert(@NonNull OrderDetailed source) {

        // Create entity from brief
        OrderEntity target = orderBriefToEntityConverter.convert(source);

        target.setPhone(source.getPhone());

        // Safely convert&set customer
        UserBrief customer = source.getCustomer();

        if (customer != null) target.setCustomer(String.valueOf(customer.getId()));

        // Safely convert&set attachments
        List<FileDetailed> attachments = source.getAttachments();

        if (attachments != null && !attachments.isEmpty()) {
            List<FileEntity> files = attachments.stream()
                    .map(fileConverter::convert)
                    .collect(Collectors.toList());

            target.setAttachments(files);
        }

        // Safely convert&set candidates
        List<Candidate> candidates = source.getCandidates();

        if (candidates != null && !candidates.isEmpty()) {

            // Convert candidates
            List<CandidateEntity> candidatesList = candidates.stream()
                    .map(candidateDtoToEntityConverter::convert)
                    .collect(Collectors.toList());

            // Set to order
            target.setCandidates(candidatesList);
        }

        return target;
    }

    @Override
    public Class<OrderDetailed> getSourceClass() {
        return OrderDetailed.class;
    }

    @Override
    public Class<OrderEntity> getTargetClass() {
        return OrderEntity.class;
    }
}
