package com.anyservice.service.converters.order.dto_entity;

import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.Review;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.entity.order.ReviewEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ReviewToEntityConverter implements IConverter<Review, ReviewEntity> {

    private final IConverter<OrderBrief, OrderEntity> orderConverter;

    public ReviewToEntityConverter(IConverter<OrderBrief, OrderEntity> orderConverter) {
        this.orderConverter = orderConverter;
    }

    @Override
    public ReviewEntity convert(@NonNull Review source) {
        ReviewEntity target = ReviewEntity.builder()
                .id(UUID.fromString(String.valueOf(source.getId())))
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .header(source.getHeader())
                .body(source.getBody())
                .compliments(source.getCompliments())
                .contractorResponse(source.getContractorResponse())
                .rating(source.getRating())
                .author(source.getAuthor() != null ? String.valueOf(source.getAuthor().getId()) : null)
                .contractor(source.getContractor() != null ? String.valueOf(source.getContractor().getId()) : null)
                .build();

        // Safely convert & set order
        OrderBrief order = source.getOrder();
        if (order != null) {
            @NonNull OrderEntity orderEntity = orderConverter.convert(order);
            target.setOrder(orderEntity);
        }

        return target;
    }

    @Override
    public Class<Review> getSourceClass() {
        return Review.class;
    }

    @Override
    public Class<ReviewEntity> getTargetClass() {
        return ReviewEntity.class;
    }
}
