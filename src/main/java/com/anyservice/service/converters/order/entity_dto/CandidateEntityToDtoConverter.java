package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Chat;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.entity.order.ChatEntity;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.service.converters.api.IConverter;
import com.anyservice.service.user.api.IUserService;
import lombok.NonNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class CandidateEntityToDtoConverter implements IConverter<CandidateEntity, Candidate> {

    private final IConverter<ChatEntity, Chat> chatConverter;
    private final IUserService userService;

    public CandidateEntityToDtoConverter(IConverter<ChatEntity, Chat> chatConverter,
                                         @Lazy IUserService userService) {
        this.chatConverter = chatConverter;
        this.userService = userService;
    }

    @Override
    public Candidate convert(@NonNull CandidateEntity source) {
        Candidate target = Candidate.builder()
                .id(source.getId())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .response(source.getResponse())
                .isSelected(source.isSelected())
                .build();

        // set chat
        ChatEntity chatEntity = source.getChat();
        if (chatEntity != null) {
            @NonNull Chat chat = chatConverter.convert(chatEntity);
            target.setChat(chat);
        }

        // set candidate
        String userId = source.getUser();
        if (userId != null) {
            UserDetailed userBrief = userService.findById(userId).orElse(null);
            target.setUser(userBrief);
        }

        // set order uuid
        OrderEntity orderEntity = source.getOrder();
        if (orderEntity != null) {
            target.setOrderId(orderEntity.getId());
        }

        return target;
    }

    @Override
    public Class<CandidateEntity> getSourceClass() {
        return CandidateEntity.class;
    }

    @Override
    public Class<Candidate> getTargetClass() {
        return Candidate.class;
    }
}
