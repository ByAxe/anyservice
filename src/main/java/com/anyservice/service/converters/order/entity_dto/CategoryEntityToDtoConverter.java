package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.order.Category;
import com.anyservice.entity.order.CategoryEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class CategoryEntityToDtoConverter implements IConverter<CategoryEntity, Category> {

    @Override
    public Category convert(@NonNull CategoryEntity source) {

        Category target = Category.builder()
                .title(source.getTitle())
                .description(source.getDescription())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .id(source.getId())
                .build();

        // Safely convert and set parent category
        if (source.getParentCategory() != null) {
            Category parentCategory = convert(source.getParentCategory());
            target.setParentCategory(parentCategory);
        }

        return target;
    }

    @Override
    public Class<CategoryEntity> getSourceClass() {
        return CategoryEntity.class;
    }

    @Override
    public Class<Category> getTargetClass() {
        return Category.class;
    }
}
