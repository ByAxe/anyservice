package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.order.Chat;
import com.anyservice.entity.order.ChatEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class ChatEntityToDtoConverter implements IConverter<ChatEntity, Chat> {

    @Override
    public Chat convert(@NonNull ChatEntity source) {
        return Chat.builder()
                .id(source.getId())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .messages(source.getMessages())
                .build();
    }

    @Override
    public Class<ChatEntity> getSourceClass() {
        return ChatEntity.class;
    }

    @Override
    public Class<Chat> getTargetClass() {
        return Chat.class;
    }
}
