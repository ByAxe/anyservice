package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.order.Location;
import com.anyservice.entity.order.LocationJson;
import com.anyservice.repository.CountryRepository;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LocationEntityToDtoConverter implements IConverter<LocationJson, Location> {

    private final CountryRepository countryRepository;

    public LocationEntityToDtoConverter(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Location convert(@NonNull LocationJson source) {
        Location target = Location.builder()
                .city(source.getCity())
                .addressLine1(source.getAddressLine1())
                .addressLine2(source.getAddressLine2())
                .zipCode(source.getZipCode())
                .locationType(source.getLocationType())
                .build();

        UUID countryUuid = source.getCountryId();
        if (countryUuid != null) {
            countryRepository.findById(countryUuid).ifPresent(target::setCountry);
        }

        return target;
    }

    @Override
    public Class<LocationJson> getSourceClass() {
        return LocationJson.class;
    }

    @Override
    public Class<Location> getTargetClass() {
        return Location.class;
    }
}
