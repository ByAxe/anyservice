package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Category;
import com.anyservice.dto.order.Location;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.entity.order.CategoryEntity;
import com.anyservice.entity.order.LocationJson;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderEntityToBriefConverter implements IConverter<OrderEntity, OrderBrief> {

    private final IConverter<FileEntity, FileDetailed> fileConverter;
    private final IConverter<LocationJson, Location> locationConverter;
    private final IConverter<CategoryEntity, Category> categoryConverter;

    public OrderEntityToBriefConverter(IConverter<FileEntity, FileDetailed> fileConverter,
                                       IConverter<LocationJson, Location> locationConverter,
                                       IConverter<CategoryEntity, Category> categoryConverter) {
        this.fileConverter = fileConverter;
        this.locationConverter = locationConverter;
        this.categoryConverter = categoryConverter;
    }

    @Override
    public OrderBrief convert(@NonNull OrderEntity source) {
        OrderBrief target = OrderBrief.builder()
                .id(source.getId())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .headline(source.getHeadline())
                .currency(source.getCurrency())
                .description(source.getDescription())
                .price(source.getPrice())
                .isDiscussablePrice(source.isDiscussablePrice())
                .deadline(source.getDeadline())
                .state(source.getState())
                .build();


        // Convert main attachment
        FileEntity fileEntity = source.getMainAttachment();

        if (fileEntity != null) {
            FileDetailed fileDetailed = fileConverter.convert(fileEntity);
            target.setMainAttachment(fileDetailed);
        }

        // Convert location
        LocationJson location = source.getLocation();
        if (location != null) {
            Location locationDto = locationConverter.convert(location);
            target.setLocation(locationDto);
        }

        // Convert candidates
        List<CandidateEntity> candidates = source.getCandidates();
        if (candidates != null) {
            int responses = candidates.size();
            target.setResponses(responses);
        }

        // Convert categories
        Set<CategoryEntity> categories = source.getCategories();

        if (!CollectionUtils.isEmpty(categories)) {
            List<@NonNull Category> categoriesDto = categories.stream()
                    .map(categoryConverter::convert)
                    .collect(Collectors.toList());

            target.setCategories(categoriesDto);
        }

        return target;
    }

    @Override
    public Class<OrderEntity> getSourceClass() {
        return OrderEntity.class;
    }

    @Override
    public Class<OrderBrief> getTargetClass() {
        return OrderBrief.class;
    }
}
