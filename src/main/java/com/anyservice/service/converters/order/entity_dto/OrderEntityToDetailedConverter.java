package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.service.converters.api.IConverter;
import com.anyservice.service.user.api.IUserService;
import lombok.NonNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderEntityToDetailedConverter implements IConverter<OrderEntity, OrderDetailed> {

    private final IConverter<OrderEntity, OrderBrief> orderEntityToBriefConverter;
    private final IConverter<FileEntity, FileDetailed> fileEntityToDetailedConverter;
    private final IConverter<CandidateEntity, Candidate> candidateEntityToDtoConverter;
    private final IUserService userService;

    public OrderEntityToDetailedConverter(IConverter<OrderEntity, OrderBrief> orderEntityToBriefConverter,
                                          IConverter<FileEntity, FileDetailed> fileEntityToDetailedConverter,
                                          IConverter<CandidateEntity, Candidate> candidateEntityToDtoConverter,
                                          @Lazy IUserService userService) {
        this.orderEntityToBriefConverter = orderEntityToBriefConverter;
        this.fileEntityToDetailedConverter = fileEntityToDetailedConverter;
        this.candidateEntityToDtoConverter = candidateEntityToDtoConverter;
        this.userService = userService;
    }

    @Override
    public OrderDetailed convert(@NonNull OrderEntity source) {

        // Create detailed from brief
        OrderDetailed target = new OrderDetailed(orderEntityToBriefConverter.convert(source));

        target.setPhone(source.getPhone());

        // Safely convert&set customer
        String customerId = source.getCustomer();

        if (customerId != null) {
            UserBrief customer = userService.findById(customerId).orElse(null);
            target.setCustomer(customer);
        }

        // Safely convert&set attachments
        List<FileEntity> attachments = source.getAttachments();

        if (attachments != null && !attachments.isEmpty()) {
            List<FileDetailed> files = attachments.stream()
                    .map(fileEntityToDetailedConverter::convert)
                    .collect(Collectors.toList());

            target.setAttachments(files);
        }

        // Safely convert&set candidates
        List<CandidateEntity> candidates = source.getCandidates();

        if (candidates != null && !candidates.isEmpty()) {
            List<Candidate> candidatesList = candidates.stream()
                    .map(candidateEntityToDtoConverter::convert)
                    .collect(Collectors.toList());

            target.setCandidates(candidatesList);
        }

        return target;
    }

    @Override
    public Class<OrderEntity> getSourceClass() {
        return OrderEntity.class;
    }

    @Override
    public Class<OrderDetailed> getTargetClass() {
        return OrderDetailed.class;
    }
}
