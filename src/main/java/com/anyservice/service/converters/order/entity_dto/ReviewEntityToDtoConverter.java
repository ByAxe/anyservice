package com.anyservice.service.converters.order.entity_dto;

import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.Review;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.entity.order.ReviewEntity;
import com.anyservice.service.converters.api.IConverter;
import com.anyservice.service.user.api.IUserService;
import lombok.NonNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class ReviewEntityToDtoConverter implements IConverter<ReviewEntity, Review> {

    private final IConverter<OrderEntity, OrderBrief> orderConverter;
    private final IUserService userService;

    public ReviewEntityToDtoConverter(IConverter<OrderEntity, OrderBrief> orderConverter,
                                      @Lazy IUserService userService) {
        this.orderConverter = orderConverter;
        this.userService = userService;
    }

    @Override
    public Review convert(@NonNull ReviewEntity source) {
        Review target = Review.builder()
                .id(source.getId())
                .dtCreate(source.getDtCreate())
                .dtUpdate(source.getDtUpdate())
                .header(source.getHeader())
                .body(source.getBody())
                .compliments(source.getCompliments())
                .contractorResponse(source.getContractorResponse())
                .rating(source.getRating())
                .build();


        // Safely convert & set author
        String authorId = source.getAuthor();
        if (authorId != null) {
            UserBrief author = userService.findById(authorId).orElse(null);
            target.setAuthor(author);
        }

        // Safely convert & set contractor
        String contractorId = source.getContractor();
        if (contractorId != null) {
            UserBrief contractor = userService.findById(contractorId).orElse(null);
            target.setContractor(contractor);
        }

        // Safely convert & set order
        OrderEntity orderEntity = source.getOrder();
        if (orderEntity != null) {
            @NonNull OrderBrief order = orderConverter.convert(orderEntity);
            target.setOrder(order);
        }

        return target;
    }

    @Override
    public Class<ReviewEntity> getSourceClass() {
        return ReviewEntity.class;
    }

    @Override
    public Class<Review> getTargetClass() {
        return Review.class;
    }
}
