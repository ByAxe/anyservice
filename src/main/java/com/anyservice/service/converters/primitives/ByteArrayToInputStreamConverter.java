package com.anyservice.service.converters.primitives;

import com.anyservice.core.ByteArrayBearer;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class ByteArrayToInputStreamConverter implements IConverter<ByteArrayBearer, ByteArrayInputStream> {

    @Override
    public ByteArrayInputStream convert(@NonNull ByteArrayBearer source) {
        byte[] byteArray = source.getByteArray();

        return new ByteArrayInputStream(byteArray);
    }

    @Override
    public Class<ByteArrayBearer> getSourceClass() {
        return ByteArrayBearer.class;
    }

    @Override
    public Class<ByteArrayInputStream> getTargetClass() {
        return ByteArrayInputStream.class;
    }
}
