package com.anyservice.service.converters.primitives;

import com.anyservice.core.DateUtils;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Date;

@Service
public class GCTimestampToOffsetDateTimeConverter implements IConverter<Timestamp, OffsetDateTime> {

    @Value("${spring.zone.offset.hours}")
    private int zoneOffsetHours;

    @Override
    public OffsetDateTime convert(@NonNull Timestamp source) {
        Date date = source.toDate();

        return DateUtils.convertLongToOffsetDateTime(date.getTime(), zoneOffsetHours);
    }

    @Override
    public Class<Timestamp> getSourceClass() {
        return Timestamp.class;
    }

    @Override
    public Class<OffsetDateTime> getTargetClass() {
        return OffsetDateTime.class;
    }
}
