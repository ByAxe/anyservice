package com.anyservice.service.converters.primitives;

import com.anyservice.core.ByteArrayBearer;
import com.anyservice.core.Utils;
import com.anyservice.service.converters.api.IConverter;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class InputStreamToByteArrayConverter implements IConverter<ByteArrayInputStream, ByteArrayBearer> {

    @Override
    @SneakyThrows
    public ByteArrayBearer convert(@NonNull ByteArrayInputStream source) {
        final byte[] bytes = Utils.convertInputStreamToByteArray(source);

        return new ByteArrayBearer(bytes);
    }

    @Override
    public Class<ByteArrayInputStream> getSourceClass() {
        return ByteArrayInputStream.class;
    }

    @Override
    public Class<ByteArrayBearer> getTargetClass() {
        return ByteArrayBearer.class;
    }
}
