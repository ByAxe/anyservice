package com.anyservice.service.converters.primitives;

import com.anyservice.core.DateUtils;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Date;

@Service
public class OffsetDateTimeToGCTimestampConverter implements IConverter<OffsetDateTime, Timestamp> {
    @Override
    public Timestamp convert(@NonNull OffsetDateTime source) {
        Date date = DateUtils.convertOffsetDateTimeToDate(source);
        return Timestamp.of(date);
    }

    @Override
    public Class<OffsetDateTime> getSourceClass() {
        return OffsetDateTime.class;
    }

    @Override
    public Class<Timestamp> getTargetClass() {
        return Timestamp.class;
    }
}
