package com.anyservice.service.converters.user.dto_entity;

import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.firestore.User;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class UserBriefToUserConverter implements IConverter<UserBrief, User> {

    private final IConverter<OffsetDateTime, Timestamp> timestampConverter;

    public UserBriefToUserConverter(IConverter<OffsetDateTime, Timestamp> timestampConverter) {
        this.timestampConverter = timestampConverter;
    }

    @Override
    public User convert(@NonNull UserBrief source) {
        User target = User.builder()
                .id(String.valueOf(source.getId()))

                .email(source.getEmail())
                .state(source.getState())
                .role(source.getRole())
                .isVerified(source.isVerified())
                .isLegalStatusVerified(source.isLegalStatusVerified())

                .name(source.getName())
                .issuer(source.getIssuer())
                .picture(source.getPicture())
                .build();

        OffsetDateTime sourceDtCreate = source.getDtCreate();
        if (sourceDtCreate != null) {
            @NonNull Timestamp dtCreate = timestampConverter.convert(sourceDtCreate);
            target.setDtCreate(dtCreate);
        }

        OffsetDateTime sourceDtUpdate = source.getDtUpdate();
        if (sourceDtUpdate != null) {
            @NonNull Timestamp dtUpdate = timestampConverter.convert(sourceDtUpdate);
            target.setDtUpdate(dtUpdate);
        }

        return target;
    }

    @Override
    public Class<UserBrief> getSourceClass() {
        return UserBrief.class;
    }

    @Override
    public Class<User> getTargetClass() {
        return User.class;
    }
}
