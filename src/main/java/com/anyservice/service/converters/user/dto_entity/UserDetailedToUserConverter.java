package com.anyservice.service.converters.user.dto_entity;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.CountryEntity;
import com.anyservice.entity.firestore.User;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailedToUserConverter implements IConverter<UserDetailed, User> {

    private final IConverter<UserBrief, User> briefConverter;
    private final IConverter<OffsetDateTime, Timestamp> timestampConverter;
    private final IConverter<CountryEntity, String> countryConverter;
    private final IConverter<FileDetailed, String> fileConverter;

    public UserDetailedToUserConverter(IConverter<UserBrief, User> briefConverter,
                                       IConverter<OffsetDateTime, Timestamp> timestampConverter,
                                       IConverter<CountryEntity, String> countryConverter,
                                       IConverter<FileDetailed, String> fileConverter) {
        this.briefConverter = briefConverter;
        this.timestampConverter = timestampConverter;
        this.countryConverter = countryConverter;
        this.fileConverter = fileConverter;
    }


    @Override
    public User convert(@NonNull UserDetailed source) {
        User target = briefConverter.convert(source);

        OffsetDateTime sourcePasswordUpdateDate = source.getPasswordUpdateDate();
        if (sourcePasswordUpdateDate != null) {
            @NonNull Timestamp passwordUpdateDate = timestampConverter.convert(sourcePasswordUpdateDate);
            target.setPasswordUpdateDate(passwordUpdateDate);
        }

        target.setDescription(source.getDescription());
        target.setContacts(source.getContacts());
        target.setLegalStatus(source.getLegalStatus());
        target.setPasswordHash(source.getPassword());
        target.setAddresses(source.getAddresses());

        CountryEntity defaultCountry = source.getDefaultCountry();
        if (defaultCountry != null) {
            @NonNull String countryId = countryConverter.convert(defaultCountry);
            target.setDefaultCountryId(countryId);
        }

        FileDetailed profilePhoto = source.getProfilePhoto();
        if (profilePhoto != null) {
            @NonNull String fileId = fileConverter.convert(profilePhoto);
            target.setProfilePhotoId(fileId);
        }

        List<FileDetailed> documents = source.getDocuments();
        if (!CollectionUtils.isEmpty(documents)) {
            List<String> documentIds = documents.stream()
                    .map(fileConverter::convert)
                    .collect(Collectors.toList());

            target.setDocumentIds(documentIds);
        }

        List<FileDetailed> portfolio = source.getPortfolio();
        if (!CollectionUtils.isEmpty(portfolio)) {
            List<String> portfolioIds = portfolio.stream()
                    .map(fileConverter::convert)
                    .collect(Collectors.toList());

            target.setPortfolioIds(portfolioIds);
        }

        List<CountryEntity> countries = source.getListOfCountriesWhereServicesProvided();
        if (!CollectionUtils.isEmpty(countries)) {
            List<String> countryIds = countries.stream()
                    .map(countryConverter::convert)
                    .collect(Collectors.toList());

            target.setCountryIdsWhereServicesProvided(countryIds);
        }


        return target;
    }

    @Override
    public Class<UserDetailed> getSourceClass() {
        return UserDetailed.class;
    }

    @Override
    public Class<User> getTargetClass() {
        return User.class;
    }
}
