package com.anyservice.service.converters.user.entity_dto;

import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.firestore.User;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class UserToUserBriefConverter implements IConverter<User, UserBrief> {
    private final IConverter<Timestamp, OffsetDateTime> timestampConverter;

    public UserToUserBriefConverter(IConverter<Timestamp, OffsetDateTime> timestampConverter) {
        this.timestampConverter = timestampConverter;
    }

    @Override
    public UserBrief convert(@NonNull User source) {
        UserBrief target = UserBrief.builder()
                .id(source.getId())

                .email(source.getEmail())
                .name(source.getName())
                .issuer(source.getIssuer())
                .picture(source.getPicture())

                .role(source.getRole())
                .state(source.getState())
                .isVerified(source.isVerified())
                .isLegalStatusVerified(source.isLegalStatusVerified())
                .build();

        Timestamp sourceDtCreate = source.getDtCreate();
        if (sourceDtCreate != null) {
            @NonNull OffsetDateTime dtCreate = timestampConverter.convert(sourceDtCreate);
            target.setDtCreate(dtCreate);
        }

        Timestamp sourceDtUpdate = source.getDtUpdate();
        if (sourceDtUpdate != null) {
            @NonNull OffsetDateTime dtUpdate = timestampConverter.convert(sourceDtUpdate);
            target.setDtUpdate(dtUpdate);
        }

        return target;
    }

    @Override
    public Class<User> getSourceClass() {
        return User.class;
    }

    @Override
    public Class<UserBrief> getTargetClass() {
        return UserBrief.class;
    }
}
