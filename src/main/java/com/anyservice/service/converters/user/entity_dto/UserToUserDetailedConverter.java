package com.anyservice.service.converters.user.entity_dto;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.CountryEntity;
import com.anyservice.entity.firestore.User;
import com.anyservice.service.converters.api.IConverter;
import com.google.cloud.Timestamp;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service
public class UserToUserDetailedConverter implements IConverter<User, UserDetailed> {

    private final IConverter<Timestamp, OffsetDateTime> timestampConverter;
    private final IConverter<User, UserBrief> briefConverter;
    private final IConverter<String, CountryEntity> countryConverter;
    private final IConverter<String, FileDetailed> fileConverter;

    public UserToUserDetailedConverter(IConverter<Timestamp, OffsetDateTime> timestampConverter,
                                       IConverter<User, UserBrief> briefConverter,
                                       IConverter<String, CountryEntity> countryConverter,
                                       IConverter<String, FileDetailed> fileConverter) {
        this.timestampConverter = timestampConverter;
        this.briefConverter = briefConverter;
        this.countryConverter = countryConverter;
        this.fileConverter = fileConverter;
    }

    @Override
    public UserDetailed convert(@NonNull User source) {
        @NonNull UserDetailed target = new UserDetailed(briefConverter.convert(source));

        Timestamp sourcePasswordUpdateDate = source.getPasswordUpdateDate();
        if (sourcePasswordUpdateDate != null) {
            @NonNull OffsetDateTime passwordUpdateDate = timestampConverter.convert(sourcePasswordUpdateDate);
            target.setPasswordUpdateDate(passwordUpdateDate);
        }

        target.setDescription(source.getDescription());
        target.setContacts(source.getContacts());
        target.setLegalStatus(source.getLegalStatus());
        target.setPassword(source.getPasswordHash());
        target.setAddresses(source.getAddresses());

        if (isNotEmpty(source.getDefaultCountryId())) {
            @NonNull CountryEntity country = countryConverter.convert(source.getDefaultCountryId());
            target.setDefaultCountry(country);
        }

        if (isNotEmpty(source.getProfilePhotoId())) {
            @NonNull FileDetailed profilePhoto = fileConverter.convert(source.getProfilePhotoId());
            target.setProfilePhoto(profilePhoto);
        }

        if (!CollectionUtils.isEmpty(source.getDocumentIds())) {
            List<FileDetailed> documents = source.getDocumentIds()
                    .stream()
                    .map(fileConverter::convert)
                    .collect(Collectors.toList());

            target.setDocuments(documents);
        }

        if (!CollectionUtils.isEmpty(source.getPortfolioIds())) {
            List<FileDetailed> portfolio = source.getPortfolioIds()
                    .stream()
                    .map(fileConverter::convert)
                    .collect(Collectors.toList());

            target.setPortfolio(portfolio);
        }

        if (!CollectionUtils.isEmpty(source.getCountryIdsWhereServicesProvided())) {
            List<CountryEntity> countries = source.getCountryIdsWhereServicesProvided()
                    .stream()
                    .map(countryConverter::convert)
                    .collect(Collectors.toList());

            target.setListOfCountriesWhereServicesProvided(countries);
        }

        return target;
    }

    @Override
    public Class<User> getSourceClass() {
        return User.class;
    }

    @Override
    public Class<UserDetailed> getTargetClass() {
        return UserDetailed.class;
    }
}
