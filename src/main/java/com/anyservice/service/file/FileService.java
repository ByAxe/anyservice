package com.anyservice.service.file;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.entity.file.FileEntity;
import com.anyservice.repository.FileRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.file.api.AFileService;
import com.anyservice.service.file.api.IFileService;
import com.anyservice.service.file.storage.api.IStorageService;
import com.anyservice.service.validators.api.IFileValidator;
import com.google.cloud.storage.Blob;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class FileService extends AFileService implements IFileService {

    private final FileRepository fileRepository;
    private final MessageSource messageSource;
    private final CustomConversionService conversionService;
    private final IFileValidator fileValidator;
    private final IStorageService<Blob, FileDetailed> storageService;

    public FileService(FileRepository fileRepository, MessageSource messageSource,
                       CustomConversionService conversionService, IFileValidator fileValidator,
                       IStorageService<Blob, FileDetailed> storageService) {
        this.fileRepository = fileRepository;
        this.messageSource = messageSource;
        this.conversionService = conversionService;
        this.fileValidator = fileValidator;
        this.storageService = storageService;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return fileRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public FileDetailed create(FileDetailed file) {

        // Generate an identifier
        UUID uuid = UUID.randomUUID();

        file.setId(uuid);
        file.setDtCreate(OffsetDateTime.now());

        // Validate file
        Map<String, Object> errors = fileValidator.validateCreation(file);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }
        Blob blob;

        // Upload file to file storage
        try {
            blob = storageService.save(file);
        } catch (Exception e) {
            String message = messageSource.getMessage("file.cannot.upload",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new IllegalStateException(message, e);
        }

        // Set link to file
        file.setLink(blob.getMediaLink());

        // Save entity to database
        FileEntity entity = conversionService.convert(file, FileEntity.class);
        fileRepository.saveAndFlush(entity);

        return conversionService.convert(entity, FileDetailed.class);
    }

    @Override
    @Transactional
    public FileDetailed createFileViaLink(FileDetailed file) {
        // Generate an identifier
        file.setId(UUID.randomUUID());
        file.setDtCreate(OffsetDateTime.now());

        // Validate file
        Map<String, Object> errors = fileValidator.validateCreation(file);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Save entity to database
        FileEntity entity = conversionService.convert(file, FileEntity.class);
        fileRepository.saveAndFlush(entity);

        return conversionService.convert(entity, FileDetailed.class);
    }

    @Override
    @Transactional
    public FileDetailed update(FileDetailed file, UUID id, Date version) {
        // Check if such file exists
        if (!existsById(id)) {
            String message = messageSource.getMessage("file.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such category exists
        FileDetailed versionOfFileFromDB = findById(id).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfFileFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("file.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        file.setId(versionOfFileFromDB.getId());
        file.setDtCreate(versionOfFileFromDB.getDtCreate());

        // Validate file
        Map<String, Object> errors = fileValidator.validateUpdates(file);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert file to entity
        @NonNull FileEntity fileEntity = conversionService.convert(file, FileEntity.class);

        // Save file
        @NonNull FileEntity savedFileEntity = fileRepository.saveAndFlush(fileEntity);

        // Convert file back
        @NonNull FileDetailed savedFile = conversionService.convert(savedFileEntity, FileDetailed.class);

        // Return updated object to caller
        return savedFile;
    }

    @Override
    @SneakyThrows
    public Optional<FileDetailed> findById(UUID uuid) {
        // Find file description in repository
        Optional<FileEntity> entityOptional = fileRepository.findById(uuid);

        // Check if it's present
        if (entityOptional.isEmpty()) return Optional.empty();

        // Convert file get extract from Optional
        FileDetailed fileDetailed = entityOptional.map(e -> conversionService.convert(e, FileDetailed.class)).get();

        // Get actual file from storage
        InputStream inputStream = storageService.findByMetadata(fileDetailed);

        // Put it into object and return
        fileDetailed.setInputStream(inputStream);

        return Optional.of(fileDetailed);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void deleteById(UUID uuid, Date version) {
        // Check if such file exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("file.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We know for sure such file exists
        FileDetailed versionOfUserFromDB = fileRepository.findById(uuid)
                .map(e -> conversionService.convert(e, FileDetailed.class))
                .get();

        long createDate = convertOffsetDateTimeToMills(versionOfUserFromDB.getDtCreate());

        // Compare the versions of entities
        if (version.getTime() != createDate) {
            String message = messageSource.getMessage("delete.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // Delete file from storage
        storageService.delete(versionOfUserFromDB);

        // Delete file
        fileRepository.deleteById(uuid);
    }

}
