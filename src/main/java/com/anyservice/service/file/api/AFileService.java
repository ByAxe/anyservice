package com.anyservice.service.file.api;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class AFileService extends ACRUDService<FileBrief, FileDetailed, UUID, Date> {
    @Override
    protected Class<FileDetailed> getDetailedClass() {
        return FileDetailed.class;
    }

    @Override
    protected Class<FileBrief> getBriefClass() {
        return FileBrief.class;
    }
}
