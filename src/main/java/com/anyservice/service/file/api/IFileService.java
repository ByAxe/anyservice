package com.anyservice.service.file.api;

import com.anyservice.dto.file.FileBrief;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.api.ICRUDService;

import java.util.Date;
import java.util.UUID;

public interface IFileService extends ICRUDService<FileBrief, FileDetailed, UUID, Date> {

    /**
     * Create file without uploading it through backend
     *
     * @param file file with link
     * @return create file metadata
     */
    FileDetailed createFileViaLink(FileDetailed file);
}
