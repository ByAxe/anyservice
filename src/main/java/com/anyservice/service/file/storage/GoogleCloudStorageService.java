package com.anyservice.service.file.storage;

import com.anyservice.core.ByteArrayBearer;
import com.anyservice.core.Utils;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.file.storage.api.IStorageService;
import com.anyservice.service.file.storage.exceptions.StorageDeletionException;
import com.anyservice.service.file.storage.exceptions.StorageSavingException;
import com.anyservice.service.file.storage.exceptions.StorageSearchingException;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Service
public class GoogleCloudStorageService implements IStorageService<Blob, FileDetailed> {

    private final Storage storage;
    private final CustomConversionService conversionService;
    @Value("${spring.cloud.gcp.storage.bucket.name}")
    private String bucketName;

    public GoogleCloudStorageService(Storage storage, CustomConversionService conversionService) {
        this.storage = storage;
        this.conversionService = conversionService;
    }

    @Override
    public Blob save(@NonNull FileDetailed file) throws StorageSavingException {
        String fileName = buildFileName(file);

        // Convert input stream to byte array
        byte[] fileAsByteArray = Utils.convertInputStreamToByteArray(file.getInputStream());

        // Build a file metadata
        BlobInfo fileMetadata = BlobInfo.newBuilder(bucketName, fileName)
                .setContentType(file.getExtension().getContentType())
                .build();

        Blob blob;

        try {
            blob = storage.create(fileMetadata, fileAsByteArray);
        } catch (StorageException e) {
            throw new StorageSavingException(e);
        }

        return blob;
    }

    @Override
    @NonNull
    public InputStream findByMetadata(@NonNull FileDetailed metadata) throws StorageSearchingException {
        String fileName = buildFileName(metadata);

        // Get file and its metadata
        Blob blob;

        try {
            blob = storage.get(bucketName, fileName);
        } catch (StorageException e) {
            throw new StorageSearchingException(e);
        }

        // File was found
        if (blob != null) {
            // Get file as byte array
            byte[] fileAsByteArray = blob.getContent();

            return conversionService.convert(new ByteArrayBearer(fileAsByteArray), ByteArrayInputStream.class);
        } else {
            // If file wasn't found - return empty input stream
            return new ByteArrayInputStream(new byte[0]);
        }
    }

    @Override
    public void delete(@NonNull FileDetailed metadata) throws StorageDeletionException {
        String fileName = buildFileName(metadata);

        try {
            storage.delete(bucketName, fileName);
        } catch (StorageException e) {
            throw new StorageDeletionException(e);
        }
    }

    @Override
    public void deleteAll() throws StorageDeletionException {
        try {
            // Get all items from bucket
            Iterable<Blob> items = storage.list(bucketName).iterateAll();

            // Delete all items from bucket
            items.forEach(i -> storage.delete(i.getBlobId()));
        } catch (Exception e) {
            throw new StorageDeletionException(e);
        }
    }

    private String buildFileName(FileDetailed metadata) {
        return metadata.getId().toString();
    }
}
