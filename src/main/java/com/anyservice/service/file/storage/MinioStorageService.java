package com.anyservice.service.file.storage;

//@Service
public class MinioStorageService /*implements IStorageService<Void, FileDetailed> */ {
//
//    private final MinioService minioService;
//
//    public MinioStorageService(MinioService minioService) {
//        this.minioService = minioService;
//    }
//
//    @Override
//    public Void save(FileDetailed file) throws StorageSavingException {
//        UUID uuid = file.getUuid();
//
//        Path path = getPathToFile(file.getFileType(), uuid);
//
//        try {
//            minioService.upload(path, file.getInputStream(), file.getExtension().getContentType());
//        } catch (MinioException e) {
//            throw new StorageSavingException(e);
//        }
//
//        return null;
//    }
//
//    @Override
//    public InputStream findByMetadata(FileDetailed metadata) throws StorageSearchingException {
//        // Get path for the file
//        Path path = getPathToFile(metadata.getFileType(), metadata.getUuid());
//
//        InputStream inputStream;
//
//        // Get an actual file from storage
//        try {
//            inputStream = minioService.get(path);
//        } catch (MinioException e) {
//            throw new StorageSearchingException(e);
//        }
//
//        return inputStream;
//    }
//
//    @Override
//    public void delete(FileDetailed metadata) throws StorageDeletionException {
//
//        // Get path for the file
//        Path path = getPathToFile(metadata.getFileType(), metadata.getUuid());
//
//        // Delete from storage
//        try {
//            minioService.remove(path);
//        } catch (MinioException e) {
//            throw new StorageDeletionException(e);
//        }
//    }
//
//    /**
//     * Build a valid path for file
//     *
//     * @param fileType type of a file
//     * @param uuid     file identifier
//     * @return path {@link Path} to file
//     */
//    private Path getPathToFile(FileType fileType, UUID uuid) {
//        // Full path to file
//        String pathAsString = fileType.directory + "/" + uuid;
//
//        // Convert it to special object
//        return Paths.get(pathAsString);
//    }
}
