package com.anyservice.service.file.storage.api;

import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.file.storage.exceptions.StorageDeletionException;
import com.anyservice.service.file.storage.exceptions.StorageSavingException;
import com.anyservice.service.file.storage.exceptions.StorageSearchingException;
import lombok.NonNull;

import java.io.InputStream;

/**
 * Abstraction for different storage options (amazon, google)
 *
 * @param <R> Result of save operation
 * @param <M> Metadata about stored object
 */
public interface IStorageService<R, M> {
    R save(@NonNull FileDetailed file) throws StorageSavingException;

    @NonNull InputStream findByMetadata(@NonNull M metadata) throws StorageSearchingException;

    void delete(@NonNull M metadata) throws StorageDeletionException;

    void deleteAll() throws StorageDeletionException;
}
