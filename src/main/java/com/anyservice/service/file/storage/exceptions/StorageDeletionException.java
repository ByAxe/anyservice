package com.anyservice.service.file.storage.exceptions;

import com.anyservice.service.file.storage.exceptions.api.StorageException;

public class StorageDeletionException extends StorageException {
    public StorageDeletionException() {
        super();
    }

    public StorageDeletionException(String message) {
        super(message);
    }

    public StorageDeletionException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageDeletionException(Throwable cause) {
        super(cause);
    }

    protected StorageDeletionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
