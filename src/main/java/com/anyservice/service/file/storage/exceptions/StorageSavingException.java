package com.anyservice.service.file.storage.exceptions;

import com.anyservice.service.file.storage.exceptions.api.StorageException;

public class StorageSavingException extends StorageException {
    public StorageSavingException() {
        super();
    }

    public StorageSavingException(String message) {
        super(message);
    }

    public StorageSavingException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageSavingException(Throwable cause) {
        super(cause);
    }

    protected StorageSavingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
