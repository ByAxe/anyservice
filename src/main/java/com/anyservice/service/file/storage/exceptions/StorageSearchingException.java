package com.anyservice.service.file.storage.exceptions;

import com.anyservice.service.file.storage.exceptions.api.StorageException;

public class StorageSearchingException extends StorageException {
    public StorageSearchingException() {
        super();
    }

    public StorageSearchingException(String message) {
        super(message);
    }

    public StorageSearchingException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageSearchingException(Throwable cause) {
        super(cause);
    }

    protected StorageSearchingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
