package com.anyservice.service.order;

import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.entity.order.CandidateEntity;
import com.anyservice.repository.CandidateRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.ACandidateService;
import com.anyservice.service.order.api.ICandidateService;
import com.anyservice.service.order.api.IOrderService;
import com.anyservice.service.validators.api.ICandidateValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.OffsetDateTime;
import java.util.*;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToDate;
import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class CandidateService extends ACandidateService implements ICandidateService {

    private final CandidateRepository candidateRepository;
    private final ICandidateValidator candidateValidator;
    private final CustomConversionService conversionService;
    private final IOrderService orderService;
    private final MessageSource messageSource;

    public CandidateService(CandidateRepository candidateRepository, MessageSource messageSource,
                            CustomConversionService conversionService, ICandidateValidator candidateValidator,
                            OrderService orderService) {
        this.candidateRepository = candidateRepository;
        this.messageSource = messageSource;
        this.conversionService = conversionService;
        this.candidateValidator = candidateValidator;
        this.orderService = orderService;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return candidateRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public Candidate create(Candidate candidate) {
        // Validate category
        final Map<String, Object> errors = candidateValidator.validateCreation(candidate);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        OffsetDateTime now = OffsetDateTime.now();

        candidate.setDtCreate(now);
        candidate.setDtUpdate(now);

        // Convert candidate to entity
        @NonNull CandidateEntity candidateEntity = conversionService.convert(candidate, CandidateEntity.class);

        // Save candidate
        @NonNull CandidateEntity savedCandidateEntity = candidateRepository.saveAndFlush(candidateEntity);

        // Convert candidate back
        @NonNull Candidate savedCandidate = conversionService.convert(savedCandidateEntity, Candidate.class);

        updateCorrespondingOrder(savedCandidate);

        // Return saved object to caller
        return savedCandidate;
    }

    @Override
    @Transactional
    public Candidate update(Candidate candidate, UUID uuid, Date version) {
        // Check if such candidate exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("candidate.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such candidate exists
        Candidate versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("candidate.update.version",
                    null, LocaleContextHolder.getLocale());
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        // Set all the system fields
        candidate.setId(versionOfOrderFromDB.getId());
        candidate.setDtCreate(versionOfOrderFromDB.getDtCreate());
        candidate.setDtUpdate(OffsetDateTime.now());

        // Validate candidate
        Map<String, Object> errors = candidateValidator.validateUpdates(candidate);

        if (!errors.isEmpty()) {
            log.error(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert candidate to entity
        @NonNull CandidateEntity candidateEntity = conversionService.convert(candidate, CandidateEntity.class);

        // Save candidate
        @NonNull CandidateEntity savedCandidateEntity = candidateRepository.saveAndFlush(candidateEntity);

        // Convert candidate back
        @NonNull Candidate savedCandidate = conversionService.convert(savedCandidateEntity, Candidate.class);

        updateCorrespondingOrder(savedCandidate);

        // Return updated object to caller
        return savedCandidate;
    }

    /**
     * Update order, according to update of this candidate
     *
     * @param candidate
     */
    private void updateCorrespondingOrder(@NonNull Candidate candidate) {
        //noinspection OptionalGetWithoutIsPresent because we already validated presence before
        final OrderDetailed order = orderService.findById(candidate.getOrderId()).get();

        // Add this candidate to an order
        final List<Candidate> candidates = order.getCandidates();
        if (!CollectionUtils.isEmpty(candidates)) {

            final Optional<Candidate> optionalCandidate = candidates.stream()
                    .filter(c -> c.getId().equals(candidate.getId()))
                    .findFirst();

            // If candidate is already in list, replace it with new version
            optionalCandidate.ifPresent(candidates::remove);

            candidates.add(candidate);
        } else {
            order.setCandidates(Collections.singletonList(candidate));
            order.setResponses(order.getResponses() + 1);
        }

        // Update corresponding order state if this candidate is selected
        if (candidate.isSelected()) {
            order.setState(OrderState.CANDIDATE_SELECTED);
        }

        orderService.update(order, (UUID) order.getId(), convertOffsetDateTimeToDate(order.getDtUpdate()));
    }

    @Override
    @Transactional
    public void deleteById(UUID uuid, Date version) {
        // Check if such candidate exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("candidate.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such candidate exists
        Candidate versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("candidate.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Delete candidate
        candidateRepository.deleteById(uuid);
    }
}
