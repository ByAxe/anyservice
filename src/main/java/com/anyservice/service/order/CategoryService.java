package com.anyservice.service.order;

import com.anyservice.dto.order.Category;
import com.anyservice.entity.order.CategoryEntity;
import com.anyservice.repository.CategoryRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.ACategoryService;
import com.anyservice.service.order.api.ICategoryService;
import com.anyservice.service.validators.api.ICategoryValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class CategoryService extends ACategoryService implements ICategoryService {

    private final CategoryRepository categoryRepository;
    private final CustomConversionService conversionService;
    private final ICategoryValidator categoryValidator;
    private final MessageSource messageSource;

    public CategoryService(CategoryRepository categoryRepository, CustomConversionService conversionService,
                           ICategoryValidator categoryValidator, MessageSource messageSource) {
        this.categoryRepository = categoryRepository;
        this.conversionService = conversionService;
        this.categoryValidator = categoryValidator;
        this.messageSource = messageSource;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return categoryRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public Category create(@NonNull Category category) {

        // Validate category
        Map<String, Object> errors = categoryValidator.validateCreation(category);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        OffsetDateTime now = OffsetDateTime.now();

        category.setDtCreate(now);
        category.setDtUpdate(now);

        // Convert category to entity
        @NonNull CategoryEntity categoryEntity = conversionService.convert(category, CategoryEntity.class);

        // Save category
        @NonNull CategoryEntity savedCategoryEntity = categoryRepository.saveAndFlush(categoryEntity);

        // Convert category back
        @NonNull Category savedCategory = conversionService.convert(savedCategoryEntity, Category.class);

        // Return saved object to caller
        return savedCategory;
    }

    @Override
    @Transactional
    public Category update(Category category, UUID uuid, Date version) {
        // Check if such category exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("category.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such category exists
        Category versionOfCategoryFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfCategoryFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("category.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        category.setId(versionOfCategoryFromDB.getId());
        category.setDtCreate(versionOfCategoryFromDB.getDtCreate());
        category.setDtUpdate(OffsetDateTime.now());

        // Validate category
        Map<String, Object> errors = categoryValidator.validateUpdates(category);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert category to entity
        @NonNull CategoryEntity categoryEntity = conversionService.convert(category, CategoryEntity.class);

        // Save category
        @NonNull CategoryEntity savedCategoryEntity = categoryRepository.saveAndFlush(categoryEntity);

        // Convert category back
        @NonNull Category savedCategory = conversionService.convert(savedCategoryEntity, Category.class);

        // Return updated object to caller
        return savedCategory;
    }

    @Override
    @Transactional
    public void deleteById(UUID uuid, Date version) {
        // Check if such category exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("category.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such category exists
        Category versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("category.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Delete category
        categoryRepository.deleteById(uuid);
    }
}
