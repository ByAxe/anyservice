package com.anyservice.service.order;

import com.anyservice.dto.order.Chat;
import com.anyservice.entity.order.ChatEntity;
import com.anyservice.repository.ChatRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.AChatService;
import com.anyservice.service.order.api.IChatService;
import com.anyservice.service.validators.api.IChatValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class ChatService extends AChatService implements IChatService {

    private final MessageSource messageSource;
    private final CustomConversionService conversionService;
    private final IChatValidator chatValidator;
    private final ChatRepository chatRepository;

    public ChatService(MessageSource messageSource, CustomConversionService conversionService,
                       IChatValidator chatValidator, ChatRepository chatRepository) {
        this.messageSource = messageSource;
        this.conversionService = conversionService;
        this.chatValidator = chatValidator;
        this.chatRepository = chatRepository;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return chatRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public Chat create(Chat chat) {
        // Validate chat
        Map<String, Object> errors = chatValidator.validateCreation(chat);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Set required fields
        OffsetDateTime now = OffsetDateTime.now();

        chat.setDtCreate(now);
        chat.setDtUpdate(now);

        // Convert it to entity
        @NonNull ChatEntity chatEntity = conversionService.convert(chat, ChatEntity.class);

        // Save entity
        @NonNull ChatEntity savedEntity = chatRepository.saveAndFlush(chatEntity);

        // Convert saved back
        @NonNull Chat savedChat = conversionService.convert(savedEntity, Chat.class);

        // Return saved dto to caller
        return savedChat;
    }

    @Override
    @Transactional
    public Chat update(Chat chat, UUID uuid, Date version) {
        // Check if such chat exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("chat.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such chat exists
        Chat versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("chat.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        chat.setId(versionOfOrderFromDB.getId());
        chat.setDtCreate(versionOfOrderFromDB.getDtCreate());
        chat.setDtUpdate(OffsetDateTime.now());

        // Validate chat
        Map<String, Object> errors = chatValidator.validateUpdates(chat);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert it to entity
        @NonNull ChatEntity chatEntity = conversionService.convert(chat, ChatEntity.class);

        // Save entity
        @NonNull ChatEntity savedEntity = chatRepository.saveAndFlush(chatEntity);

        // Convert saved back
        @NonNull Chat savedChat = conversionService.convert(savedEntity, Chat.class);

        // Return updated object to caller
        return savedChat;
    }

    @Override
    @Transactional
    public void deleteById(UUID uuid, Date version) {
        // Check if such chat exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("chat.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such chat exists
        Chat versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("chat.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Delete chat
        chatRepository.deleteById(uuid);
    }
}
