package com.anyservice.service.order;

import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.repository.OrderRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.AOrderService;
import com.anyservice.service.order.api.IOrderService;
import com.anyservice.service.validators.api.IOrderValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class OrderService extends AOrderService implements IOrderService {

    private final MessageSource messageSource;
    private final CustomConversionService conversionService;
    private final OrderRepository orderRepository;
    private final IOrderValidator orderValidator;

    public OrderService(MessageSource messageSource, CustomConversionService conversionService,
                        OrderRepository orderRepository, IOrderValidator orderValidator) {
        this.messageSource = messageSource;
        this.conversionService = conversionService;
        this.orderRepository = orderRepository;
        this.orderValidator = orderValidator;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return orderRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public OrderDetailed create(OrderDetailed dto) {

        // Validate order
        Map<String, Object> errors = orderValidator.validateCreation(dto);

        if (!errors.isEmpty()) {
            log.error(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Set required fields`
        UUID uuid = UUID.randomUUID();
        dto.setId(uuid);

        OffsetDateTime now = OffsetDateTime.now();

        dto.setDtCreate(now);
        dto.setDtUpdate(now);

        // Convert it to entity
        @NonNull OrderEntity orderEntity = conversionService.convert(dto, OrderEntity.class);

        // Save entity
        @NonNull OrderEntity savedEntity = orderRepository.saveAndFlush(orderEntity);

        // Convert saved back
        @NonNull OrderDetailed savedOrder = conversionService.convert(savedEntity, OrderDetailed.class);

        // Return saved dto to caller
        return savedOrder;
    }

    @Override
    @Transactional
    public OrderDetailed update(OrderDetailed order, UUID uuid, Date version) {

        // Check if such order exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("order.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such order exists
        OrderDetailed versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("order.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        order.setId(versionOfOrderFromDB.getId());
        order.setDtCreate(versionOfOrderFromDB.getDtCreate());
        order.setDtUpdate(OffsetDateTime.now());

        OrderState oldState = versionOfOrderFromDB.getState();
        OrderState newState = order.getState();

        // Validate order
        Map<String, Object> errors = orderValidator.validateUpdates(order, oldState, newState);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert it to entity
        @NonNull OrderEntity orderEntity = conversionService.convert(order, OrderEntity.class);

        // Save entity
        @NonNull OrderEntity savedEntity = orderRepository.saveAndFlush(orderEntity);

        // Convert saved back
        @NonNull OrderDetailed updatedOrder = conversionService.convert(savedEntity, OrderDetailed.class);

        // Return saved dto to caller
        return updatedOrder;
    }

    @Override
    @Transactional
    public void deleteById(UUID uuid, Date version) {

        // Check if such order exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("order.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such order exists
        OrderDetailed versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("order.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Delete user
        orderRepository.deleteById(uuid);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<OrderBrief> findAll(@Nullable Specification specification) {
        // Made here to easily remove issue with conversion from DTO to Entity typed specifications
        final Specification<OrderEntity> entitySpecification = specification;

        final List<OrderBrief> orders = orderRepository.findAll(entitySpecification).stream()
                .map(c -> conversionService.convert(c, OrderBrief.class))
                .collect(Collectors.toList());

        return orders;
    }

}
