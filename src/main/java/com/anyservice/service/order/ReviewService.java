package com.anyservice.service.order;

import com.anyservice.dto.order.Review;
import com.anyservice.entity.order.ReviewEntity;
import com.anyservice.repository.ReviewRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.AReviewService;
import com.anyservice.service.order.api.IReviewService;
import com.anyservice.service.validators.api.IReviewValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;

@Service
@Transactional(readOnly = true)
@Slf4j
public class ReviewService extends AReviewService implements IReviewService {

    private final ReviewRepository reviewRepository;
    private final CustomConversionService conversionService;
    private final IReviewValidator reviewValidator;
    private final MessageSource messageSource;

    public ReviewService(ReviewRepository reviewRepository, CustomConversionService conversionService,
                         IReviewValidator reviewValidator, MessageSource messageSource) {
        this.reviewRepository = reviewRepository;
        this.conversionService = conversionService;
        this.reviewValidator = reviewValidator;
        this.messageSource = messageSource;
    }

    @Override
    protected JpaRepository<?, UUID> getRepository() {
        return reviewRepository;
    }

    @Override
    protected CustomConversionService getConversionService() {
        return conversionService;
    }

    @Override
    @Transactional
    public Review create(Review review) {
        // Validate review
        Map<String, Object> errors = reviewValidator.validateCreation(review);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        OffsetDateTime now = OffsetDateTime.now();

        UUID uuid = UUID.randomUUID();
        review.setId(uuid);

        review.setDtCreate(now);
        review.setDtUpdate(now);

        // Convert review to entity
        @NonNull ReviewEntity reviewEntity = conversionService.convert(review, ReviewEntity.class);

        // Save review
        @NonNull ReviewEntity savedReviewEntity = reviewRepository.saveAndFlush(reviewEntity);

        // Convert review back
        @NonNull Review savedReview = conversionService.convert(savedReviewEntity, Review.class);

        // Return saved object to caller
        return savedReview;
    }

    @Override
    @Transactional
    public Review update(Review review, UUID uuid, Date version) {

        // Check if such review exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("review.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such review exists
        Review versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("review.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        review.setId(versionOfOrderFromDB.getId());
        review.setDtCreate(versionOfOrderFromDB.getDtCreate());
        review.setDtUpdate(OffsetDateTime.now());

        // Validate review
        Map<String, Object> errors = reviewValidator.validateUpdates(review);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Convert review to entity
        @NonNull ReviewEntity categoryEntity = conversionService.convert(review, ReviewEntity.class);

        // Save review
        @NonNull ReviewEntity savedCategoryEntity = reviewRepository.saveAndFlush(categoryEntity);

        // Convert review back
        @NonNull Review savedCategory = conversionService.convert(savedCategoryEntity, Review.class);

        // Return updated object to caller
        return savedCategory;
    }

    @Override
    @Transactional
    public void deleteById(UUID uuid, Date version) {
        // Check if such review exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage("review.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We already know that such review exists
        Review versionOfOrderFromDB = findById(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfOrderFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("review.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Delete review
        reviewRepository.deleteById(uuid);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public List<Review> findAll(Specification specification) {
        // Made here to easily remove issue with conversion from DTO to Entity typed specifications
        final Specification<ReviewEntity> entitySpecification = specification;

        final List<Review> reviews = reviewRepository.findAll(entitySpecification).stream()
                .map(c -> conversionService.convert(c, Review.class))
                .collect(Collectors.toList());

        return reviews;
    }
}
