package com.anyservice.service.order.api;

import com.anyservice.dto.order.Candidate;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class ACandidateService extends ACRUDService<Candidate, Candidate, UUID, Date> {
    @Override
    protected Class<Candidate> getDetailedClass() {
        return Candidate.class;
    }

    @Override
    protected Class<Candidate> getBriefClass() {
        return Candidate.class;
    }
}
