package com.anyservice.service.order.api;

import com.anyservice.dto.order.Category;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class ACategoryService extends ACRUDService<Category, Category, UUID, Date> {
    @Override
    protected Class<Category> getDetailedClass() {
        return Category.class;
    }

    @Override
    protected Class<Category> getBriefClass() {
        return Category.class;
    }
}
