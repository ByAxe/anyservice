package com.anyservice.service.order.api;

import com.anyservice.dto.order.Chat;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class AChatService extends ACRUDService<Chat, Chat, UUID, Date> {
    @Override
    protected Class<Chat> getDetailedClass() {
        return Chat.class;
    }

    @Override
    protected Class<Chat> getBriefClass() {
        return Chat.class;
    }
}
