package com.anyservice.service.order.api;

import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class AOrderService extends ACRUDService<OrderBrief, OrderDetailed, UUID, Date> {

    @Override
    protected Class<OrderDetailed> getDetailedClass() {
        return OrderDetailed.class;
    }

    @Override
    protected Class<OrderBrief> getBriefClass() {
        return OrderBrief.class;
    }
}
