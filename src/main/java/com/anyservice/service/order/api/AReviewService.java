package com.anyservice.service.order.api;

import com.anyservice.dto.order.Review;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class AReviewService extends ACRUDService<Review, Review, UUID, Date> {
    @Override
    protected Class<Review> getDetailedClass() {
        return Review.class;
    }

    @Override
    protected Class<Review> getBriefClass() {
        return Review.class;
    }
}
