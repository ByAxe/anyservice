package com.anyservice.service.order.api;

import com.anyservice.dto.order.Candidate;
import com.anyservice.service.api.ICRUDService;

import java.util.Date;
import java.util.UUID;

public interface ICandidateService extends ICRUDService<Candidate, Candidate, UUID, Date> {
}
