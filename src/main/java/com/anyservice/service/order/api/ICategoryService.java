package com.anyservice.service.order.api;

import com.anyservice.dto.order.Category;
import com.anyservice.service.api.ICRUDService;

import java.util.Date;
import java.util.UUID;

public interface ICategoryService extends ICRUDService<Category, Category, UUID, Date> {
}
