package com.anyservice.service.order.api;

import com.anyservice.dto.order.Chat;
import com.anyservice.service.api.ICRUDService;

import java.util.Date;
import java.util.UUID;

public interface IChatService extends ICRUDService<Chat, Chat, UUID, Date> {
}
