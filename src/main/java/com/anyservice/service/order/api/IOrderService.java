package com.anyservice.service.order.api;

import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.service.api.ICRUDService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface IOrderService extends ICRUDService<OrderBrief, OrderDetailed, UUID, Date> {

    List<OrderBrief> findAll(@Nullable Specification<OrderDetailed> specification);

}
