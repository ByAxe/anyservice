package com.anyservice.service.order.api;

import com.anyservice.dto.order.Review;
import com.anyservice.service.api.ICRUDService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface IReviewService extends ICRUDService<Review, Review, UUID, Date> {

    List<Review> findAll(@Nullable Specification<Review> specification);
}
