package com.anyservice.service.user;

import com.anyservice.core.enums.UserState;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.user.api.IFirebaseUserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.stereotype.Service;

import static com.google.firebase.auth.UserRecord.CreateRequest;
import static com.google.firebase.auth.UserRecord.UpdateRequest;

@Service
public class FirebaseUserService implements IFirebaseUserService<UserDetailed> {
    private final FirebaseAuth firebase = FirebaseAuth.getInstance();

    @Override
    public UserRecord create(UserDetailed user) throws FirebaseAuthException {
        CreateRequest request = new CreateRequest()
                .setEmail(user.getEmail())
                .setEmailVerified(false)
                .setPassword(user.getPassword())
                .setDisplayName(user.getName())
//                .setDisabled(true); TODO temporarily changed
                .setDisabled(false);

        return firebase.createUser(request);
    }

    @Override
    public UserRecord update(UserDetailed user) throws FirebaseAuthException {
        UpdateRequest request = new UpdateRequest(String.valueOf(user.getId()))
                .setEmail(user.getEmail())
                .setEmailVerified(user.isVerified())
                .setDisplayName(user.getName())
                .setDisabled(user.getState() != UserState.ACTIVE);

        return firebase.updateUser(request);
    }

    @Override
    public UserRecord updatePassword(String uid, String password) throws FirebaseAuthException {
        UpdateRequest request = new UpdateRequest(uid)
                .setPassword(password);

        return firebase.updateUser(request);
    }

    @Override
    public void delete(String uid) {
        firebase.deleteUserAsync(uid);
    }
}
