package com.anyservice.service.user;

import com.anyservice.core.enums.UserRole;
import com.anyservice.core.enums.UserState;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.dto.user.UserForChangePassword;
import com.anyservice.entity.firestore.User;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.service.aop.markers.RemovePasswordFromReturningValue;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.file.api.IFileService;
import com.anyservice.service.user.api.IFirebaseUserService;
import com.anyservice.service.user.api.IPasswordService;
import com.anyservice.service.user.api.IUserService;
import com.anyservice.service.user.api.IUserVerificationService;
import com.anyservice.service.validators.api.IUserValidator;
import com.anyservice.web.security.exceptions.WrongPasswordException;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.anyservice.core.DateUtils.convertOffsetDateTimeToDate;
import static com.anyservice.core.DateUtils.convertOffsetDateTimeToMills;
import static com.anyservice.core.enums.UserState.ACTIVE;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Service
@Slf4j
public class UserService implements IUserService {

    private static final String USER_NOT_EXISTS_EXCEPTION_CODE = "user.not.exists";

    private final IUserValidator userValidator;
    private final IPasswordService passwordService;
    private final MessageSource messageSource;
    private final IFileService fileService;
    private final IUserVerificationService<UserDetailed, UUID> verificationService;
    private final IFirestoreRepository firestoreRepository;
    private final IFirebaseUserService<UserDetailed> firebaseUserService;
    private final CustomConversionService conversionService;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String collection;

    @Value("${spring.cloud.gcp.token.claims.provider.key}")
    private String userProviderKey;

    @Value("${spring.cloud.gcp.token.claims.provider.anonymous}")
    private String anonymousUserValue;

    public UserService(IUserValidator userValidator, IPasswordService passwordService,
                       MessageSource messageSource, IFileService fileService,
                       IUserVerificationService<UserDetailed, UUID> verificationService,
                       IFirestoreRepository firestoreRepository,
                       IFirebaseUserService<UserDetailed> firebaseUserService,
                       CustomConversionService conversionService) {
        this.userValidator = userValidator;
        this.passwordService = passwordService;
        this.messageSource = messageSource;
        this.fileService = fileService;
        this.verificationService = verificationService;
        this.firestoreRepository = firestoreRepository;
        this.firebaseUserService = firebaseUserService;
        this.conversionService = conversionService;
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed create(FirebaseToken decodedToken) {
        // build user from firebase token
        UserDetailed user = buildUserFromDecodedToken(decodedToken);

        // Set required fields to user
        setAdditionalFieldsToUser(user);


        // TODO changed temporarily to test application
        user.setState(ACTIVE);
        // Set user state, based on verification status
//        if (decodedToken.isEmailVerified()) {
//            user.setState(ACTIVE);
//        } else if (isNotAnonymous(decodedToken)) {
//            // Start verification process for user
//            verificationService.startVerification(user);
//        }

        // Save user to db
        convertAndSaveUser(user);

        return user;
    }

    private boolean isNotAnonymous(FirebaseToken decodedToken) {
        return !anonymousUserValue.equals(decodedToken.getClaims().get(userProviderKey));
    }

    private UserDetailed buildUserFromDecodedToken(FirebaseToken decodedToken) {
        return UserDetailed.builder()
                .id(decodedToken.getUid())
                .email(decodedToken.getEmail())
                .issuer(decodedToken.getIssuer())
                .name(decodedToken.getName())
                .picture(decodedToken.getPicture())
                .isVerified(decodedToken.isEmailVerified())
                .build();
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed create(UserDetailed user) {
        // Validate user
        Map<String, Object> errors = userValidator.validateCreation(user);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Set system fields
        setAdditionalFieldsToUser(user);

        // TODO changed temporarily to test application
        // user.setState(WAITING);
//         user.setVerified(false);
        user.setState(ACTIVE);
        user.setVerified(true);

        UserRecord userRecord;
        try {
            userRecord = firebaseUserService.create(user);
        } catch (FirebaseAuthException e) {
            String message = messageSource.getMessage("user.save",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new RuntimeException(message, e);
        }
        String uid = userRecord.getUid();

        user.setId(uid);

        // Hash&Set password to user
        String hash = passwordService.hash(user.getPassword());
        user.setPassword(hash);

        // Create user in custom firestore collection
        convertAndSaveUser(user);

        // TODO changed temporarily to test application
        // verify user
        // verificationService.startVerification(user);

        return user;
    }

    private void convertAndSaveUser(UserDetailed user) {
        try {
            @NonNull User userEntity = conversionService.convert(user, User.class);
            String key = userEntity.getId();

            firestoreRepository.saveSync(collection, key, userEntity);
        } catch (Exception e) {
            String message = messageSource.getMessage("user.save",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new RuntimeException(message, e);
        }
    }

    /**
     * Set to {@link UserDetailed} all system required fields for successful creation
     *
     * @param user filled {@link UserDetailed} for creation
     */
    private void setAdditionalFieldsToUser(UserDetailed user) {
        OffsetDateTime now = OffsetDateTime.now();

        user.setDtCreate(now);
        user.setDtUpdate(now);

        user.setPasswordUpdateDate(now);

        // By default, everyone is a user
        user.setRole(UserRole.ROLE_USER);
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed update(UserDetailed user, String id, Date version) {
        // Check if such user exists
        if (!existsById(id)) {
            String message = messageSource.getMessage(USER_NOT_EXISTS_EXCEPTION_CODE,
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We know for sure such user exists
        UserDetailed versionOfUserFromDB = findByIdWithPassword(id).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfUserFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("user.update.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new NullPointerException(message);
        }

        // Set all the system fields
        user.setId(versionOfUserFromDB.getId());
        user.setDtCreate(versionOfUserFromDB.getDtCreate());
        user.setPasswordUpdateDate(versionOfUserFromDB.getPasswordUpdateDate());
        user.setDtUpdate(OffsetDateTime.now());

        // For example - role and state of user should not be updated with this method
        user.setRole(versionOfUserFromDB.getRole());
        user.setState(versionOfUserFromDB.getState());

        // Validate user
        Map<String, Object> errors = userValidator.validateUpdates(user);

        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Set password hash from the DB (change password operation not allowed in this method)
        user.setPassword(versionOfUserFromDB.getPassword());

        try {
            // If everything is ok - save user
            convertAndSaveUser(user);

            // Update user in firebase
            firebaseUserService.update(user);
        } catch (Exception e) {
            String message = messageSource.getMessage("user.update",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new RuntimeException(message, e);
        }
        return user;
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed changePassword(UserForChangePassword userWithPassword) {

        // Check if id is present
        String id = userWithPassword.getId();
        if (id == null) {
            String message = messageSource.getMessage("id.empty",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // Check if such user exists
        if (!existsById(id)) {
            String message = messageSource.getMessage(USER_NOT_EXISTS_EXCEPTION_CODE,
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        UserDetailed versionOfUserFromDB = findByIdWithPassword(id).get();

        String newPassword = userWithPassword.getNewPassword();
        String oldPassword = userWithPassword.getOldPassword();
        String oldPasswordHash = versionOfUserFromDB.getPassword();

        // Validate the password for change
        Map<String, Object> errors = userValidator.validatePasswordForChange(oldPassword,
                newPassword, oldPasswordHash);

        // If any errors - show it to the user
        if (!errors.isEmpty()) {
            log.info(StringUtils.join(errors));
            throw new IllegalArgumentException(errors.toString());
        }

        // Hash the password
        String hash = passwordService.hash(newPassword);

        // Set password hash to user
        versionOfUserFromDB.setPassword(hash);

        // Set password update date
        OffsetDateTime dtUpdate = versionOfUserFromDB.getDtUpdate();
        versionOfUserFromDB.setPasswordUpdateDate(dtUpdate);

        try {
            // If everything is ok - convert it to DB entity
            convertAndSaveUser(versionOfUserFromDB);

            // Update user in firebase
            firebaseUserService.updatePassword(id, newPassword);
        } catch (Exception e) {
            String message = messageSource.getMessage("user.change.password",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new RuntimeException(message, e);
        }

        return versionOfUserFromDB;
    }

    /**
     * Find user without password
     *
     * @param uuid user identifier
     * @return {@link UserDetailed} without password
     */
    @Override
    public Optional<UserDetailed> findById(String uuid) {
        Optional<UserDetailed> userDetailedWithPassword = findByIdWithPassword(uuid);
        userDetailedWithPassword.ifPresent(u -> u.setPassword(null));
        return userDetailedWithPassword;
    }

    /**
     * Finds user by given identifier with password
     *
     * <p>
     * ONLY FOR INTERNAL USAGE
     * <p>
     *
     * @param id user identifier
     * @return {@link UserDetailed} with password
     */
    @SneakyThrows
    private Optional<UserDetailed> findByIdWithPassword(String id) {
        UserDetailed user = findByIdAndConvert(id);
        return Optional.ofNullable(user);
    }

    /**
     * Find by id and convert to DTO class
     *
     * @param id user identifier
     * @return user dto
     * @throws Exception throw to caller in case of any errors
     */
    private UserDetailed findByIdAndConvert(String id) throws Exception {
        User user = firestoreRepository.findByKey(collection, id, User.class);
        UserDetailed userDetailed = user != null ? conversionService.convert(user, UserDetailed.class) : null;
        return userDetailed;
    }

    @Override
    public void deleteById(String uuid, Date version) {
        // Check if such user exists
        if (!existsById(uuid)) {
            String message = messageSource.getMessage(USER_NOT_EXISTS_EXCEPTION_CODE,
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // We know for sure such user exists
        UserDetailed versionOfUserFromDB = findByIdWithPassword(uuid).get();

        long lastUpdateDate = convertOffsetDateTimeToMills(versionOfUserFromDB.getDtUpdate());

        // Compare the versions of entities
        if (version.getTime() != lastUpdateDate) {
            String message = messageSource.getMessage("delete.version",
                    null, LocaleContextHolder.getLocale());
            log.info(message);
            throw new IllegalArgumentException(message);
        }

        // Delete profile photo
        FileDetailed profilePhoto = versionOfUserFromDB.getProfilePhoto();
        if (profilePhoto != null) {
            Date fileVersion = convertOffsetDateTimeToDate(profilePhoto.getDtCreate());
            fileService.deleteById(UUID.fromString(String.valueOf(profilePhoto.getId())), fileVersion);
        }

        // Delete all attached documents
        deleteAllAttachedFiles(versionOfUserFromDB);

        // Delete user
        firestoreRepository.delete(collection, uuid);
    }

    /**
     * Delete all user files
     *
     * @param user user with/without files
     */
    private void deleteAllAttachedFiles(UserDetailed user) {
        // Create overall list for all files
        List<FileDetailed> allFiles = new ArrayList<>();

        // If there are some documents - add them to main list
        List<FileDetailed> documents = user.getDocuments();
        if (documents != null && !documents.isEmpty()) {
            allFiles.addAll(documents);
        }

        // If there are some portfolio - add them to main list
        List<FileDetailed> portfolio = user.getPortfolio();
        if (portfolio != null && !portfolio.isEmpty()) {
            allFiles.addAll(portfolio);
        }

        // Delete all files
        allFiles.forEach(f -> fileService.deleteById(UUID.fromString(String.valueOf(f.getId())),
                convertOffsetDateTimeToDate(f.getDtCreate())));
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed findUserForLogin(String email, String password) {

        // Find user with password
        UserDetailed user = findByEmailWithPassword(email);

        if (user == null) {
            throw new IllegalStateException(messageSource.getMessage("user.not.exists", null, getLocale()));
        }

        // Check whether password is correct by verifying the hash
        boolean verificationSuccessful = passwordService.verifyHash(password, user.getPassword());

        if (!verificationSuccessful) {
            throw new WrongPasswordException(messageSource.getMessage("security.controller.login.password.wrong",
                    null, getLocale()));
        }

        return user;
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed findByEmail(String email) {
        return findByEmailWithPassword(email);
    }

    @Override
    @RemovePasswordFromReturningValue
    public UserDetailed verifyUser(String id, UUID code) {

        // Check if such user exists
        if (!existsById(id)) {
            throw new IllegalArgumentException(messageSource.getMessage(USER_NOT_EXISTS_EXCEPTION_CODE,
                    null, LocaleContextHolder.getLocale()));
        }

        // Get user via uuid (we certainly know it exists)
        UserDetailed user = findByIdWithPassword(id).get();

        UserState state = user.getState();

        // Check if user is not blocked
        if (state == UserState.BLOCKED) {
            throw new IllegalArgumentException(messageSource.getMessage("security.user.blocked",
                    null, LocaleContextHolder.getLocale()));
        }

        // Check if user is already verified
        if (state == UserState.ACTIVE) {
            throw new IllegalArgumentException(messageSource.getMessage("security.user.already.active",
                    null, LocaleContextHolder.getLocale()));
        }

        // Confirm user verification
        verificationService.confirmVerification(user, code);

        return user;
    }

    @Override
    public boolean existsById(String id) {
        try {
            return firestoreRepository.exists(collection, id);
        } catch (Exception e) {
            String message = messageSource.getMessage("user.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.error(message, e);
            throw new RuntimeException(message, e);
        }
    }

    @Override
    public Iterable<UserBrief> findAll() {
        return firestoreRepository.findAll(collection, User.class)
                .parallelStream()
                .map(user -> conversionService.convert(user, UserBrief.class))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public Iterable<UserBrief> findAllById(Iterable<String> idList) {
        return firestoreRepository.findAllById(collection, (List<String>) idList, User.class)
                .parallelStream()
                .map(user -> conversionService.convert(user, UserBrief.class))
                .collect(Collectors.toList());
    }

    @Override
    public long count() {
        return firestoreRepository.count(collection);
    }

    /**
     * Finds user by its email (with a password)
     *
     * @param email of a user
     * @return user found by email
     */
    @SneakyThrows
    private UserDetailed findByEmailWithPassword(String email) {

        // Execute selection query on database
        QuerySnapshot querySnapshot = firestoreRepository.getFirestore()
                .collection(collection)
                .whereEqualTo("email", email)
                .get()
                .get();

        // If the user was found - return it
        if (querySnapshot.size() == 1) {
            return querySnapshot.getDocuments()
                    .stream()
                    .map(document -> document.toObject(User.class))
                    .map(user -> conversionService.convert(user, UserDetailed.class))
                    .findFirst()
                    .orElse(null);
        } else if (querySnapshot.size() == 0) {
            return null;
        } else {
            String message = messageSource.getMessage("user.not.exists",
                    null, LocaleContextHolder.getLocale());
            log.error(message);
            throw new RuntimeException(message);
        }
    }
}
