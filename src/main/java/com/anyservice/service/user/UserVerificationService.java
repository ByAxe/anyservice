package com.anyservice.service.user;

import com.anyservice.core.VerificationCode;
import com.anyservice.core.enums.UserState;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.firestore.User;
import com.anyservice.repository.FirestoreRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.user.api.ICustomMailSender;
import com.anyservice.service.user.api.IUserVerificationService;
import com.google.cloud.firestore.DocumentReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserVerificationService implements IUserVerificationService<UserDetailed, UUID> {
    private final ICustomMailSender mailSender;
    private final FirestoreRepository firestoreRepository;
    private final MessageSource messageSource;
    private final CustomConversionService conversionService;

    @Value("${spring.cloud.gcp.firestore.collection.cache}")
    private String cacheCollection;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String usersCollection;

    public UserVerificationService(ICustomMailSender mailSender, FirestoreRepository firestoreRepository,
                                   MessageSource messageSource, CustomConversionService conversionService) {
        this.mailSender = mailSender;
        this.firestoreRepository = firestoreRepository;
        this.messageSource = messageSource;
        this.conversionService = conversionService;
    }

    public void startVerification(UserDetailed user) {

        // Generate verification code
        UUID verificationCode = UUID.randomUUID();

        String userId = String.valueOf(user.getId());

        // Send verification code to user's email
        mailSender.sendVerificationCode(user, verificationCode);

        // Build verification code dto
        VerificationCode vcDto = new VerificationCode(userId, verificationCode.toString());

        // Save verification code to cache for further verification
        try {
            firestoreRepository.saveAsync(cacheCollection, userId, vcDto);
        } catch (Exception e) {
            throw new RuntimeException("Saving to cache was unsuccessful", e);
        }

        // Update user state
        user.setState(UserState.WAITING);
    }

    public void confirmVerification(UserDetailed user, UUID code) {
        String userId = String.valueOf(user.getId());

        // Get expected verification code from a cache
        UUID expectedVerificationCode;
        try {
            VerificationCode verificationCode = firestoreRepository.findByKey(cacheCollection, userId,
                    VerificationCode.class);
            expectedVerificationCode = UUID.fromString(verificationCode.getVerificationCode());
        } catch (Exception e) {
            throw new RuntimeException("Retrieving from cache was unsuccessful", e);
        }

        // Check if verification code is correct
        if (!expectedVerificationCode.equals(code)) {
            throw new IllegalArgumentException(messageSource.getMessage("user.verification.code.wrong",
                    null, LocaleContextHolder.getLocale()));
        }

        // Update user object
        user.setState(UserState.ACTIVE);
        user.setVerified(true);

        // Convert dto to entity
        User userEntity = conversionService.convert(user, User.class);

        // Get references to objects for further transaction purposes
        final DocumentReference userDocumentReference = firestoreRepository.getDocumentReference(usersCollection, userId);
        final DocumentReference cacheDocumentReference = firestoreRepository.getDocumentReference(cacheCollection, userId);

        // Run following operations as single transaction
        firestoreRepository.getFirestore().runTransaction(transaction -> {
            try {
                // update user verification status
                transaction.set(userDocumentReference, userEntity);

                // remove verification code from cache
                transaction.delete(cacheDocumentReference);

                // In case everything is ok nothing should occur
                return null;
            } catch (Exception e) {
                throw new RuntimeException("Updating user verification status was unsuccessful", e);
            }
        });
    }
}
