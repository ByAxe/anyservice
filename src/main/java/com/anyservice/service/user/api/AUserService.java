package com.anyservice.service.user.api;

import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.api.ACRUDService;

import java.util.Date;
import java.util.UUID;

public abstract class AUserService extends ACRUDService<UserBrief, UserDetailed, UUID, Date> {
    @Override
    protected Class<UserDetailed> getDetailedClass() {
        return UserDetailed.class;
    }

    @Override
    protected Class<UserBrief> getBriefClass() {
        return UserBrief.class;
    }
}
