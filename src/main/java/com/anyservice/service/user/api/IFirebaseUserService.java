package com.anyservice.service.user.api;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

public interface IFirebaseUserService<U> {

    UserRecord create(U user) throws FirebaseAuthException;

    UserRecord update(U user) throws FirebaseAuthException;

    UserRecord updatePassword(String uid, String password) throws FirebaseAuthException;

    void delete(String uid);
}
