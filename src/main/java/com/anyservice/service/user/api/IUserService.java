package com.anyservice.service.user.api;

import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.dto.user.UserForChangePassword;
import com.anyservice.service.api.ICRUDService;
import com.anyservice.web.security.exceptions.UserNotFoundException;
import com.anyservice.web.security.exceptions.WrongPasswordException;
import com.google.firebase.auth.FirebaseToken;

import java.util.Date;
import java.util.UUID;

public interface IUserService extends ICRUDService<UserBrief, UserDetailed, String, Date> {

    /**
     * Change password operation
     * For all other updates - use {@link ICRUDService#update(APrimary, Object, Object)}
     *
     * @param userWithPassword {@link UserForChangePassword} that stores data for change password operation
     * @return user for that operation was performed
     */
    UserDetailed changePassword(UserForChangePassword userWithPassword);

    /**
     * Returns user if the email and password are correct
     *
     * @param email    email of a user
     * @param password password of a user
     * @return user
     * @throws UserNotFoundException  if user was not found by specified email
     * @throws WrongPasswordException if password if verification of hash was unsuccessful
     */
    UserDetailed findUserForLogin(String email, String password);

    /**
     * Finds user by its email
     *
     * @param email of a user
     * @return user found by email
     */
    UserDetailed findByEmail(String email);

    /**
     * User verification method
     *
     * @param id   identifier of a user, that must be verified
     * @param code verification code
     * @return verified {@link UserDetailed}
     * @throws IllegalArgumentException if something goes wrong with validation of passed values
     */
    UserDetailed verifyUser(String id, UUID code);

    /**
     * Create user from firebase token
     *
     * @param decodedToken firebase decoded token
     * @return created user
     */
    UserDetailed create(FirebaseToken decodedToken);
}
