package com.anyservice.service.user.api;

public interface IUserVerificationService<U, C> {

    /**
     * Starts verification process for given user
     *
     * @param user user that should be verified
     */
    void startVerification(U user);

    /**
     * Finish verification process for given user with given confirmation code
     *
     * @param user user that should be verified
     * @param code confirmation code
     */
    void confirmVerification(U user, C code);
}
