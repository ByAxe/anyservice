package com.anyservice.service.validators;

import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.service.order.api.IOrderService;
import com.anyservice.service.validators.api.ICandidateValidator;
import com.anyservice.service.validators.api.IOrderValidator;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Service
public class CandidateValidator implements ICandidateValidator {

    private final MessageSource messageSource;
    private final IOrderService orderService;
    private final IOrderValidator orderValidator;

    public CandidateValidator(MessageSource messageSource, IOrderService orderService,
                              IOrderValidator orderValidator) {
        this.messageSource = messageSource;
        this.orderService = orderService;
        this.orderValidator = orderValidator;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public Map<String, Object> validateCreation(Candidate candidate) {
        Map<String, Object> errors = new HashMap<>();

        UUID orderUuid = candidate.getOrderId();

        // Order uuid must present
        if (orderUuid == null) {
            errors.put("candidate.order.uuid", getMessageSource().getMessage("candidate.order.uuid",
                    null, getLocale()));

            return errors;
        }

        // Find bound order
        Optional<OrderDetailed> orderOptional = orderService.findById(orderUuid);

        // Order must exist
        if (orderOptional.isEmpty()) {
            // Order must present
            errors.put("candidate.order.not.found", getMessageSource().getMessage("candidate.order.not.found",
                    null, getLocale()));

            return errors;
        }

        // If it's present - extract it from optional
        OrderDetailed order = orderOptional.get();

        // During this validation will occur only validation errors bound to newly added candidate
        Map<String, Object> orderValidationErrors = orderValidator.validateUpdateCandidates(order);

        errors.putAll(orderValidationErrors);

        return errors;
    }

    @Override
    public Map<String, Object> validateUpdates(Candidate candidate) {
        Map<String, Object> errors = new HashMap<>();

        errors.putAll(validateCreation(candidate));

        return errors;
    }
}
