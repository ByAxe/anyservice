package com.anyservice.service.validators;

import com.anyservice.dto.order.Category;
import com.anyservice.service.validators.api.ICategoryValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Service
public class CategoryValidator implements ICategoryValidator {

    private final MessageSource messageSource;

    public CategoryValidator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public Map<String, Object> validateCreation(Category category) {
        Map<String, Object> errors = new HashMap<>();

        if (StringUtils.isEmpty(category.getTitle())) {
            errors.put("category.title", getMessageSource().getMessage("category.title.empty",
                    null, getLocale()));
        }

        return errors;
    }

    @Override
    public Map<String, Object> validateUpdates(Category category) {
        Map<String, Object> errors = new HashMap<>();

        if (StringUtils.isEmpty(category.getTitle())) {
            errors.put("category.title", getMessageSource().getMessage("category.title.empty",
                    null, getLocale()));
        }

        return errors;
    }
}
