package com.anyservice.service.validators;

import com.anyservice.dto.order.Chat;
import com.anyservice.service.validators.api.IChatValidator;
import lombok.NonNull;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChatValidator implements IChatValidator {

    private final MessageSource messageSource;

    public ChatValidator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public @NonNull Map<String, Object> validateCreation(Chat chat) {
        return new HashMap<>();
    }

    @Override
    public @NonNull Map<String, Object> validateUpdates(Chat chat) {
        return new HashMap<>();
    }
}
