package com.anyservice.service.validators;

import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Location;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.order.Response;
import com.anyservice.service.validators.api.IOrderValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.anyservice.core.enums.FileType.ATTACHMENT;
import static com.anyservice.core.enums.FileType.MAIN_ATTACHMENT;
import static com.anyservice.core.enums.OrderState.*;
import static com.anyservice.core.enums.UserState.ACTIVE;
import static java.time.OffsetDateTime.now;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Service
public class OrderValidator implements IOrderValidator {

    private final MessageSource messageSource;
    private final Environment environment;

    @Value("${order.max_attachments}")
    private int maxAttachments;

    @Value("${order.min_deadline}")
    private int minimalDeadlinePlusDays;

    @Value("${order.candidate.response.min_characters}")
    private int minimalResponseTextCharacters;

    public OrderValidator(MessageSource messageSource, Environment environment) {
        this.messageSource = messageSource;
        this.environment = environment;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public Map<String, Object> validateCreation(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        // Validate customer
//        errors.putAll(validateCustomer(order));

        // State validation
        errors.putAll(validateBasicStates(order, true));

        // No candidates must present
        if (!CollectionUtils.isEmpty(order.getCandidates())) {
            errors.put("order.candidates", getMessageSource().getMessage("order.candidates.not.empty",
                    null, getLocale()));
        }

        // Validate attachments
        errors.putAll(validateAttachments(order));

        // Validate deadline
        errors.putAll(validateDeadline(order));

        return errors;
    }

    @Override
    public Map<String, Object> validateUpdates(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        // Validate attachments
        errors.putAll(validateAttachments(order));

        // Validate candidates
        errors.putAll(validateUpdateCandidates(order));

        // Validate deadline
        errors.putAll(validateDeadline(order));

        return errors;
    }

    /**
     * Validate update operation of an order
     *
     * @param order    order to validate
     * @param oldState old order's state
     * @param newState new order's state
     * @return storage for errors obtained during validation
     */
    @Override
    public Map<String, Object> validateUpdates(OrderDetailed order, OrderState oldState, OrderState newState) {
        Map<String, Object> errors = new HashMap<>();

        // Validate update
        errors.putAll(validateUpdates(order));

        // Validate state change
        if (oldState != newState) {
            errors.putAll(validateStateChange(order, oldState, newState));
        }
        return errors;
    }

    /**
     * Validates basic states of an order - states with that order can be created
     *
     * @param order             newly created or updated order
     * @param isCreateOperation depending on this flag, validation will take into account
     *                          is this an update or create operation
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateBasicStates(OrderDetailed order, boolean isCreateOperation) {
        Map<String, Object> errors = new HashMap<>();

        switch (order.getState()) {
            case DRAFT:
                errors.putAll(validateDraft(order));
                break;
            case PUBLISHED:
                errors.putAll(validateCreationOfPublished(order));
                break;
            default:
                if (isCreateOperation) {
                    // Create state validation: only draft || published
                    errors.put("order.state", getMessageSource().getMessage("order.create.state",
                            null, getLocale()));
                }
        }

        return errors;
    }

    /**
     * Validate candidates in the order
     *
     * @param order to be validated
     * @return storage for errors obtained during validation
     */
    public Map<String, Object> validateUpdateCandidates(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        // If there are candidates
        List<Candidate> candidates = order.getCandidates();
        if (!CollectionUtils.isEmpty(candidates)) {

            List<UserBrief> usersCandidates = candidates.stream()
                    .map(Candidate::getUser)
                    .collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(usersCandidates)) {

                // Each candidate's legal status must be verified
                if (usersCandidates.stream().noneMatch(UserBrief::isLegalStatusVerified)) {
                    errors.put("order.candidate.legal.verified", getMessageSource().getMessage("order.candidate.legal.status.not.verified",
                            null, getLocale()));
                }

                // Each candidate must be verified
                if (usersCandidates.stream().noneMatch(UserBrief::isVerified)) {
                    errors.put("order.candidate.verified", getMessageSource().getMessage("order.candidate.not.verified",
                            null, getLocale()));
                }
            }

            // Order Uuid of a candidate must be == order.uuid
            if (candidates.stream().anyMatch(c -> !order.getId().equals(c.getOrderId()))) {
                errors.put("order.candidate.uuid", getMessageSource().getMessage("order.candidate.uuid.wrong",
                        null, getLocale()));
            }

            // If ! ( COMPLETED || BLOCKED ) -> "selected" candidate must not present
            if (!asList(CANDIDATE_SELECTED, COMPLETED, NOT_COMPLETED, BLOCKED).contains(order.getState())
                    && candidates.stream().anyMatch(Candidate::isSelected)) {
                errors.put("order.candidate.selected", getMessageSource().getMessage("order.candidate.selected",
                        new Object[]{order.getState()}, getLocale()));
            }

            // If state one of these states,
            // selected candidate must present in candidates list
            if (asList(CANDIDATE_SELECTED, COMPLETED, NOT_COMPLETED).contains(order.getState())
                    && order.getCandidates().stream().noneMatch(Candidate::isSelected)) {
                errors.put("order.candidate.state.selected", getMessageSource().getMessage("order.candidate.state.selected",
                        new Object[]{order.getState()}, getLocale()));
            }

            // > 1 candidate cannot be selected
            if (candidates.stream().filter(Candidate::isSelected).count() > 1) {
                errors.put("order.candidate.multiple.selected",
                        getMessageSource().getMessage("order.candidate.multiple.selected",
                                null, getLocale()));
            }

            // Responses should be == candidates size
            if (candidates.size() != order.getResponses()) {
                errors.put("order.responses", getMessageSource().getMessage("order.responses.size",
                        null, getLocale()));
            }

            // Validate customer
            errors.putAll(validateCustomer(order));

            // customer != candidate for the same order
            if (order.getCustomer() != null && usersCandidates.contains(order.getCustomer())) {
                errors.put("order.customer.candidate", getMessageSource().getMessage("order.customer.candidate",
                        null, getLocale()));
            }

            // Validate each candidate response
            List<Map<String, Object>> candidateErrors = candidates.stream()
                    .map(this::validateCandidateResponse)
                    .collect(Collectors.toList());

            candidateErrors.forEach(errors::putAll);

        } else {

            // Responses should be == candidates size
            if (order.getResponses() != 0) {
                errors.put("order.responses", getMessageSource().getMessage("order.responses.size",
                        null, getLocale()));
            }

            // If state is candidate selected -> candidates must present
            if (CANDIDATE_SELECTED == order.getState()) {
                errors.put("order.candidate.state.selected", getMessageSource().getMessage("order.candidate.state.selected.empty",
                        new Object[]{order.getState()}, getLocale()));
            }
        }

        // State validation
        errors.putAll(validateBasicStates(order, false));

        return errors;
    }

    /**
     * Validation of a customer in the order
     *
     * @param order {@link OrderDetailed}
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateCustomer(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        UserBrief customer = order.getCustomer();

        // Customer must present
        if (customer == null) {
            errors.put("customer", getMessageSource().getMessage("order.customer.empty",
                    null, getLocale()));
        } else {
            // Customer must be verified if state is "published"
            if ((ACTIVE != customer.getState() || !customer.isVerified())
                    && PUBLISHED == order.getState()) {
                errors.put("customer.verified", getMessageSource().getMessage("order.customer.verified",
                        null, getLocale()));
            }
        }

        return errors;
    }

    /**
     * Validation of attachments in the order
     *
     * @param order {@link OrderDetailed}
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateAttachments(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        List<FileDetailed> attachments = order.getAttachments();
        FileDetailed mainAttachment = order.getMainAttachment();

        if (!CollectionUtils.isEmpty(attachments)) {

            // Amount of attachments must not exceed X amount
            if (attachments.size() > maxAttachments) {
                errors.put("order.attachments.size.exceed", getMessageSource().getMessage("order.attachments.size.exceed",
                        new Object[]{maxAttachments}, getLocale()));
            }

            // Attachments file types must be ATTACHMENT
            if (!attachments.stream().allMatch(a -> ATTACHMENT == a.getFileType())) {
                errors.put("order.attachments", getMessageSource().getMessage("order.attachments.type",
                        null, getLocale()));
            }

            // If attachments are not empty -> main attachment must present
            if (mainAttachment == null) {
                errors.put("order.main.attachment", getMessageSource().getMessage("order.main.attachment.empty",
                        null, getLocale()));
            } else if (MAIN_ATTACHMENT != mainAttachment.getFileType()) {

                // MainAttachment file type must be MAIN_ATTACHMENT
                errors.put("order.main.attachment", getMessageSource().getMessage("order.main.attachment.type",
                        null, getLocale()));
            }
        }
        return errors;
    }

    /**
     * Validate creation of {@link OrderDetailed} with {@link OrderState#PUBLISHED}
     *
     * @param order {@link OrderDetailed}
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateCreationOfPublished(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        // Headline must not be empty
        if (isEmpty(order.getHeadline())) {
            errors.put("order.headline", getMessageSource().getMessage("order.published.headline",
                    null, getLocale()));
        }

        // Phone must not be empty
        if (isEmpty(order.getPhone())) {
            errors.put("order.phone", getMessageSource().getMessage("order.published.phone",
                    null, getLocale()));
        }

        // Price cannot be 0
        if (order.getPrice() != null && BigDecimal.ZERO.equals(order.getPrice())) {
            errors.put("order.price", getMessageSource().getMessage("order.published.price",
                    null, getLocale()));
        }

        // Candidate cannot be selected with PUBLISHED state of order
        if (order.getCandidates() != null
                && order.getCandidates().stream().anyMatch(Candidate::isSelected)) {
            errors.put("order.candidates.published", getMessageSource().getMessage("order.published.candidates.selected",
                    null, getLocale()));
        }

        // Order should belong to at least one category
        if (CollectionUtils.isEmpty(order.getCategories())) {
            errors.put("order.categories", getMessageSource().getMessage("order.categories.empty",
                    null, getLocale()));
        }

        // Validate location
        errors.putAll(validateLocation(order.getLocation()));

        return errors;
    }

    /**
     * Validate creation of {@link OrderDetailed} with {@link OrderState#DRAFT}
     *
     * @param order {@link OrderDetailed}
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateDraft(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        if (isEmpty(order.getHeadline()) && isEmpty(order.getDescription())) {
            errors.put("order.draft", getMessageSource().getMessage("order.draft.fields",
                    null, getLocale()));
        }

        if (order.getCandidates() != null) {
            errors.put("order.candidates.draft", getMessageSource().getMessage("order.draft.candidates",
                    null, getLocale()));
        }

        return errors;
    }


    /**
     * Validation of {@link Response} of a {@link Candidate}
     *
     * @param candidate that stores response of a candidate
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateCandidateResponse(Candidate candidate) {
        Map<String, Object> errors = new HashMap<>();

        UUID uuid = UUID.fromString(String.valueOf(candidate.getId()));

        Response response = candidate.getResponse();
        if (response != null) {

            // Candidate's response price != null && price != 0
            if (response.isPriceUpdated() &&
                    (response.getOfferedPrice() == null || BigDecimal.ZERO.equals(response.getOfferedPrice()))) {

                String key = "order.candidate.response.price." + uuid;
                errors.put(key, getMessageSource().getMessage("order.candidate.response.price",
                        null, getLocale()));
            }

            // Candidate's response text must not be empty
            if (StringUtils.isEmpty(response.getText())) {
                String key = "order.candidate.response.text." + uuid;
                errors.put(key, getMessageSource().getMessage("order.candidate.response.text.empty",
                        null, getLocale()));
            } else {

                // Text must not be shorter than X characters
                if (response.getText().trim().length() < minimalResponseTextCharacters) {
                    String key = "order.candidate.response.text." + uuid;
                    errors.put(key, getMessageSource().getMessage("order.candidate.response.text.short",
                            null, getLocale()));
                }
            }

        } else {
            String key = "order.candidate.response.empty." + uuid;
            errors.put(key, getMessageSource().getMessage("order.candidate.response.empty",
                    null, getLocale()));
        }

        return errors;
    }


    /**
     * Validation location of an order
     * <p>
     * Left empty for future improvements
     *
     * @param location object that stores location data
     * @return storage for errors obtained during validation
     */
    @SuppressWarnings("UnnecessaryLocalVariable")
    private Map<String, Object> validateLocation(Location location) {
        Map<String, Object> errors = Map.of();

        return errors;
    }

    /**
     * Validation of deadline of an order
     *
     * @param order {@link OrderDetailed}
     * @return storage for errors obtained during validation
     */
    private Map<String, Object> validateDeadline(OrderDetailed order) {
        Map<String, Object> errors = new HashMap<>();

        // Deadline must not be earlier than now + Y
        OffsetDateTime deadline = order.getDeadline();

        OffsetDateTime minimalDeadline = now().plusDays(minimalDeadlinePlusDays).minusMinutes(1);

        if (deadline != null && deadline.isBefore(minimalDeadline)) {
            errors.put("order.deadline", getMessageSource().getMessage("order.deadline",
                    new Object[]{minimalDeadlinePlusDays}, getLocale()));
        }
        return errors;
    }

    @Override
    public Map<String, Object> validateStateChange(OrderDetailed order,
                                                   OrderState oldState, OrderState newState) {
        Map<String, Object> errors = new HashMap<>();

        // State update on the same one is allowed for all states
        if (oldState == newState) return errors;

        // Get property
        String listOfAllowedStatesAsString = environment.getProperty("order.state.flow." + oldState);

        // If there are no states -> it's a system error
        if (listOfAllowedStatesAsString == null) throw new IllegalStateException("State must present in properties");

        // Get list of allowed state changes for given old state
        List<OrderState> allowedStates = Stream.of(listOfAllowedStatesAsString.split(", "))
                .filter(s -> !s.isEmpty())
                .map(OrderState::valueOf)
                .collect(Collectors.toList());

        // State must be one of allowed
        if (!allowedStates.contains(newState)) {
            errors.put("order.state", getMessageSource().getMessage("order.state.change.forbidden",
                    new Object[]{oldState, newState}, getLocale()));
        }

        return errors;
    }
}
