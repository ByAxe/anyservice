package com.anyservice.service.validators;

import com.anyservice.dto.order.Review;
import com.anyservice.service.validators.api.IReviewValidator;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Service
public class ReviewValidator implements IReviewValidator {

    private final MessageSource messageSource;

    public ReviewValidator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public @NonNull Map<String, Object> validateCreation(Review review) {
        Map<String, Object> errors = new HashMap<>();

        // Contractor response should not be filled at the moment of review creation
        if (StringUtils.isNotEmpty(review.getContractorResponse())) {
            errors.put("review.contractor.response", getMessageSource()
                    .getMessage("review.contractor.response.not.empty", null, getLocale()));
        }

        // Validate all required fields
        errors.putAll(validateRequiredFields(review));

        return errors;
    }

    @Override
    public @NonNull Map<String, Object> validateUpdates(Review review) {
        Map<String, Object> errors = new HashMap<>();

        // Validate all required fields
        errors.putAll(validateRequiredFields(review));

        return errors;
    }

    private @NonNull Map<String, Object> validateRequiredFields(Review review) {
        Map<String, Object> errors = new HashMap<>();

        BigDecimal rating = review.getRating();
        BigDecimal threshold = new BigDecimal("5.0");

        // Rating shouldn't be bigger than threshold
        if (rating != null && rating.compareTo(threshold) > 0) {
            errors.put("review.rating", getMessageSource().getMessage("review.rating.threshold",
                    null, getLocale()));
        }

        // Header or body of review should be filled
        if (StringUtils.isEmpty(review.getHeader()) && StringUtils.isEmpty(review.getBody())) {
            errors.put("review.text", getMessageSource()
                    .getMessage("review.text.empty", null, getLocale()));
        }

        // Order must present in review
        if (review.getOrder() == null) {
            errors.put("review.order", getMessageSource()
                    .getMessage("review.order.empty", null, getLocale()));
        }

        // Author must present in review
        if (review.getAuthor() == null) {
            errors.put("review.author", getMessageSource()
                    .getMessage("review.author.empty", null, getLocale()));
        }

        // Contractor must present in review
        if (review.getContractor() == null) {
            errors.put("review.contractor", getMessageSource()
                    .getMessage("review.contractor.empty", null, getLocale()));
        }

        return errors;
    }
}
