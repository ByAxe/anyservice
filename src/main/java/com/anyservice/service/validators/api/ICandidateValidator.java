package com.anyservice.service.validators.api;

import com.anyservice.dto.order.Candidate;

public interface ICandidateValidator extends IValidator<Candidate> {
}
