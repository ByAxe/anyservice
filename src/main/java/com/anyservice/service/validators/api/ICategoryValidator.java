package com.anyservice.service.validators.api;

import com.anyservice.dto.order.Category;

public interface ICategoryValidator extends IValidator<Category> {
}
