package com.anyservice.service.validators.api;

import com.anyservice.dto.order.Chat;

public interface IChatValidator extends IValidator<Chat> {
}
