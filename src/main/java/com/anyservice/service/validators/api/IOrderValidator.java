package com.anyservice.service.validators.api;

import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.order.OrderDetailed;

import java.util.Map;

public interface IOrderValidator extends IValidator<OrderDetailed> {

    /**
     * Validate update operation of an order
     *
     * @param order    order to validate
     * @param oldState old order's state
     * @param newState new order's state
     * @return storage for errors obtained during validation
     */
    Map<String, Object> validateUpdates(OrderDetailed order, OrderState oldState, OrderState newState);

    /**
     * Validates state change for given order
     *
     * @param order    to validate
     * @param oldState old order's state
     * @param newState new order's state
     * @return storage for errors obtained during validation
     */
    Map<String, Object> validateStateChange(OrderDetailed order, OrderState oldState, OrderState newState);


    /**
     * Validation of candidates for an order
     *
     * @param order to validate
     * @return storage for errors obtained during validation
     */
    Map<String, Object> validateUpdateCandidates(OrderDetailed order);
}
