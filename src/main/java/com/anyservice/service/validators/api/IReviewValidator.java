package com.anyservice.service.validators.api;

import com.anyservice.dto.order.Review;

public interface IReviewValidator extends IValidator<Review> {
}
