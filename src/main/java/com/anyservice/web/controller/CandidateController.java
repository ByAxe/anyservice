package com.anyservice.web.controller;

import com.anyservice.dto.order.Candidate;
import com.anyservice.service.order.api.ICandidateService;
import com.anyservice.web.controller.api.ICRUDController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/candidate")
public class CandidateController implements ICRUDController<Candidate, Candidate, UUID, Long> {

    private final MessageSource messageSource;
    private final ICandidateService categoryService;

    public CandidateController(MessageSource messageSource, ICandidateService categoryService) {
        this.messageSource = messageSource;
        this.categoryService = categoryService;
    }

    @Override
    @PostMapping
    public ResponseEntity<Candidate> create(@RequestBody Candidate candidate) {
        Candidate saved;

        try {
            saved = categoryService.create(candidate);
        } catch (Exception e) {
            log.info(messageSource.getMessage("candidate.create",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        UUID uuid = UUID.fromString(String.valueOf(saved.getId()));

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(uuid).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{uuid}/version/{version}")
    public ResponseEntity<Candidate> update(@RequestBody Candidate candidate,
                                            @PathVariable UUID uuid, @PathVariable Long version) {
        Candidate updated;

        try {
            updated = categoryService.update(candidate, uuid, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("candidate.update",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updated, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<Candidate> findById(@PathVariable UUID uuid) {
        return categoryService.findById(uuid)
                .map(candidate -> new ResponseEntity<>(candidate, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = categoryService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<Iterable<Candidate>> findAll() {
        Iterable<Candidate> dtoIterable = categoryService.findAll();

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<Candidate>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<Candidate> dtoIterable = categoryService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = categoryService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        categoryService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
