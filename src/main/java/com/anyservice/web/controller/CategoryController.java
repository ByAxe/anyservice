package com.anyservice.web.controller;

import com.anyservice.dto.order.Category;
import com.anyservice.service.order.api.ICategoryService;
import com.anyservice.web.controller.api.ICRUDController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/category")
public class CategoryController implements ICRUDController<Category, Category, UUID, Long> {

    private final MessageSource messageSource;
    private final ICategoryService categoryService;

    public CategoryController(MessageSource messageSource, ICategoryService categoryService) {
        this.messageSource = messageSource;
        this.categoryService = categoryService;
    }

    @Override
    @PostMapping
    public ResponseEntity<Category> create(@RequestBody Category category) {
        Category saved;

        try {
            saved = categoryService.create(category);
        } catch (Exception e) {
            log.info(messageSource.getMessage("category.create",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        UUID uuid = UUID.fromString(String.valueOf(saved.getId()));

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(uuid).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{uuid}/version/{version}")
    public ResponseEntity<Category> update(@RequestBody Category category,
                                           @PathVariable UUID uuid, @PathVariable Long version) {
        Category updated;

        try {
            updated = categoryService.update(category, uuid, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("category.update",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updated, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<Category> findById(@PathVariable UUID uuid) {
        return categoryService.findById(uuid)
                .map(category -> new ResponseEntity<>(category, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = categoryService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<Iterable<Category>> findAll() {
        Iterable<Category> dtoIterable = categoryService.findAll();

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<Category>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<Category> dtoIterable = categoryService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = categoryService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        categoryService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
