package com.anyservice.web.controller;

import com.anyservice.dto.order.Chat;
import com.anyservice.service.order.api.IChatService;
import com.anyservice.web.controller.api.ICRUDController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/chat")
public class ChatController implements ICRUDController<Chat, Chat, UUID, Long> {

    private final MessageSource messageSource;
    private final IChatService chatService;

    public ChatController(MessageSource messageSource, IChatService chatService) {
        this.messageSource = messageSource;
        this.chatService = chatService;
    }

    @Override
    @PostMapping
    public ResponseEntity<Chat> create(@RequestBody Chat chat) {
        Chat saved;

        try {
            saved = chatService.create(chat);
        } catch (Exception e) {
            log.info(messageSource.getMessage("chat.create",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        UUID uuid = UUID.fromString(String.valueOf(saved.getId()));

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(uuid).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{uuid}/version/{version}")
    public ResponseEntity<Chat> update(@RequestBody Chat chat,
                                       @PathVariable UUID uuid, @PathVariable Long version) {
        Chat updated;

        try {
            updated = chatService.update(chat, uuid, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("chat.update",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updated, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<Chat> findById(@PathVariable UUID uuid) {
        return chatService.findById(uuid)
                .map(chat -> new ResponseEntity<>(chat, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = chatService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<Iterable<Chat>> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<Chat>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<Chat> dtoIterable = chatService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        throw new UnsupportedOperationException();
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        chatService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
