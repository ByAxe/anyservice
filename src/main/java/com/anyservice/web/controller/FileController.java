package com.anyservice.web.controller;

import com.anyservice.core.enums.FileExtension;
import com.anyservice.core.enums.FileType;
import com.anyservice.dto.file.FileBrief;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.service.file.api.IFileService;
import com.anyservice.web.controller.api.ICRUDController;
import lombok.Cleanup;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/file")
public class FileController implements ICRUDController<FileBrief, FileDetailed, UUID, Long> {
    private final IFileService fileService;
    private final MessageSource messageSource;

    public FileController(IFileService fileService, MessageSource messageSource) {
        this.fileService = fileService;
        this.messageSource = messageSource;
    }

    @SneakyThrows
    @PostMapping("/upload/{type}")
    public ResponseEntity<FileDetailed> create(@PathVariable FileType type,
                                               @NonNull @RequestParam("file") MultipartFile file) {
        // Build file object
        FileDetailed detailed = FileDetailed.builder()
                .extension(FileExtension.findExtension(file.getContentType()))
                .name(file.getOriginalFilename())
                .size(file.getSize())
                .inputStream(file.getInputStream())
                .fileType(type)
                .build();

        // Save it
        FileDetailed saved = fileService.create(detailed);

        HttpHeaders httpHeaders = new HttpHeaders();

        // Put identifier into headers
        httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saved.getId())
                .toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    /**
     * Find File metadata, without actual file content
     *
     * @param uuid file identifier
     * @return {@link FileDetailed} without actual file content
     */
    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<FileDetailed> findById(@PathVariable UUID uuid) {
        return fileService.findById(uuid)
                .map(fileDetailed -> new ResponseEntity<>(fileDetailed, OK))
                .orElseGet(() -> new ResponseEntity<>(null, NO_CONTENT));
    }

    /**
     * Find file and its content by its identifier
     *
     * @param uuid     file identifier
     * @param response {@link HttpServletResponse} through that file content will be passed to the client
     */
    @GetMapping("/{uuid}/load")
    @SneakyThrows
    public void findById(@PathVariable UUID uuid, HttpServletResponse response) {
        // File file by identifier
        Optional<FileDetailed> fileDetailedOptional = fileService.findById(uuid);

        // Return nothing if file not found
        if (fileDetailedOptional.isEmpty()) return;

        // If file present - extract it from Optional
        FileDetailed fileDetailed = fileDetailedOptional.get();

        // Take file
        @Cleanup InputStream inputStream = fileDetailed.getInputStream();

        // Encode fileName
        String fileName = URLEncoder.encode(fileDetailed.getName(), StandardCharsets.UTF_8);

        // Replace forbidden characters
        String fileNameCleanedUp = fileName.replaceAll("\\+", "_");

        // Create contentDisposition header
        String contentDisposition = String.format("attachment; filename=\"%s\"", fileNameCleanedUp);

        // Fill response with needed meta information
        response.setHeader("Content-Disposition", contentDisposition);
        response.setContentType(fileDetailed.getExtension().getContentType());

        // Copy file to response
        IOUtils.copy(inputStream, response.getOutputStream());

        // Send response to the client
        response.flushBuffer();
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = fileService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<Iterable<FileBrief>> findAll() {
        Iterable<FileBrief> dtoIterable = fileService.findAll();

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<FileBrief>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<FileBrief> dtoIterable = fileService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = fileService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        fileService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }

    @Override
    @PostMapping
    public ResponseEntity<FileDetailed> create(@RequestBody FileDetailed dto) {
        FileDetailed saved;
        try {
            saved = fileService.createFileViaLink(dto);
        } catch (Exception e) {
            log.info(messageSource.getMessage("file.save",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        String id = String.valueOf(saved.getId());

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(id).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{id}/version/{version}")
    public ResponseEntity<FileDetailed> update(@RequestBody FileDetailed dto,
                                               @PathVariable UUID id, @PathVariable Long version) {
        FileDetailed updated;

        try {
            updated = fileService.update(dto, id, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("file.update",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        return new ResponseEntity<>(updated, CREATED);
    }
}
