package com.anyservice.web.controller;


import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.service.order.api.IOrderService;
import com.anyservice.web.controller.api.ICRUDController;
import com.anyservice.web.specification.IOrderSpec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/order")
public class OrderController implements ICRUDController<OrderBrief, OrderDetailed, UUID, Long> {

    private final IOrderService orderService;
    private final MessageSource messageSource;

    public OrderController(IOrderService orderService, MessageSource messageSource) {
        this.orderService = orderService;
        this.messageSource = messageSource;
    }

    @Override
    @PostMapping
    public ResponseEntity<OrderDetailed> create(@RequestBody OrderDetailed dto) {
        OrderDetailed saved;

        try {
            saved = orderService.create(dto);
        } catch (Exception e) {
            log.info(messageSource.getMessage("order.create",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        UUID uuid = UUID.fromString(String.valueOf(saved.getId()));

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(uuid).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{uuid}/version/{version}")
    public ResponseEntity<OrderDetailed> update(@RequestBody OrderDetailed dto,
                                                @PathVariable UUID uuid, @PathVariable Long version) {
        OrderDetailed updatedOrder;

        try {
            updatedOrder = orderService.update(dto, uuid, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("order.update",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updatedOrder, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<OrderDetailed> findById(@PathVariable UUID uuid) {
        return orderService.findById(uuid)
                .map(orderDetailed -> new ResponseEntity<>(orderDetailed, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = orderService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @GetMapping
    public ResponseEntity<List<OrderBrief>> findAll(IOrderSpec specification) {
        List<OrderBrief> dtoIterable = orderService.findAll(specification);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    public ResponseEntity<Iterable<OrderBrief>> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<OrderBrief>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<OrderBrief> dtoIterable = orderService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = orderService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        orderService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
