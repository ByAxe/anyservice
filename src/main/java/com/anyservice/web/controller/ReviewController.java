package com.anyservice.web.controller;

import com.anyservice.dto.order.Review;
import com.anyservice.service.order.api.IReviewService;
import com.anyservice.web.controller.api.ICRUDController;
import com.anyservice.web.specification.IReviewSpec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/review")
public class ReviewController implements ICRUDController<Review, Review, UUID, Long> {

    private final MessageSource messageSource;
    private final IReviewService reviewService;

    public ReviewController(MessageSource messageSource, IReviewService reviewService) {
        this.messageSource = messageSource;
        this.reviewService = reviewService;
    }

    @Override
    @PostMapping
    public ResponseEntity<Review> create(@RequestBody Review review) {
        Review saved;

        try {
            saved = reviewService.create(review);
        } catch (Exception e) {
            log.info(messageSource.getMessage("review.create",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        UUID uuid = UUID.fromString(String.valueOf(saved.getId()));

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(uuid).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{uuid}/version/{version}")
    public ResponseEntity<Review> update(@RequestBody Review review,
                                         @PathVariable UUID uuid, @PathVariable Long version) {
        Review updated;

        try {
            updated = reviewService.update(review, uuid, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("review.update",
                    null, LocaleContextHolder.getLocale()), e);
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updated, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{uuid}")
    public ResponseEntity<Review> findById(@PathVariable UUID uuid) {
        return reviewService.findById(uuid)
                .map(review -> new ResponseEntity<>(review, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{uuid}")
    public ResponseEntity<Boolean> existsById(@PathVariable UUID uuid) {
        boolean exists = reviewService.existsById(uuid);

        return new ResponseEntity<>(exists, OK);
    }

    @GetMapping
    public ResponseEntity<List<Review>> findAll(IReviewSpec specification) {
        List<Review> reviews = reviewService.findAll(specification);

        return new ResponseEntity<>(reviews, OK);
    }

    @Override
    public ResponseEntity<Iterable<Review>> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    @GetMapping("uuid/list/{uuids}")
    public ResponseEntity<Iterable<Review>> findAllById(@PathVariable List<UUID> uuids) {
        Iterable<Review> dtoIterable = reviewService.findAllById(uuids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = reviewService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{uuid}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable UUID uuid, @PathVariable Long version) {
        reviewService.deleteById(uuid, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
