package com.anyservice.web.controller;

import com.anyservice.core.enums.UserRole;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.user.UserHolder;
import com.anyservice.service.user.api.IUserService;
import com.anyservice.web.security.JwtUtil;
import com.anyservice.web.security.dto.Login;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RestController
@RequestMapping("/api/v1/user")
public class SecurityController {

    private final IUserService userService;
    private final JwtUtil jwtUtil;
    private final UserHolder holder;

    public SecurityController(JwtUtil jwtUtil, IUserService userService,
                              UserHolder holder) {
        this.jwtUtil = jwtUtil;
        this.userService = userService;
        this.holder = holder;
    }

    /**
     * Token generation for this user
     *
     * @param request all the data, to identify the user
     * @return token for user
     */
    @PostMapping("/login")
    public String login(@RequestBody Login request) throws Exception {
        UserDetailed user = userService.findUserForLogin(request.getEmail(), request.getPassword());

        return jwtUtil.generateToken(user);
    }

    @GetMapping("/logout")
    public ResponseEntity<UserDetailed> logout() {
        HttpHeaders httpHeaders = new HttpHeaders();

        // Does not make anything for now

        return new ResponseEntity<>(null, httpHeaders, OK);
    }

    @PutMapping("/refresh")
    public String refreshToken() throws Exception {
        return jwtUtil.refreshToken();
    }

    /**
     * Allowed only for authenticated users
     *
     * @return role of a user that's made a request
     */
    @GetMapping("/authenticated")
    public ResponseEntity<?> checkIfAuthenticated() {
        UserRole role = holder.getUser().getRole();

        int ordinal = role.ordinal();

        return new ResponseEntity<>(ordinal, OK);
    }
}
