package com.anyservice.web.controller;

import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.dto.user.UserForChangePassword;
import com.anyservice.service.user.UserHolder;
import com.anyservice.service.user.api.IUserService;
import com.anyservice.web.controller.api.ICRUDController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/user")
public class UserController implements ICRUDController<UserBrief, UserDetailed, String, Long> {

    private final IUserService userService;
    private final MessageSource messageSource;
    private final UserHolder userHolder;

    public UserController(IUserService userService, MessageSource messageSource, UserHolder userHolder) {
        this.userService = userService;
        this.messageSource = messageSource;
        this.userHolder = userHolder;
    }

    @PostMapping("/me")
    public ResponseEntity<UserDetailed> getUser() {
        return new ResponseEntity<>(userHolder.getUser(), OK);
    }

    @Override
    @PostMapping
    public ResponseEntity<UserDetailed> create(@RequestBody UserDetailed dto) {
        UserDetailed saved;

        try {
            saved = userService.create(dto);
        } catch (Exception e) {
            log.info(messageSource.getMessage("user.save",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        String id = String.valueOf(saved.getId());

        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(id).toUri());

        return new ResponseEntity<>(saved, httpHeaders, CREATED);
    }

    @Override
    @PutMapping("/{id}/version/{version}")
    public ResponseEntity<UserDetailed> update(@RequestBody UserDetailed dto,
                                               @PathVariable String id, @PathVariable Long version) {
        UserDetailed updatedUser;

        try {
            updatedUser = userService.update(dto, id, new Date(version));
        } catch (Exception e) {
            log.info(messageSource.getMessage("user.update",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(updatedUser, httpHeaders, OK);
    }

    /**
     * Change password of a user
     *
     * @param user special DTO for changing password operation
     * @return user with changed password
     * @throws Exception if something went wrong during change password operation
     */
    @PutMapping("/change/password")
    public ResponseEntity<UserDetailed> changePassword(@RequestBody UserForChangePassword user) {
        UserDetailed userDetailed;

        try {
            userDetailed = userService.changePassword(user);
        } catch (Exception e) {
            log.info(messageSource.getMessage("user.change.password",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(userDetailed, httpHeaders, OK);
    }

    /**
     * Verify given user via verification code
     *
     * @param id   user identifier
     * @param code user verification code
     * @return verified user
     * @throws Exception if something went wrong during verification process
     */
    @GetMapping("/verification/{id}/{code}")
    public ResponseEntity<UserDetailed> verifyUser(@NotNull @PathVariable String id,
                                                   @NotNull @PathVariable UUID code) {
        UserDetailed user;

        try {
            user = userService.verifyUser(id, code);
        } catch (Exception e) {
            log.info(messageSource.getMessage("user.verification",
                    null, LocaleContextHolder.getLocale()));
            throw e;
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        return new ResponseEntity<>(user, httpHeaders, OK);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<UserDetailed> findById(@PathVariable String id) {
        return userService.findById(id)
                .map(userDetailed -> new ResponseEntity<>(userDetailed, OK))
                .orElseGet(() -> new ResponseEntity<>(NO_CONTENT));
    }

    @Override
    @GetMapping("/exists/{id}")
    public ResponseEntity<Boolean> existsById(@PathVariable String id) {
        boolean exists = userService.existsById(id);

        return new ResponseEntity<>(exists, OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<Iterable<UserBrief>> findAll() {
        Iterable<UserBrief> dtoIterable = userService.findAll();

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("uuid/list/{ids}")
    public ResponseEntity<Iterable<UserBrief>> findAllById(@PathVariable List<String> ids) {
        Iterable<UserBrief> dtoIterable = userService.findAllById(ids);

        return new ResponseEntity<>(dtoIterable, OK);
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        long count = userService.count();
        return new ResponseEntity<>(count, OK);
    }

    @Override
    @DeleteMapping("/{id}/version/{version}")
    public ResponseEntity<?> deleteById(@PathVariable String id, @PathVariable Long version) {
        userService.deleteById(id, new Date(version));

        return new ResponseEntity<>(NO_CONTENT);
    }
}
