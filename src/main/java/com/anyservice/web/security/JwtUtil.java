package com.anyservice.web.security;

import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.user.UserHolder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Component
public class JwtUtil {
    private final UserHolder userHolder;
    private final MessageSource messageSource;

    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    public JwtUtil(UserHolder userHolder, MessageSource messageSource) {
        this.userHolder = userHolder;
        this.messageSource = messageSource;
    }

    /**
     * Refresh the token
     *
     * @return new token
     * @throws FirebaseAuthException if something goes wrong - throw everything up
     */
    public String refreshToken() throws FirebaseAuthException {
        final UserDetailed user = userHolder.getUser();

        if (user == null) {
            throw new IllegalStateException(messageSource.getMessage("jwt.util.refresh.token",
                    null, getLocale()));
        }

        return firebaseAuth.createCustomToken((String) user.getId());
    }

    /**
     * Generate and fill new token
     *
     * @param user for which token generated
     * @return token by itself
     * @throws FirebaseAuthException if something goes wrong - throw everything up
     */
    public String generateToken(UserDetailed user) throws FirebaseAuthException {
        if (user == null) {
            throw new IllegalStateException(messageSource.getMessage(
                    "jwt.authentication.provider.retrieve.user.not.found",
                    null, getLocale()));
        }

        return firebaseAuth.createCustomToken((String) user.getId());
    }

}
