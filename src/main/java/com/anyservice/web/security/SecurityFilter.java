package com.anyservice.web.security;

import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.SecurityService;
import com.anyservice.service.user.UserHolder;
import com.anyservice.web.security.dto.Credentials;
import com.anyservice.web.security.exceptions.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@Component
@Slf4j
public class SecurityFilter extends OncePerRequestFilter {

    private final SecurityService securityService;
    private final UserHolder userHolder;
    private final UserDetailed innerUser;
    private final FirebaseAuth auth = FirebaseAuth.getInstance();

    public SecurityFilter(SecurityService securityService, UserHolder userHolder,
                          @Lazy UserDetailed innerUser) {
        this.securityService = securityService;
        this.userHolder = userHolder;
        this.innerUser = innerUser;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        processToken(request);
        filterChain.doFilter(request, response);
    }

    /**
     * Process firebase token
     *
     * @param request http request
     */
    private void processToken(HttpServletRequest request) {
        FirebaseToken decodedToken = null;

        boolean isInnerUser = securityService.isInner(request);

        UserDetailed user = null;
        String token = null;

        if (!isInnerUser) {
            // Get token from header
            token = securityService.getBearerToken(request);

            // Try to decoded token
            try {
                if (token != null && !token.equalsIgnoreCase("undefined")) {
                    decodedToken = auth.verifyIdToken(token);
                }
            } catch (FirebaseAuthException e) {
                log.error("Firebase Exception", e);
                throw new FirebaseException("Firebase Exception", e);
            }

            // Get user from decoded token
            if (decodedToken != null) {
                user = securityService.getUserFromFirebaseToken(decodedToken);
            }
        } else {
            user = this.innerUser;
        }

        // If user is null, request unauthorized
        if (user == null) return;

        // Set user to request scope bean
        userHolder.setUser(user);

        // Get all user authorities
        final Collection<GrantedAuthority> authorities = securityService.getUserAuthorities(user);

        // Create spring representation of a user
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
                new Credentials(decodedToken, token), authorities);

        // Set auth details about request
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        // Mark authentication as finished
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
