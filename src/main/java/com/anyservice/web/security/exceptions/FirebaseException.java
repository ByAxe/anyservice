package com.anyservice.web.security.exceptions;

import org.springframework.security.core.AuthenticationException;

public class FirebaseException extends AuthenticationException {


    public FirebaseException(String msg) {
        super(msg);
    }

    public FirebaseException(String msg, Throwable t) {
        super(msg, t);
    }
}
