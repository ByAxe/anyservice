package com.anyservice.web.specification;

import com.anyservice.dto.order.OrderDetailed;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@And({
        @Spec(path = "customer", spec = Equal.class),
        @Spec(path = "state", spec = In.class),
        @Spec(path = "headline", spec = Like.class),
})
public interface IOrderSpec extends Specification<OrderDetailed> {
}
