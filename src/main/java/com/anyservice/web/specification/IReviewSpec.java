package com.anyservice.web.specification;

import com.anyservice.dto.order.Review;
import net.kaczmarzyk.spring.data.jpa.domain.Between;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@And({
        @Spec(path = "contractor", spec = Equal.class),
        @Spec(path = "author", spec = Equal.class),
        @Spec(path = "rating", spec = Between.class),
        @Spec(path = "order", spec = Equal.class),
})
public interface IReviewSpec extends Specification<Review> {
}
