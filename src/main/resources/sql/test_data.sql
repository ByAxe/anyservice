INSERT INTO countries (country, alpha2, alpha3, number)
VALUES ('Poland', 'PL', 'POL', 616),
       ('Lithuania', 'LT', 'LTU', 440),
       ('Latvia', 'LV', 'LVA', 428),
       ('Germany', 'DE', 'DEU', 276),
       ('Switzerland', 'CH', 'CHE', 756),
       ('France', 'FR', 'FRA', 250),
       ('Italy', 'IT', 'ITA', 380),
       ('Spain', 'ES', 'ESP', 724),
       ('Sweden', 'SE', 'SWE', 752),
       ('Any', 'NO', 'NON', 000);

INSERT INTO anyservice.categories (id, dt_create, dt_update, parent_id, title, description)
VALUES ('22c39b5f-b061-4b3a-515a-6c4e0a34a276', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Construction and finishing works', null),
       ('10976441-92f4-0bf9-b2df-1a32e93d50e3', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Transportation', null),
       ('fdc5fa6a-5e2e-d870-0e31-f97cc572f469', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Everything for home', null),
       ('56ee6722-06bd-f2ce-7430-dc9fd8b0a2b7', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Repair of equipment and tools', null),
       ('3c043899-a232-0f16-c353-75eb68edb378', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Cleaning', null),
       ('bfc71413-178a-dc6d-f656-17012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Mobile application development', null),
       ('ffc71413-178a-dc6d-f656-17012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'bfc71413-178a-dc6d-f656-17012d138588', 'Flutter development', null),
       ('ffc71413-178a-dc6d-1111-17012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'ffc71413-178a-dc6d-f656-17012d138588', 'Flutter web development', null),
       ('ffc71413-178a-dc6d-f656-19090d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'ffc71413-178a-dc6d-f656-17012d138588', 'Flutter desktop development', null),
       ('efc71413-178a-dc6d-f656-17012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'bfc71413-178a-dc6d-f656-17012d138588', 'Android development', null),
       ('ffc71413-178a-9090-f656-17012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'bfc71413-178a-dc6d-f656-17012d138588', 'iOS development', null),
       ('ffc71413-178a-dc6d-f656-19012d138588', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'bfc71413-178a-dc6d-f656-17012d138588', 'React Native development', null),
       ('015ca5cb-fa14-07ec-1ee3-e4a8c5c9ac87', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Sport',
        null),
       ('6f6e9d45-2246-c318-03c0-6a57681367ef', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Auto and moto', null),
       ('f9b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Education',
        null),
       ('294a7f68-f6a9-41ea-b200-698c038d1ee9', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'f9b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', 'Chemistry', null),
       ('19b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        'f9b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', 'Math', null),
       ('26150f17-986c-4d16-821d-3c37114d1919', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        '19b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', 'Calculus', null),
       ('f9b65d4e-a0c2-f35b-b0d9-2f8c1adf7e48', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324',
        '19b65d4e-a0c2-f35b-b0d9-2f8c1daf7e48', 'Geometry', null),
       ('8b04221e-8537-17d8-749f-0582862c01f9', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Photo and video', null),
       ('900614c6-3bd1-3feb-f504-3c2edb7ad10b', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Animals',
        null),
       ('bc7428e0-ae81-dbd7-d5a6-23f6c831d9b3', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Design', null),
       ('57e3b3cc-6206-9547-879e-f56f782b35d9', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'SEO and Internet Marketing', null),
       ('3e404c4f-bb32-80af-00c7-c1a2f52c389d', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Helpers and homeworkers', null),
       ('30e5ebb9-7c23-ce92-b226-aab9a9ff55f6', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Jurisprudence', null),
       ('556e7904-3f7d-c660-6ddb-bcd7e91a0128', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Celebrations', null),
       ('9217c628-3106-02c9-5aad-734621b3b350', '2020-09-05 09:18:49.545324', '2020-09-05 09:18:49.545324', null,
        'Accounting and Economics', null);


INSERT INTO anyservice.chats (id, messages)
VALUES ('0f76d6b0-d238-4ae2-9953-c8fac3bf6147', '[]'),
       ('27239b8d-f7f3-4281-8258-b49d27394f67', '[]');

INSERT INTO anyservice.candidates (order_id, user_id, response, chat_id)
VALUES ('c8757244-c992-478f-a5e1-3e94aa9942ce', '1Dw1How83pWcbwn5xitYygxrgMt1', '{
  "text": "Hello! We are professionals and working 7 years on the market already! Check out our latest successful jobs in the profile. It would be pleasure to help you with this"
}', '0f76d6b0-d238-4ae2-9953-c8fac3bf6147'),
       ('c8757244-c992-478f-a5e1-3e94aa9942ce', '3mHCughLfTQTtstYzV9d3JonRs92',
        '{
          "text": "I will make in a week",
          "offeredPrice": 188.422003,
          "priceUpdated": true
        }',
        '0f76d6b0-d238-4ae2-9953-c8fac3bf6147');

INSERT INTO anyservice.reviews (author, contractor, order_id, rating, header, body, contractor_response, compliments)
VALUES ('9CP773EhCNWqVqlRSnPcX8Q3eRk1', '3mHCughLfTQTtstYzV9d3JonRs92', 'faf02295-eeb2-4612-aed2-f3d0ee45aa6d', 4.5,
        'Nice person',
        'It was a great pleasure to work with this person. Very fast and crazy cheap for such a quality!\nDefinitely recommend to everyone.',
        'Thank you for this review :)',
        '[]'),
       ('dfC0EnPUGkVPLbBx2tektL8oKMy2', '3mHCughLfTQTtstYzV9d3JonRs92', '5129a6c3-4fb4-4416-b2e8-961389adfd03', 1.0,
        'Awful service',
        'We talked a lot and nothing was done',
        'We are sorry to hear that. ',
        '[]');
