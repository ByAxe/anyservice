package com.anyservice.config;

import com.anyservice.AnyServiceApplication;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.service.file.storage.api.IStorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.storage.Blob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.TimeZone;


@SpringBootTest(
        classes = {AnyServiceApplication.class}
)
@TestPropertySource(locations = {
        "classpath:environment.properties"
})
@Slf4j
public abstract class TestConfig extends AbstractTransactionalTestNGSpringContextTests {

    protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            StandardCharsets.UTF_8);

    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected Environment environment;
    @Resource
    private FilterChainProxy springSecurityFilterChain;
    @Autowired
    protected IStorageService<Blob, FileDetailed> storageService;
    @Autowired
    private IFirestoreRepository firestoreRepository;

    @Value("${spring.cloud.gcp.firestore.collection.cache}")
    private String cacheCollection;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String usersCollection;

    @BeforeMethod
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain)
                .build();
    }

    @BeforeSuite
    protected void setupSpringAutowiring() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        super.springTestContextBeforeTestClass();
        super.springTestContextPrepareTestInstance();
    }

    @AfterSuite
    protected void removeTestData() throws Exception {
        // Remove all the files from test storage
        storageService.deleteAll();

        // Clear test data
        firestoreRepository.deleteAll(cacheCollection);
        firestoreRepository.deleteAll(usersCollection);
    }
}
