package com.anyservice.core;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.OffsetDateTime;

@UtilityClass
public class RandomValuesGenerator {

    private static final SecureRandom random = new SecureRandom();

    /**
     * Generates random number in range
     *
     * @param min minimum size
     * @param max maximum size
     * @return random integer value
     */
    public static int randomNumber(int min, int max) {
        return random
                .ints(min, (max + 1))
                .findFirst()
                .getAsInt();
    }

    public static boolean randomBoolean() {
        return randomNumber(0, 1) == 0;
    }

    /**
     * Generates random Enum value for given Enum class
     * <p>
     * Example usage:
     * <code>
     * LegalStatus status = randomEnum(LegalStatus.class);
     * </code>
     * Where "status" gets a random value of possible set of values from "LegalStatus" enum
     *
     * @param clazz class of enum
     * @param <T>   Enum type
     * @return random value from enum
     */
    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    /**
     * Generates random string of random length in range
     *
     * @param min minimum size
     * @param max maximum size
     * @return random string of random size
     */
    public static String randomString(int min, int max) {
        int stringLength = randomNumber(min, max);

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'

        return random.ints(leftLimit, rightLimit + 1)
                .limit(stringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    /**
     * Generates random string of given length
     *
     * @param stringLength length of a random string
     * @return Randomly generated string of given size
     */
    public static String randomString(int stringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'

        return random.ints(leftLimit, rightLimit + 1)
                .limit(stringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    /**
     * Generates random {@link OffsetDateTime}
     *
     * @param lowerBound how many days (minimal) randomly add to now
     * @param lowerBound how many days (maximum) randomly add to now
     * @return random {@link OffsetDateTime}
     */
    public static OffsetDateTime randomOffsetDateTime(int lowerBound, int upperBound) {
        return OffsetDateTime.now().plusDays(randomNumber(lowerBound, upperBound));
    }

    public static BigDecimal randomBigDecimal() {
        String whole = String.valueOf(randomNumber(0, 1_000));
        String decimal = String.valueOf(randomNumber(0, 1_000_000));

        String numberAsString = whole + "." + decimal;

        return new BigDecimal(numberAsString);
    }

    public static BigDecimal randomRating() {
        String whole = String.valueOf(randomNumber(1, 5));

        String numberAsString = whole + ".0";

        return new BigDecimal(numberAsString);
    }


}
