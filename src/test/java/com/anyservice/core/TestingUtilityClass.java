package com.anyservice.core;

import lombok.experimental.UtilityClass;
import org.testng.Assert;

import java.util.UUID;

@UtilityClass
public class TestingUtilityClass {
    public static final boolean SUCCESS = true;
    public static final boolean FAIL = false;

    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 50;

    /**
     * Common operation to get UUID from a raw header "Location"
     *
     * @param headerLocation content of header
     * @return {@link UUID}
     */
    public static UUID getUuidFromHeaderLocation(String headerLocation) {
        return UUID.fromString(String.valueOf(getIdFromHeaderLocation(headerLocation)));
    }

    /**
     * General method for getting ID from header
     * <p>
     * Tries to cast id to {@link UUID} and if cast fails - returns bare {@link String}
     *
     * @param headerLocation content of header
     * @return ID as {@link UUID} or {@link String}
     */
    public static Object getIdFromHeaderLocation(String headerLocation) {
        Assert.assertNotNull(headerLocation);

        // Split location on components
        String[] locationComponents = headerLocation.split("/");

        // Calculate last component (id) index
        int lastElementIdx = locationComponents.length - 1;

        // Get id
        String id = locationComponents[lastElementIdx];

        try {
            // Try to convert it to uuid, if the format is appropriate
            return UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // Or else return id as it was in location header
            return id;
        }
    }
}
