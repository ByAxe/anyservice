package com.anyservice.core.services;

import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Chat;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.order.Response;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.service.order.api.IOrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

import static com.anyservice.core.RandomValuesGenerator.randomBigDecimal;
import static com.anyservice.core.RandomValuesGenerator.randomString;
import static com.anyservice.core.TestingUtilityClass.getUuidFromHeaderLocation;
import static com.anyservice.tests.api.ICRUDOperations.expectCreated;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@Service
public class CandidateUtilityService {

    @Autowired
    private UserUtilityService userUtilityService;

    @Autowired
    private ChatUtilityService chatUtilityService;

    @Autowired
    private IFirestoreRepository firestoreRepository;

    @Autowired
    private CustomConversionService conversionService;

    @Autowired
    private IOrderService orderService;

    @Value("${order.candidate.response.min_characters}")
    private int minimalResponseTextCharacters;

    /**
     * Create candidate without chat in it
     *
     * @param orderUuid order identifier for that candidate belongs to
     * @return Created candidate and its uuid
     */
    @SneakyThrows
    public DetailedWrapper<Candidate> createCandidate(UUID orderUuid, MockMvc mockMvc,
                                                      ObjectMapper objectMapper, HttpHeaders headers) {
        // Generate candidate
        Candidate candidate = generateCandidate(orderUuid);

        // Convert it to string representation
        String itemAsString = objectMapper.writeValueAsString(candidate);

        // Perform request
        String headerLocation = mockMvc.perform(post("/api/v1/candidate")
                .headers(headers)
                .contentType(APPLICATION_JSON)
                .content(itemAsString))
                .andExpect(expectCreated)
                .andReturn()
                .getResponse()
                .getHeader("Location");

        // Get newly created candidate's identifier from header
        UUID uuid = getUuidFromHeaderLocation(headerLocation);

        return DetailedWrapper.<Candidate>builder()
                .detailed(candidate)
                .id(uuid)
                .build();
    }

    /**
     * Generate valid candidate
     *
     * @param orderUuid for that candidate belongs to
     * @return candidate with randomly generated values
     */
    public Candidate generateCandidate(UUID orderUuid) {
        Candidate candidate = Candidate.builder()
                .id(UUID.randomUUID())
                .dtUpdate(OffsetDateTime.now())
                .dtCreate(OffsetDateTime.now())
                .response(generateResponse())
                .user(userUtilityService.createWorker())
                .orderId(orderUuid)
                .build();

        Optional<OrderDetailed> orderOptional = orderService.findById(orderUuid);

        String customerId = UUID.randomUUID().toString();

        // Get customer id
        if (orderOptional.isPresent()) {
            customerId = (String) orderOptional
                    .get()
                    .getCustomer()
                    .getId();
        }


        // Get candidate id
        String candidateId = (String) candidate.getUser().getId();

        // Create chat
        Chat chat = chatUtilityService.createChat(customerId, candidateId);

        // Set created chat to candidate
        candidate.setChat(chat);

        return candidate;
    }

    /**
     * Generate response of candidate
     *
     * @return candidate's response
     */
    private Response generateResponse() {
        return Response.builder()
                .offeredPrice(randomBigDecimal())
                .text(randomString(minimalResponseTextCharacters, 100))
                .build();
    }

    /**
     * Assert that two candidates are equal via basic set of fields
     *
     * @param actual   actual candidate
     * @param expected expected candidate
     */
    public void assertCandidatesAreEqual(Candidate actual, Candidate expected) {
        if (expected == null) {
            Assert.assertNull(actual);
            return;
        }

        Assert.assertEquals(actual.getOrderId(), expected.getOrderId());

        // Assert that users are equal
        UserBrief actualUser = actual.getUser();
        UserBrief expectedUser = expected.getUser();

        userUtilityService.assertUsersAreEqual(actualUser, expectedUser);

        Assert.assertEquals(actual.getResponse(), expected.getResponse());

        // Assert that chats are equal
        Chat expectedChat = expected.getChat();
        Chat actualChat = actual.getChat();

        if (expectedChat == null) {
            Assert.assertNull(actualChat);
            return;
        }

        Assert.assertEquals(actualChat.getMessages(), expectedChat.getMessages());
    }
}
