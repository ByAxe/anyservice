package com.anyservice.core.services;

import com.anyservice.core.enums.MessageStatus;
import com.anyservice.dto.order.Chat;
import com.anyservice.entity.order.Message;
import com.anyservice.service.order.api.IChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.anyservice.core.RandomValuesGenerator.*;

@Service
public class ChatUtilityService {

    @Autowired
    private IChatService chatService;

    /**
     * Generate random chat
     *
     * @param customerId  customer
     * @param candidateId candidate for an order
     * @return randomly generated chat
     */
    public Chat generateChat(String customerId, String candidateId) {
        return Chat.builder()
                .id(UUID.randomUUID().toString())
                .messages(generateMessages(customerId, candidateId))
                .build();
    }

    /**
     * Create random chat
     *
     * @param customerId  first person
     * @param candidateId second person
     * @return created chat
     */
    public Chat createChat(String customerId, String candidateId) {
        Chat chat = generateChat(customerId, candidateId);

        return chatService.create(chat);
    }

    /**
     * Generate random dialog between two person
     *
     * @param customerId  first person
     * @param candidateId second person
     * @return list of randomly generated messages
     */
    private List<Message> generateMessages(String customerId, String candidateId) {
        ArrayList<Message> messages = new ArrayList<>();

        // Generate random amount of messages
        int amountOfMessages = randomNumber(0, 10);

        // If there is nothing - return empty list
        if (amountOfMessages == 0) return messages;

        // Or else get random amounts of messages for both sides
        int amountOfCandidatesMessages = randomNumber(1, amountOfMessages);
        int amountOfCustomerMessages = amountOfMessages - amountOfCandidatesMessages;

        // Generate messages for the both sides with random amount of messages
        messages.addAll(generateMessages(customerId, amountOfCustomerMessages));
        messages.addAll(generateMessages(candidateId, amountOfCandidatesMessages));

        return messages;
    }

    /**
     * Generate given amount of messages from given user
     *
     * @param userId           user identifier
     * @param amountOfMessages amount of messages to generate
     * @return list of randomly generated messages
     */
    private List<Message> generateMessages(String userId, int amountOfMessages) {
        return IntStream.range(0, amountOfMessages)
                .mapToObj(i -> generateMessage(userId))
                .collect(Collectors.toList());
    }

    /**
     * Generate random message from given user
     *
     * @param userId user identifier
     * @return randomly generated message
     */
    private Message generateMessage(String userId) {
        return Message.builder()
                .senderId(userId)
                .status(randomEnum(MessageStatus.class))
                .text(randomString(1, 100))
                .dateTime(OffsetDateTime.now())
                .build();
    }
}
