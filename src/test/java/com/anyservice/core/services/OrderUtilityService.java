package com.anyservice.core.services;

import com.anyservice.core.DateUtils;
import com.anyservice.core.FileTestUtils;
import com.anyservice.core.RandomValuesGenerator;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Category;
import com.anyservice.dto.order.Location;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.entity.CountryEntity;
import com.anyservice.repository.CountryRepository;
import com.anyservice.service.order.api.ICategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.anyservice.core.RandomValuesGenerator.*;
import static com.anyservice.core.TestingUtilityClass.getUuidFromHeaderLocation;
import static com.anyservice.core.enums.OrderState.*;
import static com.anyservice.tests.api.ICRUDOperations.expectCreated;
import static com.anyservice.tests.api.ICRUDOperations.expectOk;
import static java.util.Comparator.comparing;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Service
public class OrderUtilityService {

    private final List<CountryEntity> countries = new ArrayList<>();

    private FileTestUtils fileTestUtils;
    @Autowired
    private UserUtilityService userUtilityService;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CandidateUtilityService candidateUtilityService;

    @Autowired
    private ICategoryService categoryService;

    @Value("${order.min_deadline}")
    private int minimalDeadlinePlusDays;

    @Value("${order.max_attachments}")
    private int maxAttachments;


    /**
     * Create published order
     *
     * @param mockMvc
     * @param objectMapper
     * @param headers
     * @return id of created order
     * @throws Exception
     */
    @SneakyThrows
    public DetailedWrapper<OrderDetailed> createPublishedOrder(MockMvc mockMvc, ObjectMapper objectMapper,
                                                               HttpHeaders headers) {

        // Generate order
        OrderDetailed orderDetailed = generateOrder(mockMvc, objectMapper, headers);

        String itemAsString = objectMapper.writeValueAsString(orderDetailed);

        // Perform request
        String headerLocation = mockMvc.perform(post("/api/v1/order")
                .headers(headers)
                .contentType(APPLICATION_JSON)
                .content(itemAsString))
                .andExpect(expectCreated)
                .andReturn()
                .getResponse()
                .getHeader("Location");

        UUID uuid = getUuidFromHeaderLocation(headerLocation);

        return DetailedWrapper.<OrderDetailed>builder()
                .detailed(orderDetailed)
                .id(uuid)
                .build();
    }

    /**
     * Create completed order
     *
     * @param mockMvc
     * @param objectMapper
     * @param headers
     * @return
     */
    @SneakyThrows
    public OrderDetailed createCompletedOrder(MockMvc mockMvc, ObjectMapper objectMapper, HttpHeaders headers) {
        DetailedWrapper<OrderDetailed> publishedOrder = createPublishedOrder(mockMvc, objectMapper, headers);

        UUID orderUuid = publishedOrder.getId();

        // Select created order
        String contentAsString = mockMvc.perform(get("/api/v1/order/" + orderUuid)
                .headers(headers)
                .contentType(APPLICATION_JSON))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        OrderDetailed selectedOrder = objectMapper.readValue(contentAsString,
                objectMapper.getTypeFactory().constructType(OrderDetailed.class));

        // Create candidate
        DetailedWrapper<Candidate> candidateWrapper = candidateUtilityService
                .createCandidate(orderUuid, mockMvc, objectMapper, headers);

        // Mark candidate as selected
        Candidate candidate = candidateWrapper.getDetailed();
        candidate.setSelected(true);
        List<Candidate> candidates = Collections.singletonList(candidate);

        // Set candidate to an order
        selectedOrder.setCandidates(candidates);
        selectedOrder.setResponses(candidates.size());


        // Update order with state candidate selected
        selectedOrder.setState(CANDIDATE_SELECTED);

        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        String updatedItemAsString = objectMapper.writeValueAsString(selectedOrder);

        mockMvc.perform(put("/api/v1/order/" + orderUuid + "/version/" + version)
                .headers(headers)
                .contentType(APPLICATION_JSON)
                .content(updatedItemAsString))
                .andExpect(expectOk);

        // Select updated order
        contentAsString = mockMvc.perform(get("/api/v1/order/" + orderUuid)
                .headers(headers)
                .contentType(APPLICATION_JSON))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        OrderDetailed updatedOrder = objectMapper.readValue(contentAsString,
                objectMapper.getTypeFactory().constructType(OrderDetailed.class));

        // Mark order as completed
        updatedOrder.setState(COMPLETED);

        version = DateUtils.convertOffsetDateTimeToMills(updatedOrder.getDtUpdate());

        updatedItemAsString = objectMapper.writeValueAsString(updatedOrder);

        // Update order again
        mockMvc.perform(put("/api/v1/order/" + orderUuid + "/version/" + version)
                .headers(headers)
                .contentType(APPLICATION_JSON)
                .content(updatedItemAsString))
                .andExpect(expectOk);

        // Select updated order
        contentAsString = mockMvc.perform(get("/api/v1/order/" + orderUuid)
                .headers(headers)
                .contentType(APPLICATION_JSON))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        return objectMapper.readValue(contentAsString,
                objectMapper.getTypeFactory().constructType(OrderDetailed.class));
    }

    @Synchronized
    @SneakyThrows
    public OrderDetailed generateOrder(MockMvc mockMvc, ObjectMapper objectMapper, HttpHeaders headers) {

        // Initialize file utils
        if (fileTestUtils == null) {
            fileTestUtils = new FileTestUtils(mockMvc, objectMapper, headers);
        }

        // Generate random deadline
        OffsetDateTime deadline = randomBoolean()
                ? randomOffsetDateTime(minimalDeadlinePlusDays, 365)
                : null;

        OrderDetailed order = OrderDetailed.builder()
                .deadline(deadline)
                .description(randomString(10, 100))
                .headline(randomString(10, 100))
                .location(generateLocation())
                .price(randomBigDecimal())
                .phone(randomString(10, 50))
                .state(PUBLISHED)
                .customer(userUtilityService.createUser())
                .build();

        // Generate random amount of attachments
        int amountOfAttachments = randomNumber(0, maxAttachments - 1);

        if (amountOfAttachments != 0) {
            List<FileDetailed> attachments = new ArrayList<>();

            for (int i = 0; i < amountOfAttachments; i++) {
                attachments.add(fileTestUtils.createAttachment().y());
            }

            FileDetailed mainAttachment = fileTestUtils.createMainAttachment().y();

            order.setMainAttachment(mainAttachment);
            order.setAttachments(attachments);
        }

        // Create category to which order belongs to
        Category category = createCategory();

        // Set it to an order
        order.setCategories(Collections.singletonList(category));

        return order;
    }


    /**
     * Create random category for an order
     *
     * @return valid randomly created category
     */
    private Category createCategory() {
        // Build category
        Category category = Category.builder()
                .id(UUID.randomUUID())
                .title(RandomValuesGenerator.randomString(1, 50))
                .description(RandomValuesGenerator.randomString(1, 200))
                .build();

        // Create and return category
        return categoryService.create(category);
    }


    private Location generateLocation() {
        return Location.builder()
                .zipCode(randomString(0, 20))
                .addressLine1(randomString(0, 100))
                .addressLine2(randomString(0, 100))
                .country(generateCountry())
                .build();
    }

    /**
     * Create fixed amount of countries and return random one
     *
     * @return random created country
     */
    @Synchronized
    private CountryEntity generateCountry() {
        if (countries.isEmpty()) {
            // Put all countries into list
            countries.addAll(countryRepository.findAll());
        }

        // Return random one
        return countries.get(randomNumber(0, countries.size() - 1));

    }

    /**
     * Assert that two orders are equal via basic set of fields
     *
     * @param actual   actual order
     * @param expected expected order
     */
    public void assertOrdersAreEqual(OrderDetailed actual, OrderDetailed expected) {
        Assert.assertEquals(actual.getPhone(), expected.getPhone());

        // Assert that users are equal
        UserBrief actualCustomer = actual.getCustomer();
        UserBrief expectedCustomer = expected.getCustomer();

        userUtilityService.assertUsersAreEqual(actualCustomer, expectedCustomer);

        Assert.assertEquals(actual.getDeadline(), expected.getDeadline());
        Assert.assertEquals(actual.getDescription(), expected.getDescription());
        Assert.assertEquals(actual.getHeadline(), expected.getHeadline());
        Assert.assertEquals(actual.getLocation(), expected.getLocation());
        Assert.assertEquals(actual.getPrice(), expected.getPrice());
        Assert.assertEquals(actual.getState(), expected.getState());
        Assert.assertEquals(actual.getResponses(), expected.getResponses());
        Assert.assertEquals(actual.getMainAttachment(), expected.getMainAttachment());

        // Compare attachments lists
        List<FileDetailed> actualAttachments = actual.getAttachments();
        List<FileDetailed> expectedAttachments = expected.getAttachments();

        if (actualAttachments != null && expectedAttachments != null) {
            actualAttachments.sort(comparing(FileDetailed::getName));
            expectedAttachments.sort(comparing(FileDetailed::getName));
        }

        Assert.assertEquals(actualAttachments, expectedAttachments);

        // Compare candidates lists
        List<Candidate> actualCandidates = actual.getCandidates();
        List<Candidate> expectedCandidates = expected.getCandidates();

        if (actualCandidates != null && expectedCandidates != null) {
            actualCandidates.sort(comparing(Candidate::getOrderId));
            expectedCandidates.sort(comparing(Candidate::getOrderId));

            for (int element = 0; element < actualCandidates.size(); element++) {
                Candidate actualCandidate = actualCandidates.get(element);
                Candidate expectedCandidate = expectedCandidates.get(element);

                candidateUtilityService.assertCandidatesAreEqual(actualCandidate, expectedCandidate);
            }
        }
    }
}
