package com.anyservice.core.services;

import com.anyservice.core.enums.LegalStatus;
import com.anyservice.core.enums.UserRole;
import com.anyservice.core.enums.UserState;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.CountryEntity;
import com.anyservice.entity.firestore.User;
import com.anyservice.repository.CountryRepository;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.testng.Assert;

import java.time.OffsetDateTime;
import java.util.*;

import static com.anyservice.core.RandomValuesGenerator.*;
import static org.apache.commons.lang3.RandomStringUtils.random;

@Service
public class UserUtilityService {

    private final List<CountryEntity> countries = new ArrayList<>();

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CustomConversionService conversionService;

    @Autowired
    private IFirestoreRepository firestoreRepository;

    @Value("${spring.mail.username}")
    private String mailLogin;

    @Value("${user.validation.password.length.min}")
    private int passwordMinLength;

    @Value("${user.validation.password.length.max}")
    private int passwordMaxLength;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String usersCollection;

    public UserDetailed buildUserDetailed() {
        return UserDetailed.builder()
                .name(random(randomNumber(2, 100), true, false))
                .contacts(createContacts())
                .email("test" + randomString(10, 20) + "@" + randomString(3, 10) + ".com")
                .description(randomString(0, 1000))
                .isVerified(false)
                .isLegalStatusVerified(false)
                .legalStatus(null)
                .password(random(randomNumber(passwordMinLength, passwordMaxLength), true, true))
                .addresses(createAddresses())
                .defaultCountry(createCountry())
                .listOfCountriesWhereServicesProvided(createServiceCountries())
                .build();
    }

    /**
     * Build fully prepared and activated user
     *
     * @return
     */
    public UserDetailed buildActivatedUser() {
        // Build verified user
        UserDetailed user = buildUserDetailed();

        OffsetDateTime now = OffsetDateTime.now();

        user.setDtCreate(now);
        user.setDtUpdate(now);
        user.setPasswordUpdateDate(now);

        // Set random id
        user.setId(UUID.randomUUID().toString());

        // Verify user
        user.setVerified(true);
        user.setState(UserState.ACTIVE);
        user.setRole(UserRole.ROLE_USER);

        return user;
    }

    /**
     * Create user
     *
     * @return user instance
     */
    @SneakyThrows
    public UserDetailed createUser() {
        UserDetailed user = buildActivatedUser();

        @NonNull User userEntity = conversionService.convert(user, User.class);

        // Create user directly through repository layer
        firestoreRepository.saveSync(usersCollection, userEntity.getId(), userEntity);

        // Return brief of a user
        return user;
    }

    /**
     * Create worker that can be valid candidate for an order
     *
     * @return user instance
     */
    @SneakyThrows
    public UserDetailed createWorker() {
        UserDetailed user = buildActivatedUser();

        user.setLegalStatusVerified(true);
        user.setLegalStatus(randomEnum(LegalStatus.class));

        @NonNull User userEntity = conversionService.convert(user, User.class);

        // Create user directly through repository layer
        firestoreRepository.saveAsync(usersCollection, userEntity.getId(), userEntity);

        // Return brief of a user
        return user;
    }

    /**
     * Generate random contacts for a user
     *
     * @return random Contacts
     */
    private Map<String, Object> createContacts() {
        Map<String, Object> contacts = new HashMap<>();

        contacts.put("email", mailLogin);
        contacts.put("phone", random(randomNumber(6, 50), false, true));

        return contacts;
    }

    /**
     * Generate random number of random addresses
     *
     * @return map of random addresses
     */
    private Map<String, String> createAddresses() {

        // Generate random amount of address
        int addressesAmount = randomNumber(0, 10);

        // No addresses means null
        if (addressesAmount == 0) return null;

        Map<String, String> addresses = new HashMap<>();

        // Generate random addresses
        for (int i = 0; i < addressesAmount; i++) {
            String randomAddressName = randomString(1, 50);
            String randomAddressLocation = randomString(1, 100);

            addresses.put(randomAddressName, randomAddressLocation);
        }

        return addresses;
    }

    /**
     * Create countries where services is available
     *
     * @return randomly created services countries list
     */
    @Synchronized
    private List<CountryEntity> createServiceCountries() {
        if (countries.isEmpty()) {
            // Put all countries into list
            countries.addAll(countryRepository.findAll());
        }

        // Use Set to avoid duplicates
        Set<CountryEntity> countriesSet = new HashSet<>();

        // Get random amount of countries to generate
        int randomAmountOfAvailableCountries = randomNumber(0, countries.size() - 1);

        // Add to set this random amount of countries
        for (int i = 0; i < randomAmountOfAvailableCountries; i++) {
            CountryEntity country = countries.get(randomNumber(0, countries.size() - 1));

            countriesSet.add(country);
        }

        // Return list without duplicating countries
        return new ArrayList<>(countriesSet);
    }

    /**
     * Create fixed amount of countries and return random one
     *
     * @return random created country
     */
    @Synchronized
    private CountryEntity createCountry() {
        if (countries.isEmpty()) {
            // Put all countries into list
            countries.addAll(countryRepository.findAll());
        }

        // Return random one
        return countries.get(randomNumber(0, countries.size() - 1));

    }

    /**
     * Assertion that two users are equal by basic fields set
     *
     * @param actual   actual user
     * @param expected expected user
     */
    public void assertUsersAreEqual(UserBrief actual, UserBrief expected) {
        if (actual != null) {
            Assert.assertNotNull(expected);

            Assert.assertEquals(actual.getEmail(), expected.getEmail());
            Assert.assertEquals(actual.getName(), expected.getName());
            Assert.assertEquals(actual.getRole(), expected.getRole());
            Assert.assertEquals(actual.getState(), expected.getState());
            Assert.assertEquals(actual.getIssuer(), expected.getIssuer());
            Assert.assertEquals(actual.getPicture(), expected.getPicture());
        }
    }
}
