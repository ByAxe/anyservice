package com.anyservice.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetailedWrapper<DETAILED> {
    private DETAILED detailed;
    private Object id;

    /**
     * BAD PRACTICE - Everyone gets exactly what he wants to get
     * <p>
     * Added to fix the presence of String id and UUID simultaneously
     * <p>
     * TODO remove when all the database moves to Firestore
     *
     * @param <V> type for id object to be casted
     * @return id with given type
     */
    @SuppressWarnings("unchecked")
    public <V> V getId() {
        return (V) id;
    }
}
