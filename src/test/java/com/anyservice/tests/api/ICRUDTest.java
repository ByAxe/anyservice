package com.anyservice.tests.api;

import com.anyservice.core.DateUtils;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.api.APrimary;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public interface ICRUDTest<BRIEF extends APrimary, DETAILED extends APrimary>
        extends ICRUDOperations<BRIEF, DETAILED> {

    /**
     * Get url prefix
     *
     * @return url as {@link String}
     */
    default String getUrlPrefix() {
        return "/api/v1";
    }

    /**
     * Creates a few items and then ensures created are equal to those were meant to be created
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void createAndSelectTest() throws Exception {
        // Select all existing at the beginning elements
        List<BRIEF> expected = selectAll();

        // Create items
        for (int i = 0; i < 10; i++) expected.add((BRIEF) create().getDetailed());

        // Select all created elements
        List<BRIEF> actual = selectAll();

        // Compare it
        assertEqualsListBrief(actual, expected);
    }

    /**
     * Measures amount of elements before and after creation of some
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void countTest() throws Exception {
        // Measure amount of elements
        int expected = count();

        // Create detailed
        create();

        // increase expected by amount of created elements
        expected++;

        // Measure amount of elements after creation
        int actual = count();

        Assert.assertEquals(actual, expected);
    }

    /**
     * Creates and then removes detailed
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void deleteAndSelectByUUIDTest() throws Exception {
        // create detailed
        DetailedWrapper<DETAILED> detailedWrapper = create();
        Object id = detailedWrapper.getId();

        // select by id
        DETAILED obtainedResult = select(id);

        long version = DateUtils.convertOffsetDateTimeToMills(obtainedResult.getDtUpdate());

        // delete this row
        remove(id, version);

        // select again by id and expect there will be no entity returned
        select(id, expectNoContent);
    }

    /**
     * Creates and updates detailed
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void updateTest() throws Exception {
        // Create detailed
        DetailedWrapper<DETAILED> detailedWrapper = create();
        Object id = detailedWrapper.getId();

        // select by id
        DETAILED obtainedResult = select(id);

        long version = DateUtils.convertOffsetDateTimeToMills(obtainedResult.getDtUpdate());

        // Update whole detailed
        DETAILED updatedItem = update(id, version);

        // Select updated row by id
        obtainedResult = select(id);

        assertEqualsDetailed(obtainedResult, updatedItem);
    }

    /**
     * Create a few elements and them find them all by id list
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void createAndFindAllByIdListTest() throws Exception {
        List<DETAILED> items = new ArrayList<>();
        List<Object> idList = new ArrayList<>();

        // Create items
        for (int i = 0; i < 10; i++) {
            DetailedWrapper<DETAILED> detailed = create();

            items.add(detailed.getDetailed());
            idList.add(detailed.getId());
        }

        // Select list of briefs by list of id
        List<BRIEF> actual = selectAll(idList);

        // Convert our detailed list to brief list
        List<BRIEF> expected = items.stream()
                .map(e -> (BRIEF) e)
                .collect(Collectors.toList());

        // Ensure that they are equal
        assertEqualsListBrief(actual, expected);
    }

    /**
     * Create an element and make sure exists method will confirm that
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    default void createAndCheckIfExists() throws Exception {
        DetailedWrapper<DETAILED> userWrapper = create();

        Object id = userWrapper.getId();

        boolean exists = exists(id);

        Assert.assertTrue(exists);
    }

}
