package com.anyservice.tests.dataproviders;

import com.anyservice.core.enums.FileType;
import com.anyservice.entity.order.Response;
import lombok.experimental.UtilityClass;
import org.testng.annotations.DataProvider;

import java.time.OffsetDateTime;

import static com.anyservice.core.RandomValuesGenerator.*;
import static com.anyservice.core.enums.OrderState.*;
import static com.anyservice.tests.api.ICRUDOperations.*;
import static java.math.BigDecimal.ZERO;

@UtilityClass
public class OrderDataProvider {

    /**
     * Create order with this state and expect result
     *
     * @return {order_status,http_status_after_request}
     */
    @DataProvider
    public static Object[][] createWithStatusesDataProvider() {
        return new Object[][]{
                {DRAFT, expectCreated},
                {PUBLISHED, expectCreated},
                {EDITED, expectBadRequest},
                {CANDIDATE_SELECTED, expectBadRequest},
                {COMPLETED, expectBadRequest},
                {NOT_COMPLETED, expectBadRequest},
                {CANCELLED, expectBadRequest},
                {BLOCKED, expectBadRequest},
                {REMOVED, expectBadRequest},
        };
    }

    @DataProvider
    public static Object[][] createWithVariablyValidatedDataProvider() {
        return new Object[][]{
                {true, PUBLISHED, expectCreated},
                {true, DRAFT, expectCreated},
                {false, DRAFT, expectCreated},


                // TODO commented temporarily while verification is turned off
//                {false, PUBLISHED, expectBadRequest},
        };
    }

    @DataProvider
    public static Object[][] amountOfAttachmentsDataProvider() {
        boolean moreThanMaxAttachments = true;
        return new Object[][]{
                {!moreThanMaxAttachments, expectOk},
                {moreThanMaxAttachments, expectBadRequest}
        };
    }

    /**
     * @return {isVerifiedCandidate, isLegallyVerifiedCandidate, expectedStatus}
     */
    @DataProvider
    public static Object[][] updateWithNonLegallyValidatedCandidatesDataProvider() {
        return new Object[][]{
                {false, false, expectBadRequest},
                {false, true, expectBadRequest},
                {true, false, expectBadRequest},
                {true, true, expectOk},
        };
    }

    @DataProvider
    public static Object[][] candidateSelectionDataProvider() {
        boolean candidateSelected = true;
        boolean candidateNotSelected = false;

        return new Object[][]{
                {candidateSelected, DRAFT, expectBadRequest},
                {candidateNotSelected, DRAFT, expectBadRequest},

                {candidateSelected, PUBLISHED, expectBadRequest},
                {candidateNotSelected, PUBLISHED, expectOk},

                {candidateSelected, EDITED, expectBadRequest},
                {candidateNotSelected, EDITED, expectOk},

                {candidateSelected, CANDIDATE_SELECTED, expectOk},
                {candidateNotSelected, CANDIDATE_SELECTED, expectBadRequest},

                {candidateSelected, COMPLETED, expectOk},
                {candidateNotSelected, COMPLETED, expectBadRequest},

                {candidateSelected, NOT_COMPLETED, expectOk},
                {candidateNotSelected, NOT_COMPLETED, expectBadRequest},

                {candidateSelected, CANCELLED, expectBadRequest},
                {candidateNotSelected, CANCELLED, expectOk},

                {candidateSelected, BLOCKED, expectOk},
                {candidateNotSelected, BLOCKED, expectOk},

                {candidateSelected, REMOVED, expectBadRequest},
                {candidateNotSelected, REMOVED, expectBadRequest},
        };
    }

    @DataProvider
    public static Object[][] orderUuidInCandidatesDataProvider() {
        return new Object[][]{
                {true, expectOk},
                {false, expectBadRequest}
        };
    }

    @DataProvider
    public static Object[][] multipleCandidatesSelectionDataProvider() {
        return new Object[][]{
                {1, expectOk},
                {randomNumber(2, 10), expectBadRequest}
        };
    }


    @DataProvider
    public static Object[][] updateSelectedCandidateDataProvider() {
        return new Object[][]{
                {false, expectOk},
                {true, expectOk}
        };
    }


    @DataProvider
    public static Object[][] amountOfResponsesDataProvider() {
        return new Object[][]{
                {false, expectBadRequest},
                {true, expectOk}
        };
    }


    @DataProvider
    public static Object[][] customerAndCandidateTheSamePersonDataProvider() {
        return new Object[][]{
                {false, expectOk},
                {true, expectBadRequest}
        };
    }


    @DataProvider
    public static Object[][] updateOrderWithoutCustomerDataProvider() {
        return new Object[][]{
                {false, expectBadRequest},
                {true, expectOk}
        };
    }


    @DataProvider
    public static Object[][] setCandidateSelectedWithoutCandidatesDataProvider() {
        boolean isCandidatesPresent = true;
        return new Object[][]{
                {!isCandidatesPresent, expectBadRequest},
                {isCandidatesPresent, expectOk}
        };
    }

    @DataProvider
    public static Object[][] attachmentsOfDifferentTypesDataProvider() {
        return new Object[][]{
                {FileType.ATTACHMENT, expectOk},
                {FileType.DOCUMENT, expectBadRequest},
                {FileType.PROFILE_PHOTO, expectBadRequest},
                {FileType.PORTFOLIO, expectBadRequest},
        };
    }

    @DataProvider
    public static Object[][] withoutSomeAttachmentDataProvider() {
        boolean withMainAttachment = true;
        boolean withoutMainAttachment = false;

        boolean withAttachments = true;
        boolean withoutAttachments = false;

        return new Object[][]{
                {withMainAttachment, withAttachments, expectOk},
                {withMainAttachment, withoutAttachments, expectOk},
                {withoutMainAttachment, withAttachments, expectBadRequest},
                {withoutMainAttachment, withoutAttachments, expectOk},
        };
    }


    @DataProvider
    public static Object[][] createDraftDataProvider() {
        return new Object[][]{
                {"", "", expectBadRequest},
                {null, null, expectBadRequest},
                {null, "", expectBadRequest},
                {"", null, expectBadRequest},
                {randomString(1, 50), "", expectCreated},
                {randomString(1, 50), null, expectCreated},
                {"", randomString(1, 50), expectCreated},
                {null, randomString(1, 50), expectCreated},
        };
    }

    @DataProvider
    public static Object[][] updateDraftDataProvider() {
        return new Object[][]{
                {"", "", expectBadRequest},
                {null, null, expectBadRequest},
                {null, "", expectBadRequest},
                {"", null, expectBadRequest},
                {randomString(1, 50), "", expectOk},
                {randomString(1, 50), null, expectOk},
                {"", randomString(1, 50), expectOk},
                {null, randomString(1, 50), expectOk},
        };
    }

    @DataProvider
    public static Object[][] createOrderAndUpdateItsStateToPublishedDataProvider() {
        return new Object[][]{
                {DRAFT, expectOk},
                {PUBLISHED, expectOk},
        };
    }

    @DataProvider
    public static Object[][] createPublishedDataProvider() {
        return new Object[][]{
                {randomString(1, 50), randomString(1, 50), randomBigDecimal(), expectCreated},
                {randomString(1, 50), randomString(1, 50), ZERO, expectBadRequest},
                {randomString(1, 50), null, randomBigDecimal(), expectBadRequest},
                {null, randomString(1, 50), randomBigDecimal(), expectBadRequest},
                {"", "", randomBigDecimal(), expectBadRequest},
                {"", null, randomBigDecimal(), expectBadRequest},
                {null, "", randomBigDecimal(), expectBadRequest},
                {null, null, randomBigDecimal(), expectBadRequest},
                {null, null, null, expectBadRequest},
                {null, null, ZERO, expectBadRequest},
                {null, "", ZERO, expectBadRequest},
                {"", null, ZERO, expectBadRequest},
                {randomString(1, 50), null, ZERO, expectBadRequest},
                {randomString(1, 50), "", ZERO, expectBadRequest},
                {"", randomString(1, 50), ZERO, expectBadRequest},
                {"", "", null, expectBadRequest},
        };
    }


    @DataProvider
    public static Object[][] updatePublishedDataProvider() {
        return new Object[][]{
                {randomString(1, 50), randomString(1, 50), randomBigDecimal(), expectOk},
                {randomString(1, 50), randomString(1, 50), ZERO, expectBadRequest},
                {randomString(1, 50), null, randomBigDecimal(), expectBadRequest},
                {null, randomString(1, 50), randomBigDecimal(), expectBadRequest},
                {"", "", randomBigDecimal(), expectBadRequest},
                {"", null, randomBigDecimal(), expectBadRequest},
                {null, "", randomBigDecimal(), expectBadRequest},
                {null, null, randomBigDecimal(), expectBadRequest},
                {null, null, null, expectBadRequest},
                {null, null, ZERO, expectBadRequest},
                {null, "", ZERO, expectBadRequest},
                {"", null, ZERO, expectBadRequest},
                {randomString(1, 50), null, ZERO, expectBadRequest},
                {randomString(1, 50), "", ZERO, expectBadRequest},
                {"", randomString(1, 50), ZERO, expectBadRequest},
                {"", "", null, expectBadRequest},
        };
    }


    @DataProvider
    public static Object[][] candidateResponseDataProvider() {
        return new Object[][]{

                {Response.builder()
                        .offeredPrice(randomBigDecimal())
                        .text(randomString(2, 100))
                        .build(), expectOk},

                {Response.builder()
                        .text(randomString(2, 100))
                        .build(), expectOk},

                {Response.builder()
                        .text(randomString(2, 100))
                        .isPriceUpdated(true)
                        .build(), expectBadRequest},

                {Response.builder()
                        .offeredPrice(ZERO)
                        .text(randomString(2, 100))
                        .isPriceUpdated(true)
                        .build(), expectBadRequest},

                {Response.builder()
                        .offeredPrice(randomBigDecimal())
                        .text(randomString(0, 1))
                        .build(), expectBadRequest},

                {Response.builder()
                        .offeredPrice(ZERO)
                        .text(randomString(0, 1))
                        .build(), expectBadRequest},

                {Response.builder()
                        .offeredPrice(ZERO)
                        .build(), expectBadRequest},

                {Response.builder()
                        .build(), expectBadRequest},
        };
    }

    @DataProvider
    public Object[][] orderDeadlineDataProvider() {
        return new Object[][]{
                {null, expectCreated},
                {OffsetDateTime.now().plusDays(randomNumber(2, 100)), expectCreated},
                {OffsetDateTime.now().plusDays(1), expectCreated},
                {OffsetDateTime.now(), expectBadRequest},
                {OffsetDateTime.now().minusDays(randomNumber(2, 100)), expectBadRequest},
        };
    }
}
