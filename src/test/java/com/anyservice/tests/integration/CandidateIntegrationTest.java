package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.services.CandidateUtilityService;
import com.anyservice.core.services.OrderUtilityService;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.order.Candidate;
import com.anyservice.tests.api.ICRUDTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;

public class CandidateIntegrationTest extends TestConfig implements ICRUDTest<Candidate, Candidate> {

    @Autowired
    private CandidateUtilityService candidateUtilityService;

    @Autowired
    private OrderUtilityService orderUtilityService;

    @Override
    public String getUrl() {
        return "/candidate";
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return Candidate.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return Candidate.class;
    }

    @Override
    public void assertEqualsDetailed(Candidate actual, Candidate expected) {
        candidateUtilityService.assertCandidatesAreEqual(actual, expected);
    }

    @Override
    public void assertEqualsListBrief(List<Candidate> actualList, List<Candidate> expectedList) {
        actualList.sort(comparing(Candidate::getOrderId));
        expectedList.sort(comparing(Candidate::getOrderId));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            Candidate actual = actualList.get(element);
            Candidate expected = expectedList.get(element);

            assertEqualsDetailed(actual, expected);
        }
    }

    @Override
    public Candidate buildNewItem() {
        UUID orderUuid = orderUtilityService.createPublishedOrder(getMockMvc(), getObjectMapper(), getHeaders())
                .getId();
        return candidateUtilityService.generateCandidate(orderUuid);
    }

    @Test
    @Override
    public void createAndSelectTest() throws Exception {
        ICRUDTest.super.createAndSelectTest();
    }

    @Test
    @Override
    public void countTest() throws Exception {
        ICRUDTest.super.countTest();
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }
}
