package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.RandomValuesGenerator;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.order.Category;
import com.anyservice.tests.api.ICRUDTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;

public class CategoryIntegrationTest extends TestConfig implements ICRUDTest<Category, Category> {
    @Override
    public String getUrl() {
        return "/category";
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return Category.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return Category.class;
    }

    @Override
    public void assertEqualsDetailed(Category actual, Category expected) {

        if (expected == null) {
            Assert.assertNull(actual);
            return;
        }

        Assert.assertEquals(actual.getTitle(), expected.getTitle());
        Assert.assertEquals(actual.getDescription(), expected.getDescription());
        Assert.assertEquals(actual.getParentCategory(), expected.getParentCategory());
    }

    @Override
    public void assertEqualsListBrief(List<Category> actualList, List<Category> expectedList) {
        actualList.sort(comparing(Category::getTitle));
        expectedList.sort(comparing(Category::getTitle));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            Category actual = actualList.get(element);
            Category expected = expectedList.get(element);

            // Assert each category is equal
            assertEqualsDetailed(actual, expected);
        }
    }

    @Override
    public Category buildNewItem() {
        return Category.builder()
                .id(UUID.randomUUID().toString())
                .title(RandomValuesGenerator.randomString(1, 50))
                .description(RandomValuesGenerator.randomString(1, 200))
                .build();
    }


    @Test
    @Override
    public void createAndSelectTest() throws Exception {
        ICRUDTest.super.createAndSelectTest();
    }

    @Test
    @Override
    public void countTest() throws Exception {
        ICRUDTest.super.countTest();
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }

    /**
     * Test the proper functioning of tree structure in categories
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void categoryTreeTest() throws Exception {
        // Create parent category
        DetailedWrapper<Category> categoryWrapper = create();

        // Select parent category with id
        Category parentCategory = select(categoryWrapper.getId());

        // Build child category
        Category childCategory = buildNewItem();

        // Set parent category to child
        childCategory.setParentCategory(parentCategory);

        // Create child category
        DetailedWrapper<Category> childCategoryWrapper = create(childCategory);

        // Select create child category
        Category createdChildCategory = select(childCategoryWrapper.getId());

        // Assert they are equal
        assertEqualsDetailed(createdChildCategory, childCategory);
    }
}
