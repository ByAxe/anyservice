package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.services.CandidateUtilityService;
import com.anyservice.core.services.ChatUtilityService;
import com.anyservice.core.services.OrderUtilityService;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.Chat;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.tests.api.ICRUDTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;

public class ChatIntegrationTest extends TestConfig implements ICRUDTest<Chat, Chat> {

    @Autowired
    private OrderUtilityService orderUtilityService;

    @Autowired
    private CandidateUtilityService candidateUtilityService;

    @Autowired
    private ChatUtilityService chatUtilityService;

    @Override
    public String getUrl() {
        return "/chat";
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return Chat.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return Chat.class;
    }

    @Override
    public void assertEqualsDetailed(Chat actual, Chat expected) {

        if (expected == null) {
            Assert.assertNull(actual);
            return;
        }

        Assert.assertEquals(actual.getMessages(), expected.getMessages());
    }

    @Override
    public void assertEqualsListBrief(List<Chat> actualList, List<Chat> expectedList) {
        actualList.sort(comparing(c -> UUID.fromString(String.valueOf(c.getId()))));
        expectedList.sort(comparing(c -> UUID.fromString(String.valueOf(c.getId()))));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            Chat actual = actualList.get(element);
            Chat expected = expectedList.get(element);

            Assert.assertEquals(actual.getMessages(), expected.getMessages());
        }
    }

    @Override
    public Chat buildNewItem() {
        // Create order
        DetailedWrapper<OrderDetailed> orderWrapper = orderUtilityService
                .createPublishedOrder(getMockMvc(), getObjectMapper(), getHeaders());

        // Get order uuid
        UUID orderUuid = orderWrapper.getId();

        // Create candidate
        DetailedWrapper<Candidate> candidateWrapper = candidateUtilityService
                .createCandidate(orderUuid, getMockMvc(), getObjectMapper(), getHeaders());

        // Get customer uuid
        String customerId = String.valueOf(orderWrapper.getDetailed().getCustomer().getId());

        // Get candidate's user uuid
        String candidateId = String.valueOf(candidateWrapper.getDetailed().getUser().getId());

        return chatUtilityService.generateChat(customerId, candidateId);
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }
}
