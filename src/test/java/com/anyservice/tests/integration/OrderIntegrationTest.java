package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.DateUtils;
import com.anyservice.core.FileTestUtils;
import com.anyservice.core.enums.FileType;
import com.anyservice.core.enums.LegalStatus;
import com.anyservice.core.enums.OrderState;
import com.anyservice.core.services.CandidateUtilityService;
import com.anyservice.core.services.OrderUtilityService;
import com.anyservice.core.services.UserUtilityService;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.entity.firestore.User;
import com.anyservice.entity.order.OrderEntity;
import com.anyservice.entity.order.Response;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.repository.OrderRepository;
import com.anyservice.service.converters.api.CustomConversionService;
import com.anyservice.tests.api.ICRUDTest;
import com.anyservice.tests.dataproviders.OrderDataProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.anyservice.core.RandomValuesGenerator.randomEnum;
import static com.anyservice.core.RandomValuesGenerator.randomNumber;
import static com.anyservice.core.enums.OrderState.*;
import static java.util.Comparator.comparing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class OrderIntegrationTest extends TestConfig implements ICRUDTest<OrderBrief, OrderDetailed> {

    private FileTestUtils fileTestUtils;

    @Autowired
    private UserUtilityService userUtilityService;

    @Autowired
    private CustomConversionService conversionService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CandidateUtilityService candidateUtilityService;

    @Autowired
    private OrderUtilityService orderUtilityService;

    @Autowired
    private IFirestoreRepository firestoreRepository;

    @Value("${order.max_attachments}")
    private int maxAttachments;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String usersCollection;

    @Override
    public String getUrl() {
        return "/order";
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return OrderBrief.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return OrderDetailed.class;
    }

    @Override
    public void assertEqualsDetailed(OrderDetailed actual, OrderDetailed expected) {
        orderUtilityService.assertOrdersAreEqual(actual, expected);
    }

    @Override
    public void assertEqualsListBrief(List<OrderBrief> actualList, List<OrderBrief> expectedList) {
        actualList.sort(comparing(OrderBrief::getHeadline));
        expectedList.sort(comparing(OrderBrief::getHeadline));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            OrderBrief actual = actualList.get(element);
            OrderBrief expected = expectedList.get(element);

            Assert.assertEquals(actual.getDeadline(), expected.getDeadline());
            Assert.assertEquals(actual.getDescription(), expected.getDescription());
            Assert.assertEquals(actual.getHeadline(), expected.getHeadline());
            Assert.assertEquals(actual.getLocation(), expected.getLocation());
            Assert.assertEquals(actual.getPrice(), expected.getPrice());
            Assert.assertEquals(actual.getState(), expected.getState());
            Assert.assertEquals(actual.getResponses(), expected.getResponses());
            Assert.assertEquals(actual.getMainAttachment(), expected.getMainAttachment());
        }
    }

    /**
     * Building of an item for an update operation
     *
     * @param orderId identifier of a future item
     * @return built order for  an update
     */
    @Override
    public OrderDetailed buildUpdateItem(Object orderId) {
        // Call super method to get an order
        OrderDetailed order = ICRUDTest.super.buildUpdateItem(orderId);
        order.setId(orderId);

        // Generate random amount of responses
        int responses = randomNumber(0, 10);

        if (responses != 0) {
            List<Candidate> candidates = IntStream.range(0, responses)
                    .mapToObj(i -> candidateUtilityService.generateCandidate(UUID.fromString(String.valueOf(orderId))))
                    .collect(Collectors.toList());

            order.setCandidates(candidates);
            order.setResponses(responses);
        }

        return order;
    }

    @Override
    @SneakyThrows
    public OrderDetailed buildNewItem() {
        initializeFileTestUtils();

        return orderUtilityService.generateOrder(getMockMvc(), getObjectMapper(), getHeaders());
    }

    /**
     * Update state of an order via repository
     * Not safe operation because it may not pass other checks
     * in case of further updates of updated with this method order
     *
     * @param uuid     id of an order
     * @param newState new state to be set to an order
     * @throws Exception
     */
    private void unsafelyUpdateOrderState(UUID uuid, OrderState newState) throws Exception {

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(uuid);

        // Set new state to it
        selectedOrder.setState(newState);

        // Convert to entity
        @NonNull OrderEntity orderEntity = conversionService.convert(selectedOrder, OrderEntity.class);

        // Save directly via repository
        orderRepository.saveAndFlush(orderEntity);
    }

    @Synchronized
    private void initializeFileTestUtils() {
        if (fileTestUtils == null) {
            fileTestUtils = new FileTestUtils(getMockMvc(), getObjectMapper(), getHeaders());
        }
    }

    @Test
    @Override
    public void createAndSelectTest() throws Exception {
        ICRUDTest.super.createAndSelectTest();
    }

    @Test
    @Override
    public void countTest() throws Exception {
        ICRUDTest.super.countTest();
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }

    @Test(dataProvider = "createWithStatusesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void createWithStatusesTest(OrderState state, ResultMatcher expect) throws Exception {
        OrderDetailed orderDetailed = buildNewItem();

        // Set state of an order
        orderDetailed.setState(state);

        create(orderDetailed, expect);
    }

    @Test(dataProvider = "createWithVariablyValidatedDataProvider", dataProviderClass = OrderDataProvider.class)
    public void createWithVariablyValidatedCustomer(boolean isVerified, OrderState state, ResultMatcher expect)
            throws Exception {

        // Build new order
        OrderDetailed order = buildNewItem();

        // Build customer
        UserBrief customer = userUtilityService.buildActivatedUser();

        // Set verification status of customer
        customer.setVerified(isVerified);

        @NonNull User customerEntity = conversionService.convert(customer, User.class);
        String key = customerEntity.getId();

        // Create customer
        firestoreRepository.saveSync(usersCollection, key, customerEntity);

        // Set state of an order
        order.setState(state);

        // Set new customer to order
        order.setCustomer(customer);

        // Create order and expect results
        create(order, expect);
    }

    @Test(dataProvider = "updateWithNonLegallyValidatedCandidatesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void updateWithNonLegallyValidatedCandidatesTest(boolean isVerifiedCandidate,
                                                            boolean isLegalStatusVerified,
                                                            ResultMatcher expectedStatus) throws Exception {

        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);

        // Get its uuid
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Build customer entity
        UserDetailed candidateUser = userUtilityService.buildActivatedUser();

        // Set required for the test combination of fields to
        candidateUser.setVerified(isVerifiedCandidate);
        candidateUser.setLegalStatusVerified(isLegalStatusVerified);
        candidateUser.setLegalStatus(randomEnum(LegalStatus.class));

        @NonNull User candidateEntity = conversionService.convert(candidateUser, User.class);
        String key = candidateEntity.getId();

        // Create candidate user
        firestoreRepository.saveSync(usersCollection, key, candidateEntity);

        // Set this user to candidate
        candidate.setUser(candidateUser);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "candidateSelectionDataProvider", dataProviderClass = OrderDataProvider.class)
    public void candidateSelectionTest(boolean candidateSelected, OrderState state,
                                       ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);

        // Get its uuid
        UUID orderUuid = orderWrapper.getId();

        if (state == CANDIDATE_SELECTED || state == CANCELLED || state == BLOCKED) {
            unsafelyUpdateOrderState(orderUuid, PUBLISHED);
        } else if (state == COMPLETED || state == NOT_COMPLETED) {
            unsafelyUpdateOrderState(orderUuid, CANDIDATE_SELECTED);
        }

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Set candidate selected
        candidate.setSelected(candidateSelected);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Set the state to an order
        order.setState(state);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "orderUuidInCandidatesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void orderUuidInCandidatesTest(boolean theSameUuid, ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);

        // Get order uuid for candidate
        UUID orderUuid = theSameUuid ? orderWrapper.getId() : UUID.randomUUID();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderWrapper.getId());

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "multipleCandidatesSelectionDataProvider", dataProviderClass = OrderDataProvider.class)
    public void multipleCandidatesSelectionTest(int amountOfSelectedCandidates, ResultMatcher expectedStatus)
            throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        if (amountOfSelectedCandidates != 0) {
            List<Candidate> candidates = IntStream.range(0, amountOfSelectedCandidates)
                    .mapToObj(i -> candidateUtilityService.generateCandidate(orderUuid))
                    .peek(c -> c.setSelected(true))
                    .collect(Collectors.toList());

            order.setCandidates(candidates);
            order.setResponses(amountOfSelectedCandidates);
        }

        // Set appropriate state
        order.setState(CANDIDATE_SELECTED);

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "updateSelectedCandidateDataProvider", dataProviderClass = OrderDataProvider.class)
    public void updateSelectedCandidateTest(boolean theSameCandidate, ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);

        // Get order uuid for candidate
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Set the second candidate as selected
        candidate.setSelected(true);

        Candidate secondCandidate = candidateUtilityService.generateCandidate(orderUuid);

        // Build candidates list
        List<Candidate> candidatesList = Arrays.asList(candidate, secondCandidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderWrapper.getId());

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Set appropriate state
        order.setState(CANDIDATE_SELECTED);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);

        // Select created order to get a version for update
        selectedOrder = select(orderWrapper.getId());

        // Extract version
        version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        List<Candidate> selectedOrderCandidates = selectedOrder.getCandidates();

        Assert.assertEquals(selectedOrderCandidates.size(), candidatesList.size());

        String candidateId = candidate.getId().toString();

        Candidate candidateDto = selectedOrderCandidates
                .stream()
                .filter(c -> candidateId.equals(c.getId()))
                .findFirst()
                .get();

        String secondCandidateId = secondCandidate.getId().toString();

        Candidate secondCandidateDto = selectedOrderCandidates
                .stream()
                .filter(c -> secondCandidateId.equals(c.getId()))
                .findFirst()
                .get();


        if (!theSameCandidate) {
            candidateDto.setSelected(false);
            secondCandidateDto.setSelected(true);
        }

        selectedOrder.setCandidates(Arrays.asList(candidateDto, secondCandidateDto));

        // Update order again
        update(selectedOrder, orderUuid, version, expectedStatus);
    }


    @Test(dataProvider = "amountOfResponsesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void amountOfResponsesTest(boolean setEqualAmountOfResponses, ResultMatcher expectedStatus)
            throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        int amountOfCandidates = randomNumber(1, 10);

        if (amountOfCandidates != 0) {
            List<Candidate> candidates = IntStream.range(0, amountOfCandidates)
                    .mapToObj(i -> candidateUtilityService.generateCandidate(orderUuid))
                    .collect(Collectors.toList());

            order.setCandidates(candidates);

            // Set amount of responses
            int responses = setEqualAmountOfResponses ? amountOfCandidates : amountOfCandidates - 1;

            order.setResponses(responses);
        }

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "customerAndCandidateTheSamePersonDataProvider", dataProviderClass = OrderDataProvider.class)
    public void customerAndCandidateTheSamePersonTest(boolean customerAndCandidateTheSamePerson,
                                                      ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Set customer the same as candidate
        if (customerAndCandidateTheSamePerson) {
            order.setCustomer(candidate.getUser());
        }

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }


    @Test(dataProvider = "updateOrderWithoutCustomerDataProvider", dataProviderClass = OrderDataProvider.class)
    public void updateOrderWithoutCustomerTest(boolean isCustomerPresent,
                                               ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order without customer
        if (!isCustomerPresent) order.setCustomer(null);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "setCandidateSelectedWithoutCandidatesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void setCandidateSelectedWithoutCandidatesTest(boolean isCandidatesPresent,
                                                          ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);
        candidate.setSelected(true);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Set candidate to an order
        order.setCandidates(candidatesList);
        order.setResponses(candidatesList.size());

        // Set candidate selected
        order.setState(CANDIDATE_SELECTED);

        // Set candidates
        if (!isCandidatesPresent) {
            order.setCandidates(null);
        }

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "amountOfAttachmentsDataProvider", dataProviderClass = OrderDataProvider.class)
    public void amountOfAttachmentsTest(boolean moreThanMaxAttachments, ResultMatcher expectedStatus)
            throws Exception {

        // Create order
        DetailedWrapper<OrderDetailed> orderWrapper = create();

        // Get its id
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed order = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(order.getDtUpdate());

        // Choose amount of attachments
        int amountOfAttachments = moreThanMaxAttachments
                ? maxAttachments + 1
                : randomNumber(1, maxAttachments);

        List<FileDetailed> attachments = new ArrayList<>();

        // Fill the attachments list
        for (int i = 0; i < amountOfAttachments; i++) {
            attachments.add(fileTestUtils.createAttachment().y());
        }

        // Generate main attachment
        FileDetailed mainAttachment = fileTestUtils.createMainAttachment().y();

        // Set attachments to updating order
        order.setMainAttachment(mainAttachment);
        order.setAttachments(attachments);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "attachmentsOfDifferentTypesDataProvider", dataProviderClass = OrderDataProvider.class)
    public void attachmentsOfDifferentTypesTest(FileType fileType, ResultMatcher expectedStatus) throws Exception {

        // Create order
        DetailedWrapper<OrderDetailed> orderWrapper = create();

        // Get its id
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed order = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(order.getDtUpdate());

        // Get random amount of attachments
        int amountOfAttachments = randomNumber(1, maxAttachments);

        List<FileDetailed> attachments = new ArrayList<>();

        // Fill the attachments list
        for (int i = 0; i < amountOfAttachments; i++) {
            attachments.add(fileTestUtils.createFile(fileType).y());
        }

        // Randomly choose the main attachment
        FileDetailed mainAttachment = fileTestUtils.createMainAttachment().y();

        // Set attachments to updating order
        order.setMainAttachment(mainAttachment);
        order.setAttachments(attachments);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "withoutSomeAttachmentDataProvider", dataProviderClass = OrderDataProvider.class)
    public void withoutSomeAttachmentTest(boolean withMainAttachment, boolean withAttachments,
                                          ResultMatcher expectedStatus) throws Exception {
        // Create order
        DetailedWrapper<OrderDetailed> orderWrapper = create();

        // Get its id
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed order = select(orderUuid);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(order.getDtUpdate());

        // Get random amount of attachments
        int amountOfAttachments = randomNumber(1, maxAttachments);

        List<FileDetailed> attachments = new ArrayList<>();

        // Fill the attachments list
        for (int i = 0; i < amountOfAttachments; i++) {
            attachments.add(fileTestUtils.createAttachment().y());
        }

        // Randomly choose the main attachment
        FileDetailed mainAttachment = fileTestUtils.createMainAttachment().y();

        // Set main attachment
        if (withMainAttachment) order.setMainAttachment(mainAttachment);
        else order.setMainAttachment(null);

        // Set attachments
        if (withAttachments) order.setAttachments(attachments);
        else order.setAttachments(null);

        // Update order and expect given status
        update(order, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "createDraftDataProvider", dataProviderClass = OrderDataProvider.class)
    public void createDraftTest(String headline, String description,
                                ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Make it draft and set required fields
        order.setState(DRAFT);
        order.setHeadline(headline);
        order.setDescription(description);

        // Create draft and expect given status
        create(order, expectedStatus);
    }

    @Test(dataProvider = "updateDraftDataProvider", dataProviderClass = OrderDataProvider.class)
    public void updateDraftTest(String headline, String description,
                                ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Make it draft and set required fields
        order.setState(DRAFT);

        // Create draft and expect given status
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Set required fields
        selectedOrder.setHeadline(headline);
        selectedOrder.setDescription(description);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(selectedOrder, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "createOrderAndUpdateItsStateToPublishedDataProvider",
            dataProviderClass = OrderDataProvider.class)
    public void createOrderAndUpdateItsStateToPublishedTest(OrderState basicState,
                                                            ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        order.setState(basicState);

        // Create draft and expect given status
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Update its state to
        selectedOrder.setState(PUBLISHED);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(selectedOrder, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "createPublishedDataProvider", dataProviderClass = OrderDataProvider.class)
    public void createPublishedTest(String headline, String phone, BigDecimal price,
                                    ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Set necessary fields
        order.setHeadline(headline);
        order.setPhone(phone);
        order.setPrice(price);

        // Create order and expect given status
        create(order, expectedStatus);
    }

    @Test(dataProvider = "updatePublishedDataProvider", dataProviderClass = OrderDataProvider.class)
    public void updatePublishedTest(String headline, String phone, BigDecimal price,
                                    ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Set these values to updating order
        selectedOrder.setHeadline(headline);
        selectedOrder.setPhone(phone);
        selectedOrder.setPrice(price);

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(selectedOrder, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "candidateResponseDataProvider", dataProviderClass = OrderDataProvider.class)
    public void candidateResponseTest(Response response, ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Create valid order
        DetailedWrapper<OrderDetailed> orderWrapper = create(order);
        UUID orderUuid = orderWrapper.getId();

        // Generate valid candidate
        Candidate candidate = candidateUtilityService.generateCandidate(orderUuid);
        candidate.setResponse(response);

        // Build candidates list
        List<Candidate> candidatesList = Collections.singletonList(candidate);

        // Select created order to get a version for update
        OrderDetailed selectedOrder = select(orderUuid);

        // Set candidate to an order
        selectedOrder.setCandidates(candidatesList);
        selectedOrder.setResponses(candidatesList.size());

        // Extract version
        long version = DateUtils.convertOffsetDateTimeToMills(selectedOrder.getDtUpdate());

        // Update order and expect given status
        update(selectedOrder, orderUuid, version, expectedStatus);
    }

    @Test(dataProvider = "orderDeadlineDataProvider", dataProviderClass = OrderDataProvider.class)
    public void orderDeadlineTest(OffsetDateTime deadline, ResultMatcher expectedStatus) throws Exception {
        // Build new order
        OrderDetailed order = buildNewItem();

        // Set necessary fields
        order.setDeadline(deadline);

        // Create order and expect given status
        create(order, expectedStatus);
    }

    @Test
    public void specHeadlineTest() throws Exception {
        String headline = "h";

        // Build an order
        OrderDetailed orderDetailed = buildNewItem();
        orderDetailed.setHeadline(headline);

        // Create order
        create(orderDetailed);

        // Select existing order
        String result = getMockMvc().perform(get(getExtendedUrl() + "?headline=" + headline)
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<OrderDetailed> orders = getObjectMapper().readValue(result,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(orders);

        Assert.assertEquals(orders.size(), 1);

        Assert.assertEquals(orders.get(0).getHeadline(), orderDetailed.getHeadline());

        // Select non existing order

        // Select existing order
        result = getMockMvc().perform(get(getExtendedUrl() + "?headline=" + "d")
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        orders = getObjectMapper().readValue(result,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(orders);

        Assert.assertEquals(orders.size(), 0);


        // Select without specification
        result = getMockMvc().perform(get(getExtendedUrl())
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        orders = getObjectMapper().readValue(result,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(orders);

        Assert.assertEquals(orders.size(), 1);

        Assert.assertEquals(orders.get(0).getHeadline(), orderDetailed.getHeadline());
    }


    @Test
    public void specOrderCustomerIdFilterTest() throws Exception {
        final int amountOfOrders = 10;

        // Create a few orders
        final List<OrderDetailed> orders = IntStream.range(0, amountOfOrders)
                .mapToObj(this::generateOrder)
                .filter(Objects::nonNull)
                .map(DetailedWrapper::getDetailed)
                .collect(Collectors.toList());

        final int randomlyChosenOrderNumber = randomNumber(0, amountOfOrders);
        final OrderDetailed randomlyChosenOrder = orders.get(randomlyChosenOrderNumber);
        final UserBrief randomlyChosenOrderCustomer = randomlyChosenOrder.getCustomer();
        final String id = String.valueOf(randomlyChosenOrderCustomer.getId());

        // Select existing order
        final String receivedOrdersAsString = getMockMvc().perform(get(getExtendedUrl() + "?customer=" + id)
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        final List<OrderDetailed> receivedOrders = getObjectMapper().readValue(receivedOrdersAsString,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(receivedOrders);

        Assert.assertEquals(receivedOrders.size(), 1);

        final OrderDetailed receivedOrder = receivedOrders.get(0);
        // Need to compare them as brief DTOs
        assertEqualsListBrief(Collections.singletonList(receivedOrder), Collections.singletonList(randomlyChosenOrder));
    }


    @Test
    public void specOrderStatesFilterTest() throws Exception {
        final OrderDetailed draft = buildNewItem();
        draft.setState(DRAFT);
        create(draft);

        final OrderDetailed published = create().getDetailed();

        final String existingStates = "?state=" + DRAFT + "&state=" + PUBLISHED;

        // Select existing orders
        final String existingOrdersAsString = getMockMvc().perform(get(getExtendedUrl() + existingStates)
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        final List<OrderDetailed> existingOrders = getObjectMapper().readValue(existingOrdersAsString,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(existingOrders);

        Assert.assertEquals(existingOrders.size(), 2);

        final String notExistingStates = Arrays.stream(values())
                .filter(s -> DRAFT != s && PUBLISHED != s)
                .map(Enum::toString)
                .collect(Collectors.joining("&state=", "?state=", ""));

        final String notExistingOrdersAsString = getMockMvc().perform(get(getExtendedUrl() + notExistingStates)
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        final List<OrderDetailed> notExistingOrders = getObjectMapper().readValue(notExistingOrdersAsString,
                getObjectMapper().getTypeFactory().constructCollectionType(List.class, getDetailedClass()));

        Assert.assertNotNull(notExistingOrders);

        Assert.assertTrue(notExistingOrders.isEmpty());
    }

    private DetailedWrapper<OrderDetailed> generateOrder(int i) {
        try {
            return create();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
