package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.enums.Compliment;
import com.anyservice.core.services.OrderUtilityService;
import com.anyservice.core.services.UserUtilityService;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.order.Candidate;
import com.anyservice.dto.order.OrderBrief;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.dto.order.Review;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.tests.api.ICRUDTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.anyservice.core.RandomValuesGenerator.*;
import static java.util.Comparator.comparing;

public class ReviewIntegrationTest extends TestConfig implements ICRUDTest<Review, Review> {

    @Autowired
    private OrderUtilityService orderUtilityService;

    @Autowired
    protected UserUtilityService userUtilityService;

    @Override
    public String getUrl() {
        return "/review";
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return Review.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return Review.class;
    }

    @Override
    public void assertEqualsDetailed(Review actual, Review expected) {

        if (expected == null) {
            Assert.assertNull(actual);
            return;
        }

        Assert.assertEquals(actual.getBody(), expected.getBody());
        Assert.assertEquals(actual.getHeader(), expected.getHeader());
        Assert.assertEquals(actual.getRating(), expected.getRating());
        Assert.assertEquals(actual.getContractorResponse(), expected.getContractorResponse());
        Assert.assertEquals(actual.getCompliments(), expected.getCompliments());
        userUtilityService.assertUsersAreEqual(actual.getAuthor(), expected.getAuthor());
        userUtilityService.assertUsersAreEqual(actual.getContractor(), expected.getContractor());
        Assert.assertEquals(actual.getOrder(), expected.getOrder());
    }

    @Override
    public void assertEqualsListBrief(List<Review> actualList, List<Review> expectedList) {
        actualList.sort(comparing(Review::getHeader));
        expectedList.sort(comparing(Review::getHeader));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            Review actual = actualList.get(element);
            Review expected = expectedList.get(element);

            Assert.assertEquals(actual.getBody(), expected.getBody());
            Assert.assertEquals(actual.getHeader(), expected.getHeader());
            Assert.assertEquals(actual.getRating(), expected.getRating());
            Assert.assertEquals(actual.getRating(), expected.getRating());
            Assert.assertEquals(actual.getContractorResponse(), expected.getContractorResponse());
            Assert.assertEquals(actual.getCompliments(), expected.getCompliments());
            Assert.assertEquals(actual.getAuthor(), expected.getAuthor());
            Assert.assertEquals(actual.getContractor(), expected.getContractor());
            Assert.assertEquals(actual.getOrder(), expected.getOrder());
        }
    }

    @Override
    public Review buildUpdateItem(Object uuid) {

        // Call super method to build a review
        Review review = ICRUDTest.super.buildUpdateItem(uuid);

        review.setContractorResponse(randomString(1, 50));

        return review;
    }

    @Override
    public Review buildNewItem() {
        Review review = Review.builder()
                .body(randomString(1, 100))
                .header(randomString(1, 50))
                .rating(randomRating())
                .build();

        // Create completed order
        OrderDetailed completedOrder = orderUtilityService
                .createCompletedOrder(getMockMvc(), getObjectMapper(), getHeaders());

        // Set order
        review.setOrder(new OrderBrief(completedOrder));

        // Set author
        UserBrief author = completedOrder.getCustomer();
        review.setAuthor(author);

        // Set contractor
        UserBrief contractor = completedOrder.getCandidates()
                .stream()
                .filter(Candidate::isSelected)
                .findFirst()
                .get()
                .getUser();

        review.setContractor(contractor);

        // Set random compliments
        int amount = Compliment.values().length;
        int randomAmountOfCompliments = randomNumber(0, amount);

        if (randomAmountOfCompliments != 0) {

            Set<Compliment> compliments = IntStream.range(0, randomAmountOfCompliments)
                    .mapToObj(i -> randomEnum(Compliment.class))
                    .collect(Collectors.toSet());

            review.setCompliments(compliments);
        }

        return review;
    }

    @Test
    @Override
    public void createAndSelectTest() throws Exception {
        ICRUDTest.super.createAndSelectTest();
    }

    @Test
    @Override
    public void countTest() throws Exception {
        ICRUDTest.super.countTest();
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }
}
