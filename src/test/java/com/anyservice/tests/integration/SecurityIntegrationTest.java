package com.anyservice.tests.integration;

import com.anyservice.core.VerificationCode;
import com.anyservice.core.enums.UserRole;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.repository.IFirestoreRepository;
import com.anyservice.web.security.dto.Login;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.testng.annotations.Test;

import java.util.UUID;

import static com.anyservice.core.RandomValuesGenerator.randomString;
import static com.anyservice.core.TestingUtilityClass.PASSWORD_MAX_LENGTH;
import static com.anyservice.core.TestingUtilityClass.PASSWORD_MIN_LENGTH;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class SecurityIntegrationTest extends UserIntegrationTest {

    @Value("${security.jwt.header}")
    private String jwtHeader;

    @Value("${security.jwt.key-prefix}")
    private String jwtKeyPrefix;

    @Value("${spring.cloud.gcp.firestore.collection.cache}")
    private String cacheCollection;

    @Value("${spring.cloud.gcp.firestore.collection.users}")
    private String usersCollection;

    @Autowired
    private IFirestoreRepository firestoreRepository;

    /**
     * Create user, login and verify it
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void loginTest() throws Exception {
        // Create empty headers
        HttpHeaders headers = new HttpHeaders();

        // Access special method that is allowed only for authenticated users
        checkIfAuthenticated(headers, expectUnauthorized);

        // Generate password
        String password = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Build a user
        UserDetailed user = buildNewItem();
        user.setPassword(password);

        // Create user
        DetailedWrapper<UserDetailed> userDetailedDetailedWrapper = create(user, expectDefault);
        String id = userDetailedDetailedWrapper.getId();

        // Get user name of created user
        String username = user.getEmail();

        // Build login-specific object
        Login login = Login.builder()
                .password(password)
                .email(username)
                .build();

        // Convert it to string
        String loginAsString = getObjectMapper().writeValueAsString(login);

        // try to login
        String token = getMockMvc().perform(post(getExtendedUrl() + "/login")
                .headers(headers)
                .contentType(getContentType())
                .content(loginAsString))
                .andExpect(expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Check if it's not null
        Assert.assertNotNull(token);

        // Put token into headers
        headers.add(jwtHeader, jwtKeyPrefix + token);

        // TODO commented temporarily while verification is turned off
        // Verify created user
//        verifyUser(id);

        /*
         We CANNOT successfully pass authentication with received custom firebase token
         Because beforehand, this custom token must be converted to Firebase ID token
         And this conversion is not possible with just Firebase Admin SDK

         It can be performed automatically and 'behind the scenes' with any Firebase Client SDK's
         when calling .signInWithCustomToken(customToken) method

         https://firebase.google.com/docs/auth/admin/create-custom-tokens
        */
    }

    /**
     * Verify user with given identifier
     *
     * @param id user identifier
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private void verifyUser(String id) throws Exception {
        // Select user via identifier
        UserDetailed selectedUser = select(id);

        // Get expected verification code from a cache
        UUID expectedVerificationCode;
        try {
            VerificationCode verificationCode = firestoreRepository.findByKey(cacheCollection, id, VerificationCode.class);
            expectedVerificationCode = UUID.fromString(verificationCode.getVerificationCode());
        } catch (Exception e) {
            throw new RuntimeException("Retrieving from cache was unsuccessful", e);
        }

        // Assert it's present
        Assert.assertNotNull(expectedVerificationCode);

        // Verify user with this verification code
        UserDetailed actual = verifyUser(id, expectedVerificationCode, expectOk);

        // For expected and actual to be equal
        selectedUser.setVerified(true);

        // Assert that verified user is the one we needed to verify
        assertEqualsDetailed(actual, selectedUser);
    }

    /**
     * Verify user with given verification code
     *
     * @param id     user identifier
     * @param code   verification code
     * @param expect what response to expect
     * @return verified user
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private UserDetailed verifyUser(String id, UUID code, ResultMatcher expect) throws Exception {
        String contentAsString = getMockMvc().perform(
                get(getExtendedUrl() + "/verification/" + id + "/" + code)
                        .headers(getHeaders())
                        .contentType(getContentType()))
                .andExpect(expect)
                .andReturn()
                .getResponse()
                .getContentAsString();

        return getObjectMapper().readValue(contentAsString,
                getObjectMapper().getTypeFactory().constructType(getDetailedClass()));
    }

    /**
     * Access special method that is allowed only for authenticated users
     *
     * @param headers headers that may contain or not the valid token
     * @return special request object that can be parsed further if everthing was ok with query
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private ResultActions checkIfAuthenticated(HttpHeaders headers, ResultMatcher expect) throws Exception {
        ResultActions resultActions = getMockMvc()
                .perform(get(getExtendedUrl() + "/authenticated")
                        .headers(headers))
                .andExpect(expect);

        return resultActions;
    }

    /**
     * Access special method that is allowed only for authenticated users
     * <p>
     * Expect by default that query is ok and parse results into {@link UserRole}
     *
     * @param headers headers that may contain or not the valid token
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private UserRole checkIfAuthenticated(HttpHeaders headers) throws Exception {
        String userRoleAsString = checkIfAuthenticated(headers, expectOk)
                .andReturn()
                .getResponse()
                .getContentAsString();

        int ordinal = Integer.parseInt(userRoleAsString);

        return UserRole.values()[ordinal];
    }

    /**
     * Log out of application
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void logoutTest() throws Exception {
        getMockMvc().perform(get(getExtendedUrl() + "/logout")
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk);
    }

    /**
     * Try to refresh token without token in headers
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void refreshTokenWithoutUserTest() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();

        getMockMvc().perform(put(getExtendedUrl() + "/refresh")
                .headers(httpHeaders)
                .contentType(getContentType()))
                .andExpect(expectUnauthorized);
    }

    /**
     * Refresh token for existing user
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void refreshTokenTest() throws Exception {
        getMockMvc().perform(put(getExtendedUrl() + "/refresh")
                .headers(getHeaders())
                .contentType(getContentType()))
                .andExpect(expectOk);
    }
}
