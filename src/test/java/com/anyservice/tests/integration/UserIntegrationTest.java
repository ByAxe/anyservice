package com.anyservice.tests.integration;

import com.anyservice.config.TestConfig;
import com.anyservice.core.DateUtils;
import com.anyservice.core.FileTestUtils;
import com.anyservice.core.Tuple;
import com.anyservice.core.services.UserUtilityService;
import com.anyservice.dto.DetailedWrapper;
import com.anyservice.dto.api.APrimary;
import com.anyservice.dto.file.FileDetailed;
import com.anyservice.dto.user.UserBrief;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.dto.user.UserForChangePassword;
import com.anyservice.service.user.api.IUserService;
import com.anyservice.tests.api.ICRUDTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.anyservice.core.RandomValuesGenerator.randomString;
import static com.anyservice.core.TestingUtilityClass.PASSWORD_MAX_LENGTH;
import static com.anyservice.core.TestingUtilityClass.PASSWORD_MIN_LENGTH;
import static java.util.Comparator.comparing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;


public class UserIntegrationTest extends TestConfig implements ICRUDTest<UserBrief, UserDetailed> {

    protected FileTestUtils fileTestUtils;

    @Autowired
    private IUserService userService;

    @Autowired
    protected UserUtilityService userUtilityService;

    @Value("${user.validation.password.length.min}")
    private int passwordMinLength;

    @Value("${user.validation.password.length.max}")
    private int passwordMaxLength;


    /**
     * Data provider for {@link this#createAndDeleteUserWithDocumentsAndPortfolioTest(int, int)} test
     *
     * @return [documentsAmount, portfolioAmount]
     */
    @DataProvider
    public static Object[][] createAndDeleteUserWithDocumentsAndPortfolioDataProvider() {
        return new Object[][]{
                {5, 20},
                {10, 0},
                {0, 10},
                {0, 0},
                {20, 20}
        };
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public String getUrl() {
        return "/user";
    }

    @Override
    public MockMvc getMockMvc() {
        return mockMvc;
    }

    @Override
    public MediaType getContentType() {
        return contentType;
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public Class<? extends APrimary> getBriefClass() {
        return UserBrief.class;
    }

    @Override
    public Class<? extends APrimary> getDetailedClass() {
        return UserDetailed.class;
    }

    @Override
    public void assertEqualsDetailed(UserDetailed actual, UserDetailed expected) {
        Assert.assertEquals(actual.getName(), expected.getName());
        Assert.assertEquals(actual.getContacts(), expected.getContacts());
        Assert.assertEquals(actual.getEmail(), expected.getEmail());
        Assert.assertEquals(actual.getDescription(), expected.getDescription());
        Assert.assertEquals(actual.isLegalStatusVerified(), expected.isLegalStatusVerified());
        // TODO commented temporarily while verification is turned off
//        Assert.assertEquals(actual.isVerified(), expected.isVerified());
        Assert.assertEquals(actual.getLegalStatus(), expected.getLegalStatus());
    }

    @Override
    public void assertEqualsListBrief(List<UserBrief> actualList, List<UserBrief> expectedList) {
        actualList.sort(comparing(UserBrief::getEmail));
        expectedList.sort(comparing(UserBrief::getEmail));

        Assert.assertEquals(actualList.size(), expectedList.size());

        for (int element = 0; element < actualList.size(); element++) {
            UserBrief user = actualList.get(element);
            UserBrief otherUser = expectedList.get(element);

            Assert.assertEquals(user.getName(), otherUser.getName());
            Assert.assertEquals(user.getEmail(), otherUser.getEmail());
        }
    }


    @Test
    @Override
    public void createAndSelectTest() throws Exception {
        ICRUDTest.super.createAndSelectTest();
    }

    @Test
    @Override
    public void countTest() throws Exception {
        ICRUDTest.super.countTest();
    }

    @Test
    @Override
    public void deleteAndSelectByUUIDTest() throws Exception {
        ICRUDTest.super.deleteAndSelectByUUIDTest();
    }

    @Test
    @Override
    public void updateTest() throws Exception {
        ICRUDTest.super.updateTest();
    }

    @Test
    @Override
    public void createAndFindAllByIdListTest() throws Exception {
        ICRUDTest.super.createAndFindAllByIdListTest();
    }

    @Test
    @Override
    public void createAndCheckIfExists() throws Exception {
        ICRUDTest.super.createAndCheckIfExists();
    }

    /**
     * Create two users and then change user name of first on the userName of already existing user (second user)
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void userNameValidationTest_changeOnExistingUserName() throws Exception {
        String emailOfFirstUser = randomString(1, 50) + "@mail.com";
        String emailOfSecondUser = randomString(1, 50) + "@mail.com";

        // Build the first user
        UserDetailed firstUser = buildNewItem();
        firstUser.setEmail(emailOfFirstUser);

        // Create first user in DB
        DetailedWrapper<UserDetailed> firstUserWrapper = create(firstUser, expectDefault);

        // Get it id
        String idOfFirstUser = firstUserWrapper.getId();

        // Build the second user
        UserDetailed secondUser = buildNewItem();
        secondUser.setEmail(emailOfSecondUser);

        // Create second user in DB
        DetailedWrapper<UserDetailed> secondUserWrapper = create(secondUser, expectDefault);

        // Select the first user
        UserDetailed obtainedResult = select(idOfFirstUser);

        // Assert its id's are equal
        Assert.assertEquals(obtainedResult.getId(), idOfFirstUser);

        long version = DateUtils.convertOffsetDateTimeToMills(obtainedResult.getDtUpdate());

        // Try to change userName of firstUser, on the userName of the secondUser
        obtainedResult.setEmail(emailOfSecondUser);
        obtainedResult.setPassword(randomString(passwordMinLength, passwordMaxLength));

        // Make sure we get an exception in the end
        update(obtainedResult, idOfFirstUser, version, expectBadRequest);
    }

    /**
     * Removing non-existing user
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void deleteNotExistingUser() throws Exception {
        String id = UUID.randomUUID().toString();
        long version = new Date().getTime();

        // Wait for fail when removing non-existing user
        remove(id, version, expectBadRequest);
    }

    /**
     * Removing user with wrong version and expecting fail
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void deleteUserWithWrongVersion() throws Exception {
        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create();
        String id = userWrapper.getId();

        // Set wrong version
        long version = new Date().getTime() + 100_000;

        // Make delete request and expect exception
        remove(id, version, expectBadRequest);
    }

    /**
     * Create user then change its password and make sure it's actually changed
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void changePasswordTest() throws Exception {
        // Generate password
        String password = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Build a user
        UserDetailed user = buildNewItem();
        user.setPassword(password);

        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create(user, expectDefault);

        // Get id of created user
        String id = userWrapper.getId();

        // Generate a new password for the user
        String newPassword = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Change password for the first time
        UserForChangePassword userForChangePassword = changePassword(id, password, newPassword, expectOk);

        // Try to change it again, and expect error because password has already changed
        changePassword(userForChangePassword, expectBadRequest);

        // Change password one more time, but this time expect that password already changed,
        // so that previous "newPassword" becomes "oldPassword" for this operation
        password = newPassword;

        // Generate a new password for the second change
        newPassword = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Change password the second time but expect success now, because everything is correct
        changePassword(id, password, newPassword, expectOk);
    }

    /**
     * Change password operation wrapper
     * Builds {@link UserForChangePassword} from given data
     *
     * @param id            identifier of a user
     * @param password      old password
     * @param newPassword   new password
     * @param resultMatcher result to expect after execution of method
     * @return {@link UserForChangePassword} that was used for change password operation
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private UserForChangePassword changePassword(String id, String password, String newPassword,
                                                 ResultMatcher resultMatcher) throws Exception {
        // Build a special object, required for the second password change operation
        UserForChangePassword userForChangePassword = UserForChangePassword.builder()
                .id(id)
                .oldPassword(password)
                .newPassword(newPassword)
                .build();

        // Change password
        changePassword(userForChangePassword, resultMatcher);

        return userForChangePassword;
    }

    /**
     * Change password operation wrapper
     * Converts {@link UserForChangePassword} to string and performs change password operation
     *
     * @param userForChangePassword {@link UserForChangePassword} that is used for change password operation
     * @param resultMatcher         result to expect after execution of method
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    private void changePassword(UserForChangePassword userForChangePassword,
                                ResultMatcher resultMatcher) throws Exception {
        String valueAsString = getObjectMapper().writeValueAsString(userForChangePassword);

        getMockMvc().perform(put(getExtendedUrl() + "/change/password")
                .headers(getHeaders())
                .contentType(getContentType())
                .content(valueAsString))
                .andExpect(resultMatcher);
    }

    /**
     * Create user to ensure he does not have a password
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void getUserWithoutPassword() throws Exception {
        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create();

        // Find user by id
        UserDetailed selectedUser = select(userWrapper.getId());

        // make sure password is null
        Assert.assertNull(selectedUser.getPassword());
    }

    /**
     * Create&Update user and make sure its password wasn't changed unintentionally, since creation
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void updateUserButPasswordMustNotChange() throws Exception {
        // Generate password
        String password = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Build a new user
        UserDetailed user = buildNewItem();
        user.setPassword(password);

        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create(user, expectDefault);

        // Get id of created user
        String id = userWrapper.getId();

        // Select created user
        UserDetailed createdUser = select(id);

        // Get its latest "version"
        long version = DateUtils.convertOffsetDateTimeToMills(createdUser.getDtUpdate());

        // Update user
        update(id, version);

        /* If changing operation will be successful - password wasn't changed, since creation */

        // Generate a new password for the user
        String newPassword = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Change password, using our first password
        changePassword(id, password, newPassword, expectOk);
    }

    @Override
    public UserDetailed buildNewItem() {
        return userUtilityService.buildUserDetailed();
    }

    /**
     * Create user and  find it via its userName
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void findUserForLoginTest() throws Exception {
        // Generate password
        String password = randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH);

        // Build a new user
        UserDetailed user = buildNewItem();
        user.setPassword(password);

        // Get user name of a user
        String userName = user.getEmail();

        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create(user, expectDefault);

        // Get id
        String id = userWrapper.getId();

        // Select created user
        UserDetailed expectedUser = select(id);

        // Try to get user via given criteria
        UserDetailed actualUser = userService.findUserForLogin(userName, password);

        // If we got here, search was successful
        assertEqualsDetailed(actualUser, expectedUser);
    }

    /**
     * Create user with profile photo and delete it when made sure that created one are equal to expected,
     * as well as created profile photo
     *
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test
    public void createAndDeleteUserWithProfilePhoto() throws Exception {
        // Create test utils needed for this test
        initializeFileTestUtils();

        // Prepare user
        UserDetailed userDetailed = buildNewItem();

        // Create profile photo
        Tuple<MockMultipartFile, FileDetailed> photoTuple = fileTestUtils.createProfilePhoto();
        MockMultipartFile multipartFile = photoTuple.x();
        FileDetailed fileDetailed = photoTuple.y();

        // Add profile photo
        userDetailed.setProfilePhoto(fileDetailed);

        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create(userDetailed);
        String userId = userWrapper.getId();

        // Select created user
        UserDetailed selectedUser = select(userId);

        // Assert users are equal
        assertEqualsDetailed(selectedUser, userDetailed);

        // Get selected user's profile photo
        FileDetailed selectedUserProfilePhoto = selectedUser.getProfilePhoto();

        // Compare created file metadata with the source one
        Assert.assertEquals(selectedUserProfilePhoto.getId(), fileDetailed.getId());
        Assert.assertEquals(selectedUserProfilePhoto.getName(), multipartFile.getOriginalFilename());

        // Get version from selected user
        long version = DateUtils.convertOffsetDateTimeToMills(selectedUser.getDtCreate());

        // Remove user and file
        remove(userId, version);
    }

    /**
     * Create user with given amount of documents and portfolio elements and expect everything was created as expected.
     * Then, delete all the created documents
     *
     * @param documentsAmount amount of documents for user
     * @param portfolioAmount amount of portfolio elements for user
     * @throws Exception if something goes wrong - let interpret it as failed test
     */
    @Test(dataProvider = "createAndDeleteUserWithDocumentsAndPortfolioDataProvider")
    public void createAndDeleteUserWithDocumentsAndPortfolioTest(int documentsAmount, int portfolioAmount)
            throws Exception {
        // Create test utils needed for this test
        initializeFileTestUtils();

        // Prepare user
        UserDetailed userDetailed = buildNewItem();

        List<Tuple<MockMultipartFile, FileDetailed>> documents = null;
        List<Tuple<MockMultipartFile, FileDetailed>> portfolio = null;

        // Create bunch of documents and set it to user
        if (documentsAmount != 0) {
            documents = fileTestUtils.createDocuments(documentsAmount);
            List<FileDetailed> documentsList = fileTestUtils.extractFiles(documents);
            userDetailed.setDocuments(documentsList);
        }

        if (portfolioAmount != 0) {
            portfolio = fileTestUtils.createPortfolio(portfolioAmount);
            List<FileDetailed> portfolioList = fileTestUtils.extractFiles(portfolio);
            userDetailed.setPortfolio(portfolioList);
        }

        // Create user
        DetailedWrapper<UserDetailed> userWrapper = create(userDetailed);
        String userId = userWrapper.getId();

        // Select created user
        UserDetailed selectedUser = select(userId);

        // Assert users are equal
        assertEqualsDetailed(selectedUser, userDetailed);

        // Get selected user's documents and portfolio
        List<FileDetailed> selectedUserDocuments = selectedUser.getDocuments();
        List<FileDetailed> selectedUserPortfolio = selectedUser.getPortfolio();

        // Assert these lists are not null and not empty
        if (documentsAmount != 0) {
            Assert.assertNotNull(selectedUserDocuments);

            if (selectedUserDocuments.isEmpty())
                throw new AssertionError("Selected user documents list should not be empty!");

            // Assert lists of files are equal
            fileTestUtils.assertFilesDTOFromListsAreEqual(selectedUserDocuments, documents);

        }

        // Assert these lists are not null and not empty
        if (portfolioAmount != 0) {
            Assert.assertNotNull(selectedUserPortfolio);

            if (selectedUserPortfolio.isEmpty())
                throw new AssertionError("Selected user portfolio list should not be empty!");

            // Assert lists of files are equal
            fileTestUtils.assertFilesDTOFromListsAreEqual(selectedUserPortfolio, portfolio);
        }

        // Get version from selected user
        long version = DateUtils.convertOffsetDateTimeToMills(selectedUser.getDtCreate());

        // Remove user and all its files
        remove(userId, version);
    }

    @Synchronized
    private void initializeFileTestUtils() {
        if (fileTestUtils == null) {
            fileTestUtils = new FileTestUtils(getMockMvc(), getObjectMapper(), getHeaders());
        }
    }
}
