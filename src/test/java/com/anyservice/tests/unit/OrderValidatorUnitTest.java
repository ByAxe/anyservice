package com.anyservice.tests.unit;

import com.anyservice.config.TestConfig;
import com.anyservice.core.enums.OrderState;
import com.anyservice.dto.order.OrderDetailed;
import com.anyservice.service.validators.api.IOrderValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

import static com.anyservice.core.TestingUtilityClass.FAIL;
import static com.anyservice.core.TestingUtilityClass.SUCCESS;
import static com.anyservice.core.enums.OrderState.*;

public class OrderValidatorUnitTest extends TestConfig {

    @Autowired
    private IOrderValidator orderValidator;

    @DataProvider
    public static Object[][] orderStateChangeDataProvider() {
        return new Object[][]{
                {null, DRAFT, DRAFT, SUCCESS},
                {null, DRAFT, PUBLISHED, SUCCESS},
                {null, DRAFT, REMOVED, SUCCESS},
                {null, DRAFT, EDITED, FAIL},
                {null, DRAFT, CANDIDATE_SELECTED, FAIL},
                {null, DRAFT, CANCELLED, FAIL},
                {null, DRAFT, BLOCKED, FAIL},
                {null, DRAFT, COMPLETED, FAIL},
                {null, DRAFT, NOT_COMPLETED, FAIL},

                {null, PUBLISHED, DRAFT, FAIL},
                {null, PUBLISHED, PUBLISHED, SUCCESS},
                {null, PUBLISHED, REMOVED, FAIL},
                {null, PUBLISHED, EDITED, SUCCESS},
                {null, PUBLISHED, CANDIDATE_SELECTED, SUCCESS},
                {null, PUBLISHED, CANCELLED, SUCCESS},
                {null, PUBLISHED, BLOCKED, SUCCESS},
                {null, PUBLISHED, COMPLETED, FAIL},
                {null, PUBLISHED, NOT_COMPLETED, FAIL},

                {null, EDITED, DRAFT, FAIL},
                {null, EDITED, PUBLISHED, FAIL},
                {null, EDITED, REMOVED, FAIL},
                {null, EDITED, EDITED, SUCCESS},
                {null, EDITED, CANDIDATE_SELECTED, SUCCESS},
                {null, EDITED, CANCELLED, SUCCESS},
                {null, EDITED, BLOCKED, SUCCESS},
                {null, EDITED, COMPLETED, FAIL},
                {null, EDITED, NOT_COMPLETED, FAIL},

                {null, CANDIDATE_SELECTED, DRAFT, FAIL},
                {null, CANDIDATE_SELECTED, PUBLISHED, FAIL},
                {null, CANDIDATE_SELECTED, REMOVED, FAIL},
                {null, CANDIDATE_SELECTED, EDITED, FAIL},
                {null, CANDIDATE_SELECTED, CANDIDATE_SELECTED, SUCCESS},
                {null, CANDIDATE_SELECTED, CANCELLED, FAIL},
                {null, CANDIDATE_SELECTED, BLOCKED, FAIL},
                {null, CANDIDATE_SELECTED, COMPLETED, SUCCESS},
                {null, CANDIDATE_SELECTED, NOT_COMPLETED, SUCCESS},

                {null, CANCELLED, DRAFT, FAIL},
                {null, CANCELLED, PUBLISHED, SUCCESS},
                {null, CANCELLED, REMOVED, FAIL},
                {null, CANCELLED, EDITED, FAIL},
                {null, CANCELLED, CANDIDATE_SELECTED, FAIL},
                {null, CANCELLED, CANCELLED, SUCCESS},
                {null, CANCELLED, BLOCKED, FAIL},
                {null, CANCELLED, COMPLETED, FAIL},
                {null, CANCELLED, NOT_COMPLETED, FAIL},

                {null, BLOCKED, DRAFT, SUCCESS},
                {null, BLOCKED, PUBLISHED, FAIL},
                {null, BLOCKED, REMOVED, FAIL},
                {null, BLOCKED, EDITED, FAIL},
                {null, BLOCKED, CANDIDATE_SELECTED, FAIL},
                {null, BLOCKED, CANCELLED, FAIL},
                {null, BLOCKED, BLOCKED, SUCCESS},
                {null, BLOCKED, COMPLETED, FAIL},
                {null, BLOCKED, NOT_COMPLETED, FAIL},

                {null, REMOVED, DRAFT, SUCCESS},
                {null, REMOVED, PUBLISHED, FAIL},
                {null, REMOVED, REMOVED, SUCCESS},
                {null, REMOVED, EDITED, FAIL},
                {null, REMOVED, CANDIDATE_SELECTED, FAIL},
                {null, REMOVED, CANCELLED, FAIL},
                {null, REMOVED, BLOCKED, FAIL},
                {null, REMOVED, COMPLETED, FAIL},
                {null, REMOVED, NOT_COMPLETED, FAIL},

                {null, COMPLETED, DRAFT, FAIL},
                {null, COMPLETED, PUBLISHED, FAIL},
                {null, COMPLETED, REMOVED, FAIL},
                {null, COMPLETED, EDITED, FAIL},
                {null, COMPLETED, CANDIDATE_SELECTED, FAIL},
                {null, COMPLETED, CANCELLED, FAIL},
                {null, COMPLETED, BLOCKED, FAIL},
                {null, COMPLETED, COMPLETED, SUCCESS},
                {null, COMPLETED, NOT_COMPLETED, FAIL},

                {null, NOT_COMPLETED, DRAFT, FAIL},
                {null, NOT_COMPLETED, PUBLISHED, FAIL},
                {null, NOT_COMPLETED, REMOVED, FAIL},
                {null, NOT_COMPLETED, EDITED, FAIL},
                {null, NOT_COMPLETED, CANDIDATE_SELECTED, FAIL},
                {null, NOT_COMPLETED, CANCELLED, FAIL},
                {null, NOT_COMPLETED, BLOCKED, FAIL},
                {null, NOT_COMPLETED, COMPLETED, FAIL},
                {null, NOT_COMPLETED, NOT_COMPLETED, SUCCESS},
        };
    }

    @Test(dataProvider = "orderStateChangeDataProvider")
    public void orderStateChangeTest(OrderDetailed order, OrderState oldState, OrderState newState,
                                     boolean expected) {
        Map<String, Object> errors = orderValidator.validateStateChange(order, oldState, newState);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }
}
