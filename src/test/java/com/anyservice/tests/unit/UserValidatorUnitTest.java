package com.anyservice.tests.unit;

import com.anyservice.config.TestConfig;
import com.anyservice.core.enums.LegalStatus;
import com.anyservice.dto.user.UserDetailed;
import com.anyservice.service.user.api.IPasswordService;
import com.anyservice.service.validators.api.IUserValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.UUID;

import static com.anyservice.core.RandomValuesGenerator.*;
import static com.anyservice.core.TestingUtilityClass.*;
import static org.apache.commons.lang3.RandomStringUtils.random;

public class UserValidatorUnitTest extends TestConfig {

    @Autowired
    private IUserValidator userValidator;

    @Autowired
    private IPasswordService passwordService;

    /**
     * Fist parameter - User
     * Second parameter - Should be successful or not
     *
     * @return test dataset
     */
    @DataProvider
    public static Object[][] createValidationDataProvider() {
        String email = "byaxe@ya.ru";
        boolean isVerified = randomBoolean();
        boolean isLegalStatusVerified = isVerified;
        LegalStatus legalStatus = isLegalStatusVerified ? randomEnum(LegalStatus.class) : null;

        return new Object[][]{
                // Empty user - wait for FAIL
                {UserDetailed.builder().build(), FAIL},

                // "email" is null - wait for FAIL
                {UserDetailed.builder()
                        .name(random(randomNumber(2, 100), true, false))
                        .contacts(Map.of("phone", random(randomNumber(6, 50), false, true)))
                        .email(null)
                        .description(randomString(0, 1000))
                        .isVerified(isVerified)
                        .isLegalStatusVerified(isLegalStatusVerified)
                        .legalStatus(legalStatus)
                        .password(random(randomNumber(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)))
                        .build(), FAIL},


                // Everything is great - wait for SUCCESS
                {UserDetailed.builder()
                        .name(random(randomNumber(2, 100), true, false))
                        .contacts(Map.of("email", email, "phone", random(randomNumber(6, 50), false, true)))
                        .email(email)
                        .description(randomString(0, 1000))
                        .isVerified(isVerified)
                        .isLegalStatusVerified(isLegalStatusVerified)
                        .legalStatus(legalStatus)
                        .password(random(randomNumber(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH),
                                true, true))
                        .build(), SUCCESS}
        };
    }

    @DataProvider
    public static Object[][] updateValidationDataProvider() {
        boolean isVerified = randomBoolean();
        boolean isLegalStatusVerified = isVerified;
        LegalStatus legalStatus = isLegalStatusVerified ? randomEnum(LegalStatus.class) : null;

        return new Object[][]{
                // Empty user - wait for FAIL
                {UserDetailed.builder().build(), FAIL},

                // Everything is great - wait for SUCCESS
                {UserDetailed.builder()
                        .name(random(randomNumber(2, 100), true, false))
                        .contacts(Map.of("email", randomString(1, 50), "phone", random(randomNumber(6, 50), false, true)))
                        .email("email@mail.com")
                        .description(randomString(0, 1000))
                        .isVerified(isVerified)
                        .isLegalStatusVerified(isLegalStatusVerified)
                        .legalStatus(legalStatus)
                        .password(random(randomNumber(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH),
                                true, true))
                        .build(), SUCCESS}
        };
    }

    @DataProvider
    public static Object[][] passwordValidationDataProvider() {
        return new Object[][]{
                // wait for SUCCESS
                {random(randomNumber(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH), true, true), SUCCESS},

                // "password" is null - wait for FAIL
                {null, FAIL},

                // "password" is longer then allowed - wait for FAIL
                {random(randomNumber(PASSWORD_MAX_LENGTH + 1, PASSWORD_MAX_LENGTH * 2), true, true), FAIL},

                // "password"
                {random(randomNumber(PASSWORD_MIN_LENGTH - 1, PASSWORD_MAX_LENGTH - 1)) + "~", SUCCESS},

                // "password" is shorter than allowed - wait for FAIL
                {random(randomNumber(0, PASSWORD_MIN_LENGTH - 1), true, true), FAIL}
        };
    }

    @DataProvider
    public static Object[][] lettersOnlyValidationDataProvider() {
        final String fieldName = randomString(1, 100);

        return new Object[][]{
                // "field" contains numbers - wait for SUCCESS
                {randomString(1, 100), fieldName, SUCCESS},

                // "field" contains numbers - wait for FAIL
                {randomString(1, 100) + "111", fieldName, FAIL},

                // "field" contains restricted characters - wait for FAIL
                {"[" + randomString(1, 100) + "]", fieldName, FAIL},

                // "field" is empty - wait for FAIL
                {"", fieldName, FAIL},

                // "fieldName" & "field" are empty - wait for FAIL
                {"", "", FAIL},

                // "fieldName" is empty & "field" contains restricted characters - wait for FAIL
                {"[" + randomString(1, 100) + "]", "", FAIL},

                // "fieldName" is empty - wait for FAIL
                {randomString(1, 100), "", FAIL},

                // "fieldName" is null - wait for FAIL
                {randomString(1, 100), null, FAIL},

                // "fieldName" is null - wait for FAIL
                {null, "", FAIL},

                // "fieldName" & "field" - wait for FAIL
                {null, null, FAIL},
        };
    }

    @DataProvider
    public static Object[][] emailValidationDataProvider() {
        return new Object[][]{
                // everything is null - wait for FAIL
                {null, null, FAIL},

                // "email" is null - wait for FAIL
                {null, UUID.randomUUID().toString(), FAIL},

                // "email" is normal - wait for SUCCESS
                {"email@mail.com", UUID.randomUUID().toString(), SUCCESS},

                // "email" is normal & uuid is null - wait for SUCCESS
                {"email@mail.com", null, SUCCESS},

                // "email" contains not only letters and numbers - wait for FAIL
                {"email@mail.com]}~", null, FAIL},

                // "email" is empty string - wait for FAIL
                {"", UUID.randomUUID().toString(), FAIL},
        };
    }

    @DataProvider
    public static Object[][] passwordForChangeValidationDataProvider() {
        return new Object[][]{
                // everything is null - wait for fail
                {null, null, null, FAIL},

                // "oldPassword" is null - wait for fail
                {null, randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH), randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH), FAIL},

                // "newPassword" is null - wait for fail
                {randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH), null, randomString(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH), FAIL},

                // "newPassword" is null, and although "old" is correct - wait for fail
                {"1234567890", null, "1234567890", FAIL},

                // Change password on the same one - wait for fail
                {"1234567890", "1234567890", "1234567890", FAIL},

                // wait for success
                {"1234567890", "123456789", "1234567890", SUCCESS},
        };
    }

    @Test(dataProvider = "createValidationDataProvider")
    public void createValidationTest(UserDetailed user, boolean expected) {
        Map<String, Object> errors = userValidator.validateCreation(user);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "updateValidationDataProvider")
    public void updateValidationTest(UserDetailed user, boolean expected) {
        Map<String, Object> errors = userValidator.validateUpdates(user);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "passwordValidationDataProvider")
    public void passwordValidationTest(String password, boolean expected) {

        Map<String, Object> errors = userValidator.validatePassword(password);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "lettersOnlyValidationDataProvider")
    public void lettersOnlyValidationTest(String field, String fieldName, boolean expected) {
        Map<String, Object> errors = userValidator.validateLettersOnlyField(field, fieldName);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }

    // TODO commented while verification is stopped
//    @Test(dataProvider = "emailValidationDataProvider")
    public void emailValidationTest(String email, String userId, boolean expected) {
        Map<String, Object> errors = userValidator.validateEmail(email, userId);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "passwordForChangeValidationDataProvider")
    public void passwordForChangeValidationTest(String oldPassword, String newPassword, String passwordFromDB,
                                                boolean expected) {
        // Hash the password
        String passwordHashFromDB = passwordService.hash(passwordFromDB);

        // Test the method
        Map<String, Object> errors = userValidator.validatePasswordForChange(oldPassword, newPassword, passwordHashFromDB);

        boolean actual = errors.isEmpty();

        System.out.println(StringUtils.join(errors));

        Assert.assertEquals(actual, expected);
    }
}
