create database anyservice_test_db;

\c anyservice_db

drop schema if exists anyservice cascade;
create schema if not exists anyservice;

comment on schema anyservice is 'Main schema for AnyService';

-- FILE_DESCRIPTION
drop table if exists file_description cascade;
create table file_description
(
    id        uuid          not null primary key,
    name      varchar(255)  not null,
    link      varchar(1000) not null,
    size      bigint,
    extension varchar(50),
    state     varchar(50),
    type      varchar(50)   not null,
    dt_create timestamptz   not null
);
comment on table file_description is 'Storage of files metadata';
comment on column file_description.id is 'Primary key of a file';
comment on column file_description.name is 'Original file name';
comment on column file_description.size is 'Size in bytes';
comment on column file_description.extension is 'File extension';
comment on column file_description.state is 'File state (LOADING or other)';
comment on column file_description.type is 'Domain, that files belongs to (profile photo etc.)';
comment on column file_description.dt_create is 'Date and time of file creation and also works as Version of a file';

drop table if exists countries cascade;
create table if not exists countries
(
    id      uuid primary key default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    country varchar(100) not null unique,
    alpha2  char(2)      not null unique,
    alpha3  char(3)      not null unique,
    number  smallint     not null unique
);

comment on table countries is 'Dictionary of allowed countries';
comment on column countries.id is 'Primary key of a country';
comment on column countries.country is 'Country name';
comment on column countries.alpha2 is 'Two letter code of a country';
comment on column countries.alpha2 is 'Three letter code of a country';
comment on column countries.number is 'Unique number of a country';

--------------------------------------------------------------------

drop table if exists orders cascade;
create table if not exists orders
(
    id                   uuid primary key     default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    dt_create            timestamptz not null default now(),
    dt_update            timestamptz not null default now(),
    headline             varchar,
    phone                varchar(50),
    state                varchar(50),
    description          text,
    price                decimal,
    is_discussable_price boolean              default false,
    currency             varchar(50),
    deadline             timestamptz,
    location             jsonb       not null,
    main_attachment      uuid references file_description,
    customer             varchar     not null
);

drop table if exists chats cascade;
create table if not exists chats
(
    id        uuid primary key     default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    dt_create timestamptz not null default now(),
    dt_update timestamptz not null default now(),
    messages  jsonb
);

-- candidates
drop table if exists candidates cascade;
create table if not exists candidates
(
    id         uuid primary key                default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    dt_create  timestamptz            not null default now(),
    dt_update  timestamptz            not null default now(),
    order_uuid uuid references orders not null,
    user_id    varchar                not null,
    response   jsonb                  not null,
    chat_uuid  uuid references chats,
    selected   boolean                         default false
);

-- orders_files
drop table if exists orders_files;
create table orders_files
(
    order_id UUID references orders (id)
        on update cascade
        on delete cascade,
    file_id  UUID references file_description (id)
        on update cascade
        on delete cascade
);

comment on table orders_files is 'Contains any additional materials for an order (videos and photos)';

drop table if exists reviews cascade;
create table if not exists reviews
(
    id                  uuid primary key                default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    dt_create           timestamptz            not null default now(),
    dt_update           timestamptz            not null default now(),
    author              varchar                not null,
    contractor          varchar                not null,
    order_uuid          uuid references orders not null,
    rating              decimal                not null,
    header              varchar,
    body                text,
    contractor_response text,
    compliments         jsonb
);

drop table if exists categories cascade;
create table if not exists categories
(
    id          uuid primary key     default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
    dt_create   timestamptz not null default now(),
    dt_update   timestamptz not null default now(),
    parent_uuid uuid references categories (id),
    title       varchar     not null,
    description text
);

drop table if exists orders_categories;
create table if not exists orders_categories
(
    order_id    uuid references orders (id)
        on update cascade
        on delete cascade,
    category_id uuid references categories (id)
        on update cascade
        on delete cascade
);
